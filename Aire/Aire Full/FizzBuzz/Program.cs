﻿using System;

namespace FizzBuzz
{
    public class Program
    {
        public static int NumberOfFizz;
        public static int NumberOfBuzz;
        public static int NumberOfFizzBuzz;
        public static int NumberOfLucky;
        public static int NumberOfNotAffected;

        private static void Main()
        {
            Console.Write("Insert First Number of the range and press enter \n");
            var input = Console.ReadLine();
            var firstNumber = CheckIfNumber(input);

            Console.Write("Insert Second Number of the range and press enter \n");
            input = Console.ReadLine();
            var secondNumber = CheckIfNumber(input);

            ConsoleKeyInfo newKey;
            if (firstNumber > secondNumber)
            {
                Console.WriteLine(@"First number must be lower than second one. Press Q to exit or any other key to restart.");
                newKey = Console.ReadKey();
                ProgramActions.DoAction(newKey.Key);
            }

            for (var i = firstNumber; i <= secondNumber; i++)
            {
                var output = GetOutput(i);
                Console.Write("{0}\t", output);
            }

            Console.WriteLine();

            Console.WriteLine("{0} : {1}", "fizz", NumberOfFizz);
            Console.WriteLine("{0} : {1}", "buzz", NumberOfBuzz);
            Console.WriteLine("{0} : {1}", "fizzbuzz", NumberOfFizzBuzz);
            Console.WriteLine("{0} : {1}", "lucky", NumberOfLucky);
            Console.WriteLine("{0} : {1}", "integer", NumberOfNotAffected);

            Console.WriteLine(@"Press Q to exit or any other key to restart.");
            newKey = Console.ReadKey();
            ProgramActions.DoAction(newKey.Key);
        }

        #region GetOutput
        public static string GetOutput(int i)
        {
            var output = string.Empty;

            if (IsDivisble(i, 3))
                output = "fizz";
            if (IsDivisble(i, 5))
                output += "buzz";

            if (ContainSpecificDigit(3, i.ToString()))
                output = "lucky";

            if (string.IsNullOrEmpty(output))
                output = i.ToString();

            IncrementTotal(output);

            return output;
        }
        #endregion

        #region CheckIfNumber
        private static int CheckIfNumber(string input)
        {
            int n;

            if (!int.TryParse(input, out n))
            {
                Console.WriteLine(@"Insert only numbers. Press Q to exit or any other key to restart.");
                var newKey = Console.ReadKey();
                ProgramActions.DoAction(newKey.Key);
            }

            return n;
        }
        #endregion

        #region IsDivisble
        public static bool IsDivisble(int x, int n)
        {
            return (x % n) == 0;
        }
        #endregion

        #region ContainSpecificDigit
        public static bool ContainSpecificDigit(int digit, string valueToCheck)
        {
            return valueToCheck.Contains(digit.ToString());
        }
        #endregion

        #region IncrementTotal
        public static void IncrementTotal(string numberType)
        {
            switch (numberType)
            {
                case "fizz":
                    NumberOfFizz += 1;
                    break;
                case "buzz":
                    NumberOfBuzz += 1;
                    break;
                case "fizzbuzz":
                    NumberOfFizzBuzz += 1;
                    break;
                case "lucky":
                    NumberOfLucky += 1;
                    break;
                default:
                    NumberOfNotAffected += 1;
                    break;
            }
        }
        #endregion
    }
}