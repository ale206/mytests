﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FizzBuzz.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void NumberDivisibleForThree()
        {
            Assert.AreEqual(true, Program.IsDivisble(6, 3));
        }

        [TestMethod]
        public void NumberDivisibleForFive()
        {
            Assert.AreEqual(true, Program.IsDivisble(15, 5));
        }

        [TestMethod]
        public void NumberDivisibleForFifteen()
        {
            Assert.AreEqual(true, Program.IsDivisble(15, 15));
        }

        [TestMethod]
        public void NumberNotDivisibleForThree()
        {
            Assert.AreEqual(false, Program.IsDivisble(7, 3));
        }

        [TestMethod]
        public void NumberNotDivisibleForFive()
        {
            Assert.AreEqual(false, Program.IsDivisble(7, 5));
        }

        [TestMethod]
        public void NumberNotDivisibleForFifteen()
        {
            Assert.AreEqual(false, Program.IsDivisble(7, 15));
        }

        [TestMethod]
        public void GetOutputDivisibleForFive()
        {
            Assert.AreEqual("buzz", Program.GetOutput(10));
        }

        [TestMethod]
        public void GetOutputDivisibleForFifteen()
        {
            Assert.AreEqual("fizzbuzz", Program.GetOutput(15));
        }
    }
}
