﻿using FizzBuzz;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FizzBuzzTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void NumberDivisibleForThree()
        {
            Assert.AreEqual(true, Program.IsDivisble(6, 3));
        }

        [TestMethod]
        public void NumberDivisibleForFive()
        {
            Assert.AreEqual(true, Program.IsDivisble(15, 5));
        }

        [TestMethod]
        public void NumberDivisibleForFifteen()
        {
            Assert.AreEqual(true, Program.IsDivisble(15, 15));
        }

        [TestMethod]
        public void NumberNotDivisibleForThree()
        {
            Assert.AreEqual(false, Program.IsDivisble(7, 3));
        }

        [TestMethod]
        public void NumberNotDivisibleForFive()
        {
            Assert.AreEqual(false, Program.IsDivisble(7, 5));
        }

        [TestMethod]
        public void NumberNotDivisibleForFifteen()
        {
            Assert.AreEqual(false, Program.IsDivisble(7, 15));
        }

        [TestMethod]
        public void GetOutputDivisibleForThree()
        {
            Assert.AreEqual("lucky", Program.GetOutput(3));
        }

        [TestMethod]
        public void GetOutputDivisibleForFive()
        {
            Assert.AreEqual("buzz", Program.GetOutput(10));
        }

        [TestMethod]
        public void GetOutputDivisibleForFifteen()
        {
            Assert.AreEqual("fizzbuzz", Program.GetOutput(15));
        }

        [TestMethod]
        public void GetOutputNotDivisibleForThree()
        {
            const int number = 7;
            Assert.AreEqual(number.ToString(), Program.GetOutput(number));
        }

        [TestMethod]
        public void GetOutputNotDivisibleForFive()
        {
            const int number = 7;
            Assert.AreEqual(number.ToString(), Program.GetOutput(number));
        }

        [TestMethod]
        public void GetOutputNotDivisibleForFifteen()
        {
            const int number = 7;
            Assert.AreEqual(number.ToString(), Program.GetOutput(number));
        }

        [TestMethod]
        public void NumberContainSpecificDigit()
        {
            const int number = 3;
            Assert.AreEqual(true, Program.ContainSpecificDigit(3, number.ToString()));
        }

        [TestMethod]
        public void NumberDoesntContainSpecificDigit()
        {
            const int number = 5;
            Assert.AreEqual(false, Program.ContainSpecificDigit(3, number.ToString()));
        }
    }
}
