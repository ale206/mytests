﻿using System;
using System.Reflection;

namespace FizzBuzz
{
    public static class ProgramActions
    {
        public static void DoAction(ConsoleKey key)
        {
            switch (key)
            {
                case ConsoleKey.Q:
                    Environment.Exit(0);
                    break;
                default:
                    var fileName = Assembly.GetExecutingAssembly().Location;
                    System.Diagnostics.Process.Start(fileName);
                    break;

            }
        }
    }
}