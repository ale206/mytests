﻿using AutomapperTest.IoC;
using AutomapperTest.Service;

namespace AutomapperTest.IntegrationTests
{
    [TestFixture]
    public class TestClass
    {
        [SetUp]
        public void Init()
        {
            //AutoFac
            var container = IocSetup();
            _service1 = container.Resolve<IService1>();
        }

        [TearDown]
        public void Cleanup()
        {
        }

        private IService1 _service1;


        public static IContainer IocSetup()
        {
            return AppBuilderExtensions.IoCSetup();
        }
    }
}