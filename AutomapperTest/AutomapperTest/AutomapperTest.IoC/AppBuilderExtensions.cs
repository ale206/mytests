﻿using System.Reflection;

//using IContainer = System.ComponentModel.IContainer;

namespace AutomapperTest.IoC
{
    public static class AppBuilderExtensions
    {
        /// <summary>
        ///    Setup for Consoles
        /// </summary>
        public static IContainer IoCSetup()
        {
            var builder = new ContainerBuilder();

            builder.RegisterAssemblyModules(Assembly.GetExecutingAssembly());

            var container = builder.Build();

            return container;
        }
    }
}