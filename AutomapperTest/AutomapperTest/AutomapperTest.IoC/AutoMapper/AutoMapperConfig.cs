﻿namespace AutomapperTest.IoC.AutoMapper
{
    public class AutoMapperConfig
    {
        public static void Setup(ContainerBuilder builder)
        {
            //AutoMapper
            var mappingConfig = new MapperConfiguration(cfg => { cfg.MappingForFoo(); });

            //Initialize Mapper
            Mapper.Initialize(cfg => { cfg.CreateMissingTypeMaps = true; });

            Mapper.AssertConfigurationIsValid();

            //To Use IMapper Injection
            //TODO: Evaluate to separate configs and inject only what is needed
            builder.RegisterInstance(mappingConfig).As<MapperConfiguration>();
            builder.Register(c => c.Resolve<MapperConfiguration>().CreateMapper(c.Resolve)).As<IMapper>().PreserveExistingDefaults();
            var mapper = mappingConfig.CreateMapper();
            builder.RegisterInstance(mapper).As<IMapper>();
        }
    }
}