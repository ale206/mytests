﻿using AutomapperTest.Service.Dto;

namespace AutomapperTest.IoC.AutoMapper
{
    //DO NOT USED
    public class FooProfile : Profile
    {
        public FooProfile()
        {
            CreateMap<FooSource, FooDestination>()
                .ForMember(dest => dest.TextId, opt => opt.MapFrom(src => src.IDText));
        }
    }
}