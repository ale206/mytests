﻿using AutomapperTest.Service.Dto;

namespace AutomapperTest.IoC.AutoMapper
{
    public static class MapForFoo
    {
        public static IMapperConfigurationExpression MappingForFoo(this IMapperConfigurationExpression cfg)
        {
            //AUDIT
            /**************************************************************************/

            cfg.CreateMap<FooSource, FooDestination>()
                .ForMember(dest => dest.TextId, opt => opt.MapFrom(src => src.IDText));

            return cfg;

            /**************************************************************************/
        }
    }
}