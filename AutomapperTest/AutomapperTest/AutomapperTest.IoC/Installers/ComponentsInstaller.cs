﻿using AutomapperTest.IoC.AutoMapper;
using AutomapperTest.Service;

namespace AutomapperTest.IoC.Installers
{
    public class ComponentsInstaller : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            AutoMapperConfig.Setup(builder);

            builder.RegisterType<Service1>().As<IService1>().PropertiesAutowired();
        }
    }
}