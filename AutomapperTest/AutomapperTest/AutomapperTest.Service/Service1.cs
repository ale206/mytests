﻿using AutomapperTest.Service.Dto;

namespace AutomapperTest.Service
{
    public class Service1 : IService1
    {
        private static IMapper _mapper;

        public Service1(IMapper mapper)
        {
            _mapper = mapper;
        }

        public string MethodTest()
        {
            var fooSource = new FooSource
            {
                IDText = "text"
            };

            var fooDestination = _mapper.Map<FooDestination>(fooSource);

            return fooDestination.TextId;
        }
    }
}