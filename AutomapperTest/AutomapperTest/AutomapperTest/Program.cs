﻿using System;
using AutomapperTest.IoC;
using AutomapperTest.Service;

namespace AutomapperTest
{
    internal class Program
    {
        private static IService1 _service;

        private static void Main()
        {
            //AutoFac
            var container = AppBuilderExtensions.IoCSetup();

            _service = container.Resolve<IService1>();

            Console.WriteLine(_service.MethodTest());
            Console.ReadKey();
        }
    }
}