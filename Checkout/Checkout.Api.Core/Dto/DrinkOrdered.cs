﻿namespace Checkout.Api.Core.Dto
{
    public class DrinkOrdered
    {
        public string Name { get; set; } 
        public int TotalQuantity { get; set; } 
    }
}