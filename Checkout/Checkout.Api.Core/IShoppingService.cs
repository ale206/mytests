﻿using System.Collections.Generic;
using Checkout.Api.Core.Dto;

namespace Checkout.Api.Core
{
    public interface IShoppingService
    {
        int NewDrink(string name, int quantity);
        bool UpdateDrink(int drinkId, int quantity);
        bool DeleteDrink(int drinkId);
        DrinkOrdered GetDrink(string drinkName);
        IEnumerable<Drink> GetShoppingList();
    }
}
