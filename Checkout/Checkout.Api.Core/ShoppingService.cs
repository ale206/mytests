﻿using System;
using System.Collections.Generic;
using System.Linq;
using Checkout.Api.Core.Dto;

namespace Checkout.Api.Core
{
    public class ShoppingService : IShoppingService
    {
        #region NewDrink
        /// <summary>
        /// Http Post request should adding a drink in to the list with quantity e.g.name of drink (Pepsi) and quantity(1)
        /// </summary>
        /// <param name="name"></param>
        /// <param name="quantity"></param>
        /// <returns></returns>
        public int NewDrink(string name, int quantity)
        {
            try
            {
                if (string.IsNullOrEmpty(name) || quantity < 1)
                    throw new ArgumentException("Empty Name or quantity less than 1");

                if (ShoppingList.DrinkList == null)
                    ShoppingList.DrinkList = new List<Drink>();

                var newId = ShoppingList.DrinkList.Any() ? ShoppingList.DrinkList.Max(x => x.Id) + 1 : 1;

                var drink = new Drink() { Id = newId, Name = name, Quantity = quantity };

                ShoppingList.DrinkList.Add(drink);

                return newId;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region UpdateDrink
        /// <summary>
        /// Http Put request for updating a drinks quantity
        /// </summary>
        /// <param name="drinkId"></param>
        /// <param name="quantity"></param>
        /// <returns></returns>
        public bool UpdateDrink(int drinkId, int quantity)
        {
            try
            {
                if (drinkId < 1 || quantity < 1)
                    throw new ArgumentException("DrinkId or Quantity less than 1");

                if (ShoppingList.DrinkList == null)
                    throw new Exception("Any drink present");

                var drink = ShoppingList.DrinkList.FirstOrDefault(x => x.Id == drinkId);

                if (drink == null)
                    throw new Exception("Wronk Id");

                ShoppingList.DrinkList.First(x => x.Id == drinkId).Quantity = quantity;

                return true;
            }
            catch (Exception)
            {
                throw;
            }
            
        }
        #endregion

        #region DeleteDrink
        /// <summary>
        /// Http Delete request for removing a drink from the shopping list
        /// </summary>
        /// <param name="drinkId"></param>
        /// <returns></returns>
        public bool DeleteDrink(int drinkId)
        {
            try
            {
                if (drinkId < 1)
                    throw new ArgumentException("DrinkId less than 1");

                if (ShoppingList.DrinkList == null)
                    throw new Exception("Any drink present");

                var drink = ShoppingList.DrinkList.FirstOrDefault(x => x.Id == drinkId);

                if (drink == null)
                    throw new Exception("Wronk Id");

                ShoppingList.DrinkList.Remove(drink);

                return true;
            }
            catch (Exception)
            {
                throw;
            }
           
        }
        #endregion

        #region GetDrink
        /// <summary>
        /// Http Get request for retrieving a drink by its name and its quantity so we can see how many ordered
        /// </summary>
        /// <param name="drinkName"></param>
        /// <returns></returns>
        public DrinkOrdered GetDrink(string drinkName)
        {
            try
            {
                if (string.IsNullOrEmpty(drinkName))
                    throw new ArgumentException("Drink name empty");

                if (ShoppingList.DrinkList == null)
                    throw new Exception("Any drink present");

                var drink = ShoppingList.DrinkList.FirstOrDefault(x => x.Name == drinkName);

                if (drink == null)
                    throw new Exception("Wrong Id");

                var drinkOrdered = new DrinkOrdered()
                {
                    Name = drinkName,
                    TotalQuantity = ShoppingList.DrinkList.Where(x => x.Name == drinkName).Sum(x => x.Quantity)
                };

                return drinkOrdered;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region GetShoppingList
        /// <summary>
        /// Http Get request for retrieving full list of what we have in the shopping list
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Drink> GetShoppingList()
        {
            try
            {
                if (ShoppingList.DrinkList == null)
                    throw new Exception("Any drink present");

                return ShoppingList.DrinkList;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        #endregion

    }
}