﻿using System.Collections;
using System.Collections.Generic;
using System.Web.Http;
using Checkout.Api.Core;
using Checkout.Api.Core.Dto;

namespace Checkout.Api.Controllers
{
    public class ShoppingController : ApiController
    {
        private readonly IShoppingService _shoppingService;

        public ShoppingController(IShoppingService shoppingService)
        {
            _shoppingService = shoppingService;
        }

        /// <summary>
        /// Http Post request should adding a drink in to the list with quantity e.g.name of drink (Pepsi) and quantity(1)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("drink/add")]
        public int NewDrink(string name, int quantity)
        {
            return _shoppingService.NewDrink(name, quantity);
        }

        /// <summary>
        /// Http Put request for updating a drinks quantity
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        [Route("drink/update/{drinkid}/{quantity}")]
        public bool UpdateDrink(int drinkId, int quantity)
        {
            return _shoppingService.UpdateDrink(drinkId, quantity);
        }

        /// <summary>
        /// Http Delete request for removing a drink from the shopping list
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        [Route("drink/delete/{drinkid}")]
        public bool DeleteDrink(int drinkId)
        {
            return _shoppingService.DeleteDrink(drinkId);
        }

        /// <summary>
        /// Http Get request for retrieving a drink by its name and its quantity so we can see how many ordered
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("drink/{drinkname}")]
        public DrinkOrdered GetDrink(string drinkName)
        {
            return _shoppingService.GetDrink(drinkName);
        }

        /// <summary>
        /// Http Get request for retrieving full list of what we have in the shopping list
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("shoppinglist/info")]
        public IEnumerable<Drink> GetShoppingList()
        {
            return _shoppingService.GetShoppingList();
        }
    }
}