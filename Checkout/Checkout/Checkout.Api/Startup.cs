﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Checkout.Api.App_Start;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Checkout.Api.Startup))]
namespace Checkout.Api
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //app.LetMeInspectRequests();

            //app.LogUnhandledErrors();


            AreaRegistration.RegisterAllAreas();
            ControllerBuilder.Current.DefaultNamespaces.Add("Checkout.Api.Controllers");
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            //app.UseWebApi(GlobalConfiguration.Configuration);

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            GlobalConfiguration.Configuration.EnsureInitialized();
        }
    }
}
