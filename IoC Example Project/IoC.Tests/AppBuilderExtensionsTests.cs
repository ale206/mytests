﻿using System.Web.Http;
using Autofac;
using Microsoft.Owin.Builder;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Owin;

namespace IoC.Tests {

   [TestClass]
   public class AppBuilderExtensionsTests {
      private IContainer _container;

      #region Setup & teardown
      
      [TestInitialize]
      public void TestInitialize() {}

      [TestCleanup]
      public void TestFixtureTearDown() {}

      #endregion

      #region Tests

      [TestMethod]
      [Description("The Setup with no AppBuilder shouldn't throw an exception")]
      public void SetupWithNoAppBuilder_Success() {
         _container = AppBuilderExtensions.IoCSetup();

         Assert.IsNotNull(_container);

         _container.Dispose();
      }

      [TestMethod]
      [Description("The Setup with AppBuilder shouldn't throw an exception")]
      public void SetupWithAppBuilder_Success() {

         IAppBuilder app = new AppBuilder();

         app.IoCSetup(GlobalConfiguration.Configuration);
      }

      #endregion
   }
}