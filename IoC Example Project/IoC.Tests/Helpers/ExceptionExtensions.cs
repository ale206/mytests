﻿using System;

namespace IoC.Tests.Helpers {

   internal static class ExceptionExtensions {

      /// <summary>
      /// Check if an exception is to be swallowed based on its own type or the type of its inner exceptions
      /// </summary>
      /// <typeparam name="T">Type to check the exception against</typeparam>
      /// <param name="e">Exception being verified</param>
      /// <returns><code>true</code> if the exception is to be swallowed</returns>
      public static bool Swallow<T>(this Exception e) where T : Exception {

         return e is T
                || (e.InnerException != null && e.InnerException.Swallow<T>());
      }
   }
}
