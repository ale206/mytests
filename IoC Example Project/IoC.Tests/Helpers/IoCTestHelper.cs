﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Autofac;
using Autofac.Core;
using IoC.Installers;

namespace IoC.Tests.Helpers {

   public static class IoCTestHelper {

      /// <summary>
      ///    Check if the type is an implementation of the generic interface
      /// </summary>
      /// <param name="type">Type to be checked</param>
      /// <param name="interface">Generic Interface</param>
      /// <returns><code>true</code> if the given type implements the requested interface</returns>
      public static bool Implements(this Type type, Type @interface) {

         if (!@interface.IsInterface) {
            throw new ArgumentException(@interface + "is not an interface");
         }

         return @interface.IsAssignableFrom(type) 
                || type.GetInterfaces().Any(i => i.IsGenericType && (i.GetGenericTypeDefinition() == @interface));
      }

      /// <summary>
      ///    Get all types with attribute InjectSelfAttribute given an assembly
      /// </summary>
      /// <typeparam name="T">Attribute to filter types</typeparam>
      /// <param name="assembly">Assembly</param>
      /// <returns>A list of types in the assembly with the given attribute</returns>
      public static IEnumerable<Type> GetTypesWithAttribute<T>(this Assembly assembly) where T : Attribute {
         
         return assembly.GetTypes().WithAttribute<T>();
      }

      /// <summary>
      ///    Get all types in a list with a given attribute 
      /// </summary>
      /// <param name="types">Types to be filtered</param>
      /// <returns>A list of types with the given attribute</returns>
      public static IEnumerable<Type> WithAttribute<T>(this IEnumerable<Type> types) where T : Attribute {
         
         return GetByCustomAttribute<T>(types);
      }

      /// <summary>
      ///    Find all types in a list with a given attribute 
      /// </summary>
      /// <param name="types">Types to be filtered</param>
      /// <returns>A list of types with the given attribute</returns>
      private static IEnumerable<Type> GetByCustomAttribute<T>(IEnumerable<Type> types) where T : Attribute {
         
         return types.Where(type => type.GetCustomAttributes(typeof(T), true).Length > 0);
      }

      /// <summary>
      ///    Get container registrations given a specific attribute, filtered by Namespaces and Types
      /// </summary>
      /// <typeparam name="T">Attribute by which types are filtered</typeparam>
      /// <param name="container">Container</param>
      /// <returns>A list of registered types woth the given attribute</returns>
      public static List<Type> GetRegisteredComponents<T>(IContainer container) where T : Attribute {

         // get registered services
         var services = container
            .ComponentRegistry
            .Registrations
            .SelectMany(x => x.Services)
            .Cast<TypedService>()
            .Select(x => x.ServiceType);
            //.Where(t => ComponentsInstaller.NamespacesToPickInBusinessLogic.Any(t.IsInNamespace) // Filters for Namespaces
            //            && ComponentsInstaller.FilterOnTypesInBusinessLogic.All(f => f(t)) // Filters for Types
            //);

         var components = container
            .ComponentRegistry
            .Registrations
            .Where(r => r.Services.Cast<TypedService>().Any(s => services.Contains(s.ServiceType)))
            .Select(r1 => r1.Activator.LimitType);

         return GetByCustomAttribute<T>(components).ToList();

         //var typesRegistered =
         //    container.ComponentRegistry.Registrations.SelectMany(x => x.Services)
         //       .Cast<TypedService>()
         //       .Select(x => x.ServiceType)
         //       .Where(t => ComponentsInstaller.NamespacesToPickInBusinessLogic.Any(t.IsInNamespace) // Filters for Namespaces
         //                   && ComponentsInstaller.FilterOnTypesInBusinessLogic.All(f => f(t)) // Filters for Types
         //       )
         //       .ToList();

         //typesRegistered = GetByCustomAttribute<T>(typesRegistered).ToList();

         //return typesRegistered;
      }
   }
}