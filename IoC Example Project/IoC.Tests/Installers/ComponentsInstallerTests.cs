﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using adazzle.mediaApp.BL.Conversion.PlanBooking.Interfaces;
using adazzle.mediaApp.globals.Utils.Attributes;
using Autofac;
using IoC.Installers;
using IoC.Tests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace IoC.Tests.Installers {
   
   [TestClass]
   public class ComponentsInstallerTests
   {
      private IContainer _container;

      #region Setup & teardown

      [TestInitialize]
      public void TestInitialize() {

         //Direct References to the assemblies containing controllers
         var blConversionAssembly = typeof(IConversionService).Assembly;
         var blAssembly = typeof(adazzle.mediaApp.businessLogic.Helpers).Assembly;

         _container = AppBuilderExtensions.IoCSetup();
      }

      [TestCleanup]
      public void TestFixtureTearDown() {

         _container.Dispose();
      }

      #endregion

      #region Tests

      [TestMethod]
      [Description("All singleton types in Business Logic are registered")]
      public void SingletonsInBusinessLogic_HaveBeenRegistered() {

         // arrange
         // get types registered as singletons in the container
         var registeredTypes = IoCTestHelper.GetRegisteredComponents<AsSingletonAttribute>(_container);    

         // get expected singleton types
         var expectedTypes = new List<Type>();
         foreach (var profile in ComponentsInstaller.Profiles) {

            var types = profile.Assembly.GetTypes().AsEnumerable();
            if (profile.NamespacesToInclude != null && profile.NamespacesToInclude.Any()) {
               types = types.Where(t => profile.NamespacesToInclude.Any(t.IsInNamespace));
            }
            if (profile.FilterOnTypes != null) {
               foreach (var filter in profile.FilterOnTypes) {
                  types = types.Where(filter);
               }
            }
            types = types
               .Where(type => type.GetCustomAttributes(typeof(AsSingletonAttribute), true).Any())
               .Where(p => !p.IsAbstract && !p.IsGenericType);
            expectedTypes.AddRange(types);
         }

         // Assert
         Assert.IsTrue(new HashSet<Type>(expectedTypes).SetEquals(registeredTypes));

         // Try to resolve types
         foreach (var type in registeredTypes) {
            try {
               var instance = _container.Resolve(type.GetInterfaces().First());
               Debug.WriteLine("{0} resolved", type);
            }
            catch (Exception e) {

               if (!e.Swallow<NullReferenceException>()) {
                  throw new Exception("Unable to resolve type " + type, e);
               }
               Debug.WriteLine("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-");
               Debug.WriteLine("Unable to resolve type {0}, {1} ", type, e);
            }
         }
      }

      //[TestMethod]
      //public void AAA() {

      //   var builder = new ContainerBuilder();
      //   var profile = ComponentsInstaller.Profiles.Last();

      //   profile.RegisterTypes(builder, true);

      //   var container = builder.Build();
      //   container.Dispose();

      //   builder = new ContainerBuilder();
      //   builder.RegisterType<BL.DataFeeds.DeliveryDataServices.DeliveryDataService>().As<BL.DataFeeds.DeliveryDataServices.IDeliveryDataService>();
      //   container = builder.Build();
      //   container.Dispose();

      //   builder = new ContainerBuilder();
      //   builder.RegisterAssemblyTypes(profile.Assembly).AsImplementedInterfaces();
      //   container = builder.Build();
      //   container.Dispose();

      //   builder = new ContainerBuilder();
      //   builder.RegisterAssemblyTypes(profile.Assembly)
      //      .Where(t => profile.NamespacesToInclude.Any(t.IsInNamespace))
      //      .AsImplementedInterfaces();
      //   container = builder.Build();
      //   container.Dispose();

      //   //builder = new ContainerBuilder();
      //   //profile = ComponentsInstaller.Profiles.First();

      //   //profile.RegisterTypes(builder, true);

      //   //container = builder.Build();
      //   //container.Dispose();
      //}

      #endregion
   }
}