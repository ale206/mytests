﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Controllers;
using adazzle.Api.Auth;
using adazzle.mediaApp.globals.Utils;
using api.agency.Controllers;
using Api.Core.Controllers.Actualisation;
using Api.Core.Controllers.Finance;
using Autofac;
using IoC.Installers;
using IoC.Tests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace IoC.Tests.Installers {

   [TestClass]
   public class ControllersInstallerTests {

      private IEnumerable<Assembly> _assemblies;
      private IContainer _container;

      #region Setup & teardown
      
      [TestInitialize]
      public void TestInitialize() {
   
         // direct References to the assemblies containing controllers
         var apiCoreReference = typeof(ActualisationController);
         var apiAgencyReference = typeof(BookingController);
         var apiAuthReference = typeof(ApiOauthAuthorizationServerProvider);

         _container = AppBuilderExtensions.IoCSetup();
         _assemblies = ControllersInstaller.NamePatterns.SelectMany(n => ReflectionHelper.GetAllAssemblies(n, true));
      }

      [TestCleanup]
      public void TestFixtureTearDown() {

         _container.Dispose();
      }

      #endregion

      #region Tests
      
      [TestMethod]
      [Description("All controllers are registered")]
      public void ApiControllers_HaveBeenRegistered() {
         
         // arrange
         // get registered type that implements IHttpController (ApiController)
         var registeredTypes = _container
            .ComponentRegistry
            .Registrations
            .Where(r => typeof(IHttpController).IsAssignableFrom(r.Activator.LimitType))
            .Select(r => r.Activator.LimitType)
            .Where(t => !t.IsAbstract);

         // get types from assemblies deriving from ApiController
         var expectedTypes = _assemblies
            .SelectMany(GetControllersInAssembly)
            .Distinct()
            .Where(type => !type.IsAbstract && !type.IsGenericType);

         // assert
         Assert.IsTrue(registeredTypes.Any());                                            // we do have registered controllers
         Assert.IsTrue(new HashSet<Type>(expectedTypes).SetEquals(registeredTypes));      // and they are the expected ones

         foreach (var type in registeredTypes) {
            try {
               var instance = _container.Resolve(type);
               Debug.WriteLine("{0} resolved", type);
            }
            catch (Exception e) {

               // Skip DDSController for now (dependency issue to be discussed)
               if (!e.Swallow<NullReferenceException>() && type != typeof(DDSController)) {
                  throw new Exception("Unable to resolve type " + type, e);
               }
               Debug.WriteLine("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-");
               Debug.WriteLine("Unable to resolve type {0}, {1} ", type, e);
            }
         }
      }

      #endregion

      #region Internals

      /// <summary>
      ///    Get all ApiControllers in a given Assembly
      /// </summary>
      /// <param name="assembly">Assembly</param>
      /// <returns>A list of types deriving from <see cref="ApiController"/></returns>
      private static IEnumerable<Type> GetControllersInAssembly(Assembly assembly) {
         
         return assembly
            .GetTypes()
            .Where(type => typeof(ApiController).IsAssignableFrom(type)) //filter controllers
            .Where(method => method.IsPublic && !method.IsDefined(typeof(NonActionAttribute)));
      }

      #endregion
   }
}