﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Autofac;
using Autofac.Core;
using IoC.Installers;
using IoC.Tests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace IoC.Tests.Installers {

   [TestClass]
   public class RepositoriesInstallerTests {
      
      private IContainer _container;

      #region Setup & teardown

      [TestInitialize]
      public void TestInitialize() {

         _container = AppBuilderExtensions.IoCSetup();
      }

      [TestCleanup]
      public void TestFixtureTearDown() {
         
         _container.Dispose();
      }

      #endregion

      #region Tests

      [TestMethod]
      [Description("All the RepositoryTypes are registered in the selected assembly")]
      public void Respositories_HaveBeenRegistered() {
         
         // arrange
         // repositories are registered as component for their own service
         var registeredTypes = _container
            .ComponentRegistry
            .Registrations
            .SelectMany(x => x.Services)
            .Cast<TypedService>()
            .Select(x => x.ServiceType)
            .Where(r => !r.IsAbstract 
                        && r.GetInterfaces().Any(i => i.IsGenericType
                                                      && RepositoriesInstaller.BaseTypes.Contains(i.GetGenericTypeDefinition())))
            .Distinct()
            .ToArray();

         var expectedTypes = new List<Type>();

         foreach (var assembly in RepositoriesInstaller.Assemblies) {
            foreach (var baseType in RepositoriesInstaller.BaseTypes) {
         
               var types = assembly
                  .GetTypes()
                  .Where(t => !t.IsAbstract)
                  .Where(type => type.Implements(baseType));

               expectedTypes.AddRange(types);
            }
         }

         expectedTypes = expectedTypes.Distinct().ToList();

         // assert
         Assert.IsTrue(registeredTypes.Any());           // we DO have registered repositories 
         Assert.IsTrue(new HashSet<Type>(expectedTypes).SetEquals(registeredTypes));

         foreach (var type in registeredTypes) {
            try {
               var instance = _container.Resolve(type);
               Debug.WriteLine("{0} resolved", type);
            }
            catch (Exception e) {

               if (!e.Swallow<NullReferenceException>()) {
                  throw new Exception("Unable to resolve type " + type, e);
               }
               Debug.WriteLine("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-");
               Debug.WriteLine("Unable to resolve type {0}, {1} ", type, e);
            }
         }
      }

      #endregion
   }
}