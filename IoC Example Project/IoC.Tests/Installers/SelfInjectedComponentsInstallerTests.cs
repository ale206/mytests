﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using adazzle.mediaApp.globals.Utils.Attributes;
using Autofac;
using Autofac.Core;
using IoC.Installers;
using IoC.Tests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace IoC.Tests.Installers {

   [TestClass]
   public class SelfInjectedComponentsInstallerTests {

      private IContainer _container;

      #region Setup & teardown

      [TestInitialize]
      public void TestInitialize() {

         _container = AppBuilderExtensions.IoCSetup();
      }

      [TestCleanup]
      public void TestFixtureTearDown() {

         _container.Dispose();
      }

      #endregion

      #region Tests

      [TestMethod]
      [Description("All types with attribute InjectSelf are registered")]
      public void InjectSelfAttributeTypes_HaveBeenRegistered() {

         // arrange
         // get registered components with attribute InjectSelf
         var registeredTypes = _container
            .ComponentRegistry
            .Registrations
            .SelectMany(x => x.Services)
            .OfType<IServiceWithType>()
            .Select(x => x.ServiceType)
            .WithAttribute<InjectSelfAttribute>();

         // get expected types 
         var expectedTypes = SelfInjectedComponentsInstaller.SourceAssemblies
            .SelectMany(a => a
               .GetTypesWithAttribute<InjectSelfAttribute>()
               .Where(t => !t.IsAbstract && !t.IsGenericType))
            .Distinct();

         // assert
         Assert.IsTrue(registeredTypes.Any());                    // we got at least one SelfInjected type
         Assert.IsTrue(new HashSet<Type>(expectedTypes).SetEquals(registeredTypes));      // and we got all expected ones

         foreach (var type in registeredTypes) {
            try {
               var instance = _container.Resolve(type);
               Debug.WriteLine("{0} resolved", type);
            }
            catch (Exception e) {

               if (!e.Swallow<NullReferenceException>()) {
                  throw new Exception("Unable to resolve type " + type, e);
               }
               Debug.WriteLine("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-");
               Debug.WriteLine("Unable to resolve type {0}, {1} ", type, e);
            }
         }
      }

      #endregion
   }
}