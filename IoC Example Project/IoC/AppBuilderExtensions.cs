﻿using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using Owin;
using System.Linq;
using IoC.Installers;
using System;

namespace IoC {

   public static class AppBuilderExtensions {
      /// <summary>
      ///    Setup for WebApi
      /// </summary>
      /// <param name="appBuilder">Builder</param>
      /// <param name="config">Configuration</param>
      /// <param name="assemblies">Optional list of Assemblies to scan for Autofac modules</param>
      public static void IoCSetup(this IAppBuilder appBuilder, HttpConfiguration config, params Assembly[] assemblies) {
         
         var builder = new ContainerBuilder();
         builder.RegisterAssemblyModules(Assembly.GetExecutingAssembly());

         if (assemblies.Any()) {
            builder.RegisterAssemblyModules(assemblies);
         }

         var container = builder.Build();

         // Web API
         config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

         appBuilder.UseAutofacWebApi(config);
      }

      /// <summary>
      /// Setup the container for legacy webforms applications
      /// </summary>
      /// <returns>A container</returns>
      public static IContainer LegacyWebformsIoCSetup() {

         var builder = new ContainerBuilder();

         builder.RegisterAssemblyModules(Assembly.GetExecutingAssembly());

         var container = builder.Build();

         return container;
      }

      /// <summary>
      ///    Setup for Consoles or Integration Tests
      /// </summary>
      public static IContainer IoCSetup() {

         var builder = new ContainerBuilder();

         //Debugger.Break();
         var installers = Assembly
            .GetExecutingAssembly()
            .GetTypes()
            .Where(t => !t.IsAbstract && t.IsAssignableTo<Autofac.Module>());

         //builder.RegisterAssemblyModules(Assembly.GetExecutingAssembly());
         foreach (var type in installers) {
            var instance = (Autofac.Module) Activator.CreateInstance(type);
            var installer = instance as InstallerBase;

            if (installer != null) {
               installer.NoWebContext = true;
            }
            builder.RegisterModule(instance);
         }

         var container = builder.Build();

         return container;
      }
   }
}