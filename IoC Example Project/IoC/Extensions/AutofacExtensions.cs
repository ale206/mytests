﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using adazzle.mediaApp.globals.Utils.Attributes;
using Autofac;
using Autofac.Builder;
using Autofac.Features.Scanning;

namespace IoC.Extensions {

   /// <summary>
   /// Extension methods for Autofac
   /// </summary>
   public static class AutofacExtensions {

      /// <summary>
      /// List of namespace prefixes to match when scanning for types having at least one interface (<see cref="WithAtLeastOneInterface"/>
      /// </summary>
      public static readonly IEnumerable<string> AllowedNamespacePrefixes = new[] {
         "adazzle",
         "BL"
      };

      #region Apis

      /// <summary>
      /// Filter out types not implementing an interface
      /// </summary>
      /// <param name="registration">Builder</param>
      /// <returns>The builder</returns>
      public static IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> WithAtLeastOneInterface(
         this IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> registration) {

         //return registration.Where(t => t.GetInterfaces().Any(i => i.Namespace.StartsWith("adazzle", StringComparison.InvariantCulture)));
         return registration
            .Where(t => t.GetInterfaces()
               .Any(i => AllowedNamespacePrefixes.Any(ns => i.Namespace.StartsWith(ns, StringComparison.OrdinalIgnoreCase))));
      }

      /// <summary>
      /// Select only types with the given attribute
      /// </summary>
      /// <typeparam name="TAttribute">Type of the attribute used to select types</typeparam>
      /// <param name="registration">Builder</param>
      /// <returns>The builder</returns>
      public static IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> WithAttribute<TAttribute>(
         this IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> registration)
         where TAttribute : Attribute {

         return registration.Where(t => t.GetCustomAttribute<TAttribute>(true) != null);
      }

      /// <summary>
      /// Select only types without the given attribute
      /// </summary>
      /// <typeparam name="TAttribute">Type of the attribute used to select types</typeparam>
      /// <param name="registration">Builder</param>
      /// <returns>The builder</returns>
      public static IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> WithoutAttribute<TAttribute>(
         this IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> registration)
         where TAttribute : Attribute {

         return registration.Where(t => t.GetCustomAttribute<TAttribute>(true) == null);
      }

      /// <summary>
      /// Apply a list of filters for types
      /// </summary>
      /// <param name="registration">Builder</param>
      /// <param name="typeFilters">List of predicates for types</param>
      /// <returns>The builder</returns>
      public static IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> ApplyTypeFilters(
         this IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> registration, IEnumerable<Func<Type, bool>> typeFilters) {

         if (typeFilters != null && typeFilters.Any()) {
            registration.Where(t => typeFilters.Any(pred => pred(t)));
            //foreach (var filter in typeFilters) {
            //   registration.Where(filter);
            //}
         }

         return registration;
      }

      /// <summary>
      /// Filters types based on namespaces
      /// </summary>
      /// <param name="registration">Builder</param>
      /// <param name="nameSpaces">List of namespaces to pick types from</param>
      /// <returns>The builder</returns>
      public static IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> ApplyNamespaceFilters(
         this IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> registration, IEnumerable<string> nameSpaces) {
         
         if (nameSpaces != null && nameSpaces.Any()) {
            registration.Where(t => nameSpaces.Any(t.IsInNamespace));
         }

         return registration;
      }

      public static IRegistrationBuilder<TLimit, TActivatorData, TStyle> InstancePerRequestIfAvailable<TLimit, TActivatorData, TStyle>(this IRegistrationBuilder<TLimit, TActivatorData, TStyle> registration, bool webContextAvailable) {

         if (webContextAvailable) {
            registration.InstancePerRequest();
         }
         else {
            registration.InstancePerDependency();
         }

         return registration;
      }

      /// <summary>
      /// Scan an assembly for types with attribute <see cref="AsSingletonAttribute"/>, implementing at least one interface,
      /// optionally applying a filter
      /// and register them as singletons
      /// </summary>
      /// <param name="builder">Builder</param>
      /// <param name="assembly">Target assembly</param>
      /// <param name="typeFilters">Optional filter for types</param>
      /// <param name="namespaces">Optional list of namespaces to include (ie only types from these namespaces will be considered)</param>
      public static void RegisterAsSingletons(this ContainerBuilder builder, Assembly assembly, IEnumerable<Func<Type, bool>> typeFilters = null, IEnumerable<string> namespaces = null) {

         var rb = builder
            .RegisterAssemblyTypes(assembly)
            .WithAtLeastOneInterface();

         rb.ApplyTypeFilters(typeFilters);
         rb.ApplyNamespaceFilters(namespaces);

         rb.WithAttribute<AsSingletonAttribute>()
            .AsImplementedInterfaces()
            .SingleInstance();
      }

      /// <summary>
      /// Scan an assembly for types with attribute <see cref="AsTransientAttribute"/>, implementing at least one interface,
      /// optionally applying a filter, and register them as transients
      /// </summary>
      /// <param name="builder">Builder</param>
      /// <param name="assembly">Target assembly</param>
      /// <param name="typeFilters">Optional filter for types</param>
      /// <param name="namespaces">Optional list of namespaces to include (ie only types from these namespaces will be considered)</param>
      public static void RegisterAsTransients(this ContainerBuilder builder, Assembly assembly, IEnumerable<Func<Type, bool>> typeFilters = null, IEnumerable<string> namespaces = null) {

         var rb = builder
            .RegisterAssemblyTypes(assembly)
            .WithAtLeastOneInterface();

         rb.ApplyTypeFilters(typeFilters);
         rb.ApplyNamespaceFilters(namespaces);

         rb.WithAttribute<AsTransientAttribute>()
            .AsImplementedInterfaces()
            .InstancePerDependency();
      }

      /// <summary>
      /// Scan an assembly for types without a lifestyle attribute, implementing at least one interface,
      /// optionally applying a filter, and register them with request scope
      /// </summary>
      /// <param name="builder">Builder</param>
      /// <param name="assembly">Target assembly</param>
      /// <param name="noWebContext">Flag to signal if we are in a web context</param>
      /// <param name="typeFilters">Optional filter for types</param>
      /// <param name="namespaces">Optional list of namespaces to include (ie only types from these namespaces will be considered)</param>
      public static void RegisterPerRequest(this ContainerBuilder builder, Assembly assembly, bool noWebContext, IEnumerable<Func<Type, bool>> typeFilters = null, IEnumerable<string> namespaces = null) {

         var rb = builder
            .RegisterAssemblyTypes(assembly)
            .WithAtLeastOneInterface();

         rb.ApplyTypeFilters(typeFilters);
         rb.ApplyNamespaceFilters(namespaces);

         rb.WithoutAttribute<AsTransientAttribute>()
            .WithoutAttribute<AsSingletonAttribute>()
            .AsImplementedInterfaces()
            .InstancePerRequestIfAvailable(!noWebContext);
         //.InstancePerRequest();
      }

      #endregion
   }
}
