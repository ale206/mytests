﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using adazzle.DataFeeds.IO;
using adazzle.mediaApp.BL.Conversion.PlanBooking.Interfaces;
using adazzle.mediaApp.businessLogic.Booking;
using adazzle.mediaApp.businessLogic.company;
using adazzle.mediaApp.businessLogic.ExternalMappers.SBMS;
using adazzle.mediaApp.businessLogic.Finance.Opale;
using adazzle.mediaApp.businessLogic.FinanceActivity;
using adazzle.mediaApp.businessLogic.Lego;
using adazzle.mediaApp.businessLogic.Planning;
using adazzle.mediaApp.businessLogic.Rfp;
using adazzle.mediaApp.businessLogic.ScheduledTasks;
using adazzle.mediaApp.businessLogic.User.BatchRegistration;
using adazzle.mediaApp.dataAccess.Transactions.Interfaces;
using adazzle.mediaApp.globals.ErrorHandling;
using Autofac;
using BL.DataFeeds.DeliveryDataServices;
using IoC.Extensions;
using adazzle.mediaApp.businessLogic.CampaignDelivery;

namespace IoC.Installers {

   /// <summary>
   /// Register components from BusinessLogic and global implementing at least one interface
   /// </summary>
   public class ComponentsInstaller : InstallerBase {

      public static readonly IEnumerable<RegistrationProfile> Profiles = new[] {
         new RegistrationProfile(typeof(IBatchRegistration).Assembly, new [] {
            typeof(IBatchRegistration).Namespace,
            typeof(SBMSClient).Namespace,
            typeof(IOpaleService).Namespace,
            typeof(IBookingProxy).Namespace,
            typeof(ICompanyProxy).Namespace,
            typeof(IScheduledTaskLogFactory).Namespace,
            typeof(IFinanceReconciliationReporterFactory).Namespace,
            typeof(IPlanningService).Namespace,
            typeof(IEntityMatchService).Namespace,
            typeof(ICampaignDeliveryFactory).Namespace
         }, new [] {
            typeof(ExtendedCsvParser<>),
            typeof(LegoService<>)
         }),
         new RegistrationProfile(typeof(IErrorHandler).Assembly),
         new RegistrationProfile(typeof(IConversionService).Assembly),
         new RegistrationProfile(typeof(ITransactionScopeFactory).Assembly),
         new RegistrationProfile(typeof(IUploaderFactory).Assembly, new [] {
            typeof(IUploaderFactory).Namespace
         }),
         new RegistrationProfile(typeof(IDeliveryDataService).Assembly, new [] {
            typeof(IDeliveryDataService).Namespace
         })
      };

      #region Overrides

      protected override void Load(ContainerBuilder builder) {

         foreach (var profile in Profiles) {
            profile.RegisterTypes(builder, NoWebContext);
         } 

         base.Load(builder);
      }

      #endregion
   }

   /// <summary>
   /// Rules to register types from an assembly, for which both condition hold
   /// - the type is in any of the given namespaces (if any), and
   /// - the type satisfies any predicate (if any)
   /// </summary>
   public class RegistrationProfile {

      /// <summary>
      /// Source assembly
      /// </summary>
      public Assembly Assembly { get; private set; }

      /// <summary>
      /// Namespaces to include in scanning
      /// Types from all namespaces are included if null or empty
      /// </summary>
      public IEnumerable<string> NamespacesToInclude { get; private set; }

      /// <summary>
      /// Open generic types to register (if any)
      /// TODO consider scanning for open generic types instead if there are many of them,
      /// something like
      /// <code>
      ///   someAssembly.GetTypes()
      ///      .Where(t => t.IsGenericType 
      ///         && !t.IsAbstract
      ///         && !t.GetCustomAttributes<CompilerGeneratedAttribute>().Any());
      /// </code>
      /// Since we reference the types directly, no filter (<see cref="NamespacesToInclude"/> 
      /// or <see cref="FilterOnTypes"/>) is applied in this case
      /// </summary>
      /// <remarks>This propewrty does make sense in this type only if we eventually end
      /// up scanning for these types automatically. If it is not the case, probably it make
      /// more sense to have a separate stand-alone property with the open generic types
      /// to register</remarks>
      public IEnumerable<Type> OpenGenericsToRegister { get; private set; }

      /// <summary>
      /// Register only types satisfying any of the given predicates (if any)
      /// </summary>
      /// <remarks>Propably the best usage for this property would be to exclude
      /// types by their namespaces for namespaces nested in <see cref="NamespacesToInclude"/>
      /// </remarks>
      public IEnumerable<Func<Type, bool>> FilterOnTypes { get; set; }

      #region Constructors

      /// <summary>
      /// Get a new instance
      /// </summary>
      /// <param name="assembly">Assembly containing the types to register</param>
      /// <param name="namespacesToInclude">List of namespaces to be included</param>
      /// <param name="openGenerics">Open generic types to register</param>
      /// <param name="filterOnTypes">Filter to be applied on types</param>
      public RegistrationProfile(Assembly assembly, IEnumerable<string> namespacesToInclude = null, IEnumerable<Type> openGenerics = null, 
         IEnumerable<Func<Type, bool>> filterOnTypes = null) {

         Assembly = assembly;
         NamespacesToInclude = namespacesToInclude;
         OpenGenericsToRegister = openGenerics;
         FilterOnTypes = filterOnTypes;
      }

      #endregion

      #region Apis

      /// <summary>
      /// Register types from the assembly
      /// </summary>
      /// <param name="builder">Builder</param>
      /// <param name="noWebContext">Flag to signal whether we have a web context or not (ie test environment)</param>
      public void RegisterTypes(ContainerBuilder builder, bool noWebContext) {

         builder.RegisterAsSingletons(Assembly, FilterOnTypes, NamespacesToInclude);
         builder.RegisterAsTransients(Assembly, FilterOnTypes, NamespacesToInclude);
         builder.RegisterPerRequest(Assembly, noWebContext, FilterOnTypes, NamespacesToInclude);

         if (OpenGenericsToRegister != null) {
            foreach (var type in OpenGenericsToRegister) {

               // Register as implemented interfaces (if any) or as self if the type 
               // does not implement any interfaces
               var rb = builder.RegisterGeneric(type);
               if (type.GetInterfaces().Any()) {
                  rb.AsImplementedInterfaces();
               }
               else {
                  rb.AsSelf();
               }
               rb.InstancePerRequestIfAvailable(!noWebContext);
            }
         }
      }

      #endregion
   }
}