﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using adazzle.mediaApp.globals.Utils;
using Autofac;
using Autofac.Integration.WebApi;
using Module = Autofac.Module;

namespace IoC.Installers {
   
   /// <summary>
   ///    Register Api Controllers
   /// </summary>
   public class ControllersInstaller : Module {

      public static readonly IEnumerable<string> NamePatterns = new[] {
         "api.agency",
         "api.core",
         "api.auth"
      };

      #region Overrides

      /// <summary>
      ///    Override Load method to register Api Controllers
      /// </summary>
      /// <param name="builder">ContainerBuilder</param>
      protected override void Load(ContainerBuilder builder) {
         
         builder
            .RegisterApiControllers(GetAssembliesWithControllers().ToArray())
            .ExternallyOwned();
      }

      #endregion

      #region Internals

      /// <summary>
      ///    Get the assemblies containing the controllers we want to register
      /// </summary>
      /// <returns>List of Assemblies</returns>
      public static IEnumerable<Assembly> GetAssembliesWithControllers() {

         return NamePatterns.SelectMany(n => ReflectionHelper.GetAllAssemblies(n, true));
      }

      #endregion
   }
}