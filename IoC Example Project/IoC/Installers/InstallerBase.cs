﻿using Autofac;

namespace IoC.Installers {

   public abstract class InstallerBase : Module {

      public bool NoWebContext { get; set; }
   }
}
