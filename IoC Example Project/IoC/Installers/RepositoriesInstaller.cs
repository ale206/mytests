﻿using System;
using System.Collections.Generic;
using System.Reflection;
using adazzle.mediaApp.businessLogic;
using adazzle.mediaApp.businessLogic.ApplicationSettings;
using adazzle.mediaApp.businessLogic.Department;
using adazzle.mediaApp.businessLogic.Repository;
using adazzle.mediaApp.businessLogic.User;
using Autofac;
using IoC.Extensions;
using Module = Autofac.Module;

namespace IoC.Installers {

   /// <summary>
   ///    Register types implementing <see cref="IReadRepositoryBase{TEntity}" /> or
   ///    <see cref="IReadWriteRepositoryBase{T}" />
   /// </summary>
   public class RepositoriesInstaller : InstallerBase
   {
      /// <summary>
      /// Base types for repository implementations
      /// </summary>
      public static readonly IEnumerable<Type> BaseTypes = new[] {
         typeof(IReadRepositoryBase<>),
         typeof(IReadWriteRepositoryBase<>)
      };

      /// <summary>
      /// Assemblies to scan for types implementing <see cref="BaseTypes"/>
      /// </summary>
      public static readonly IEnumerable<Assembly> Assemblies = new[] {
         typeof(AppSettingDefinition).Assembly, // BL.AppSettings
         typeof(Helpers).Assembly,              // business Logic
         typeof(DepartmentActions).Assembly,    // BL.Department
         typeof(UserFactory).Assembly           // BL.User
      };

      #region Overrides

      /// <summary>
      ///    Override Load method to register types implementing IReadRepositoryBase and IReadWriteRepositoryBase
      /// </summary>
      /// <param name="builder">ContainerBuilder</param>
      protected override void Load(ContainerBuilder builder) {
         
         foreach (var assembly in Assemblies) {
            foreach (var type in BaseTypes) {
               builder
                  .RegisterAssemblyTypes(assembly)
                  .AsClosedTypesOf(type)
                  .AsSelf()
                  .AsImplementedInterfaces()
                  .InstancePerRequestIfAvailable(!NoWebContext);
                  //.InstancePerRequest();
            }
         }

         base.Load(builder);
      }

      #endregion
   }
}