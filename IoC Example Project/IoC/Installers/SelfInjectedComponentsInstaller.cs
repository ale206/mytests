﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using adazzle.mediaApp.businessLogic;
using adazzle.mediaApp.businessLogic.User.Impersonation;
using adazzle.mediaApp.globals.Utils.Attributes;
using Autofac;
using IoC.Extensions;

namespace IoC.Installers {
   
   /// <summary>
   /// Register types with attribute <see cref="InjectSelfAttribute"/>
   /// </summary>
   public class SelfInjectedComponentsInstaller : InstallerBase {

      /// <summary>
      /// Source assemblies containing types with <see cref="InjectSelfAttribute"/>
      /// </summary>
      public static readonly IEnumerable<Assembly> SourceAssemblies = new[] {
         typeof(Helpers).Assembly,
         typeof(ImpersonationService).Assembly
      }.Distinct();

      #region Overrides

      protected override void Load(ContainerBuilder builder) {

         foreach (var assembly in SourceAssemblies) {
            // Add some generic concrete class => concrete class binds w\ InRequestScope scope
            // This will appy to any concrete class in BusinessLogic if they're decorated with [InjectSelf]
            builder
               .RegisterAssemblyTypes(assembly)
               .WithAttribute<InjectSelfAttribute>()
               .AsSelf()
               // TODO always InstancePerRequest? or should we also check for AsSingleton, AsTransient?
               //.InstancePerRequest();
               .InstancePerRequestIfAvailable(!NoWebContext);
         }

         base.Load(builder);
      }

      #endregion
   }
}
