﻿using Autofac;
using Autofac.Integration.Web;
using IoCComparison.Core;

namespace IoCComparison.Autofac.WebformsSample {

   public partial class Global : IContainerProviderAccessor {

      // Provider that holds the application container.
      static IContainerProvider _containerProvider;

      #region IContainerProviderAccessor

      // Instance property that will be used by Autofac HttpModules
      // to resolve and inject dependencies.
      public IContainerProvider ContainerProvider {
         get { return _containerProvider; }
      }

      #endregion

      #region Internals

      private void SetupContainer() {
         // Build up your application container and register your dependencies.
         var builder = new ContainerBuilder();

         //doc: http://docs.autofac.org/en/latest/lifetime/instance-scope.html
         builder
            .RegisterType<DebugLogger>()
            .AsImplementedInterfaces()  // automatically implements from all interfaces
            .SingleInstance();          // one instance is returned from all requests in the root and all nested scopes.

         builder
            .RegisterType<SampleService>()
            .As<IService>()                           // explicitly define interface
            .PropertiesAutowired()                    // populate Logger property on SampleService
            .InstancePerRequest();                    // in Web Forms and MVC it's helpful to have a sort of singleton per request

         _containerProvider = new ContainerProvider(builder.Build());
      }

      #endregion
   }
}