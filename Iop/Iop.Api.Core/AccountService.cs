﻿using AutoMapper;
using Iop.Api.Core.Dto;
using Iop.ObjectManager;

namespace Iop.Api.Core
{
    public class AccountService : IAccountService
    {
        private readonly IAccountManager _accountManager;

        public AccountService(IAccountManager accountManager)
        {
            _accountManager = accountManager;
        }

        public User Login(string username, string password)
        {
            return Mapper.Map<User>(_accountManager.Login(username, password));
        }
    }
}