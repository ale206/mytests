﻿namespace Iop.Api.Core.Dto
{
    public class Comment
    {
        public int Id { get; set; }
        public int PostId { get; set; }
        public string Text { get; set; }
        public string Username { get; set; }
    }
}