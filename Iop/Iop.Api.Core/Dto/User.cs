﻿namespace Iop.Api.Core.Dto
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Role { get; set; }
        public string Password { get; set; }
        public int Likes { get; set; }
    }
}