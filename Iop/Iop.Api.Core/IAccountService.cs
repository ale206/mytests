﻿using Iop.Api.Core.Dto;

namespace Iop.Api.Core
{
    public interface IAccountService
    {
        User Login(string username, string password);
    }
}