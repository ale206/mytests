﻿using System.Collections.Generic;
using Iop.Api.Core.Dto;

namespace Iop.Api.Core
{
    public interface INewsService
    {
        IEnumerable<Post> GetAllPosts();
        Post GetById(int id);
        bool NewPost(Post postObj);
        bool EditPost(Post postObj);
        bool DeletePost(int id);
        bool NewLike(Like likeObj);
        bool NewComment(Comment commentObj);
    }
}