﻿using System.Collections.Generic;
using AutoMapper;
using Iop.Api.Core.Dto;
using Iop.ObjectManager;
using Iop.ObjectManager.Dto;

namespace Iop.Api.Core
{
    public class NewsService : INewsService
    {
        private readonly INewsManager _newsManager;

        public NewsService(INewsManager newsManager)
        {
            _newsManager = newsManager;
        }


        public IEnumerable<Post> GetAllPosts()
        {
            return Mapper.Map<IEnumerable<Post>>(_newsManager.GetAllPosts());
        }

        public Post GetById(int id)
        {
            return Mapper.Map<Post>(_newsManager.GetById(id));
        }

        public bool NewPost(Post post)
        {
            return _newsManager.NewPost(Mapper.Map<PostObj>(post));
        }

        public bool EditPost(Post post)
        {
            return _newsManager.EditPost(Mapper.Map<PostObj>(post));
        }

        public bool DeletePost(int id)
        {
            return _newsManager.DeletePost(id);
        }

        public bool NewLike(Like like)
        {
            return _newsManager.NewLike(Mapper.Map<LikeObj>(like));
        }


        public bool NewComment(Comment comment)
        {
            return _newsManager.NewComment(Mapper.Map<CommentObj>(comment));
        }
    }
}