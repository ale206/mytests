﻿using System.Web.Http;
using Iop.Api.Attributes;

namespace Iop.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API routes

            //enables attributes routing
            config.MapHttpAttributeRoutes();

            config.Filters.Add(new ExceptionHandlingAttribute());
        }
    }
}