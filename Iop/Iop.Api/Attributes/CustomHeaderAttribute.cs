﻿using System;
using System.Globalization;
using System.Web.Http.Filters;

namespace Iop.Api.Attributes
{
    /// <summary>
    /// Header for all API responses. Configured on Global.asax
    /// </summary>
    public class CustomHeaderAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Exception == null)
            {
                actionExecutedContext.Response.Headers.Add("DateTime", DateTime.UtcNow.ToString(CultureInfo.InvariantCulture));
                actionExecutedContext.Response.Headers.Add("Copyright", "Alessio Di Salvo");
            }
        }
    }
}