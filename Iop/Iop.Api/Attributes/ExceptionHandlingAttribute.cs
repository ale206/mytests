﻿using System;
using System.Net;
using System.Net.Http;
using System.Security.Authentication;
using System.Web.Http;
using System.Web.Http.Filters;

namespace Iop.Api.Attributes
{
    /// <summary>
    /// Set correct status with Http Response Exception
    /// </summary>
    public class ExceptionHandlingAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            var statusCode = HttpStatusCode.InternalServerError;

            if (context.Exception is AuthenticationException)
                statusCode = HttpStatusCode.Forbidden;

            if (context.Exception is ArgumentException)
                statusCode = HttpStatusCode.BadRequest;

            throw new HttpResponseException(context.Request.CreateErrorResponse(statusCode, context.Exception.Message));
        }
    }
}