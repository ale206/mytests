﻿using System.Web.Http;
using Iop.Api.Core;
using Iop.Api.Core.Dto;

namespace Iop.Api.Controllers
{
    public class AccountController : ApiController
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpPost]
        [Route("account/login")]
        public User Login(string username, string password)
        {
            return _accountService.Login(username, password);
        }
    }
}