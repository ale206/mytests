﻿using System.Collections.Generic;
using System.Web.Http;
using Iop.Api.Core;
using Iop.Api.Core.Dto;

namespace Iop.Api.Controllers
{
    public class NewsController : ApiController
    {
        private readonly INewsService _newsService;

        public NewsController(INewsService newsService)
        {
            _newsService = newsService;
        }

        [HttpGet]
        [Route("news/getAll")]
        public IEnumerable<Post> GetAllPosts()
        {
            return _newsService.GetAllPosts();
        }

        [HttpGet]
        [Route("news/{id}")]
        public Post GetById(int id)
        {
            return _newsService.GetById(id);
        }

        [HttpPost]
        [Route("news/add")]
        public bool NewPost(Post post)
        {
            return _newsService.NewPost(post);
        }

        [HttpPost] //TODO: USE PUT
        [Route("news/edit")]
        public bool EditPost(Post post)
        {
            return _newsService.EditPost(post);
        }

        [HttpPost] //TODO: USE DELETE
        [Route("news/{id}/delete")]
        public bool DeletePost(int id)
        {
            return _newsService.DeletePost(id);
        }

        [HttpPost]
        [Route("news/like")]
        public bool NewLike(Like like)
        {
            return _newsService.NewLike(like);
        }


        [HttpPost]
        [Route("news/comment")]
        public bool NewComment(Comment comment)
        {
            return _newsService.NewComment(comment);
        }
    }
}