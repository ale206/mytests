﻿using System.Web.Http;
using Iop.Api.Attributes;
using Iop.Api.Helpers.AutoMapper;

namespace Iop.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AutoMapperXConfiguration.Configure();

            GlobalConfiguration.Configuration.Filters.Add(new CustomHeaderAttribute());
        }
    }
}