﻿namespace Iop.Api.Helpers.AutoMapper
{
    public class AutoMapperXConfiguration
    {
        public AutoMapperXConfiguration()
        {
        }

        public static void Configure()
        {
            ConfigureMapping();
        }

        private static void ConfigureMapping()
        {
            MapForNews.Setup();
            MapForAccount.Setup();
        }
    }
}