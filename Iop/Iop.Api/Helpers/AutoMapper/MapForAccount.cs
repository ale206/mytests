﻿using AutoMapper;
using Iop.Api.Core.Dto;
using Iop.DataAccessLayer.Dto;
using Iop.ObjectManager.Dto;

namespace Iop.Api.Helpers.AutoMapper
{
    public static class MapForAccount
    {
        public static void Setup()
        {
            //ACCOUNT
            /**************************************************************************/
            Mapper.CreateMap<User, UserObj>();
            Mapper.CreateMap<UserObj, UserDto>();
            Mapper.CreateMap<UserDto, UserObj>();
            Mapper.CreateMap<UserObj, User>();
            /**************************************************************************/
        }
    }
}