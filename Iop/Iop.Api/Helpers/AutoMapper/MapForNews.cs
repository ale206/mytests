﻿using AutoMapper;
using Iop.Api.Core.Dto;
using Iop.DataAccessLayer.Dto;
using Iop.ObjectManager.Dto;

namespace Iop.Api.Helpers.AutoMapper
{
    public static class MapForNews
    {
        public static void Setup()
        {
            //NEWS
            /**************************************************************************/
            Mapper.CreateMap<Post, PostObj>();
            Mapper.CreateMap<PostObj, PostDto>();
            Mapper.CreateMap<PostDto, PostObj>();
            Mapper.CreateMap<PostObj, Post>();

            Mapper.CreateMap<Comment, CommentObj>();
            Mapper.CreateMap<CommentObj, CommentDto>();
            Mapper.CreateMap<CommentDto, CommentObj>();
            Mapper.CreateMap<CommentObj, Comment>();

            Mapper.CreateMap<Like, LikeObj>();
            Mapper.CreateMap<LikeObj, LikeDto>();
            Mapper.CreateMap<LikeDto, LikeObj>();
            Mapper.CreateMap<LikeObj, Like>();
            /**************************************************************************/
        }
    }
}