﻿using FluentData;
using Iop.DataAccessLayer.Dto;

namespace Iop.DataAccessLayer
{
    public class DataServiceAccount : IDataServiceAccount
    {
        private readonly IDbContext _dbContext;

        public DataServiceAccount()
        {
            _dbContext = new DbContext().ConnectionStringName("DBConnectionString", new SqlServerProvider());
        }

        public UserDto Login(string username, string password)
        {
            return _dbContext.Sql("USP_Login")
                .CommandType(DbCommandTypes.StoredProcedure)
                .Parameter("Username", username)
                .Parameter("Password", password)
                .QuerySingle<UserDto>();
        }
    }
}