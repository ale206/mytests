﻿using System.Collections.Generic;
using FluentData;
using Iop.DataAccessLayer.Dto;

namespace Iop.DataAccessLayer
{
    public class DataServiceNews : IDataServiceNews
    {
        private readonly IDbContext _dbContext;

        public DataServiceNews()
        {
            _dbContext = new DbContext().ConnectionStringName("DBConnectionString", new SqlServerProvider());
        }

        public IEnumerable<PostDto> GetAllPosts()
        {
            return _dbContext.Sql("USP_GetAllPosts")
                .CommandType(DbCommandTypes.StoredProcedure)
                .QueryMany<PostDto>();
        }

        public IEnumerable<CommentDto> GetAllComments(int postId)
        {
            return _dbContext.Sql("USP_GetAllComments")
                .CommandType(DbCommandTypes.StoredProcedure)
                .Parameter("PostId", postId)
                .QueryMany<CommentDto>();
        }

        public PostDto GetById(int id)
        {
            return _dbContext.Sql("USP_GetById")
                .CommandType(DbCommandTypes.StoredProcedure)
                .Parameter("Id", id)
                .QuerySingle<PostDto>();
        }

        public bool NewPost(PostDto postDto)
        {
            return _dbContext.Sql("USP_NewPost")
                .CommandType(DbCommandTypes.StoredProcedure)
                .Parameter("Author", postDto.Author)
                .Parameter("Body", postDto.Body)
                .Parameter("Title", postDto.Title)
                .QuerySingle<bool>();
        }

        public bool EditPost(PostDto postDto)
        {
            return _dbContext.Sql("USP_EditPost")
                .CommandType(DbCommandTypes.StoredProcedure)
                .Parameter("PostId", postDto.Id)
                .Parameter("Author", postDto.Author)
                .Parameter("Body", postDto.Body)
                .Parameter("Title", postDto.Title)
                .QuerySingle<bool>();
        }

        public bool DeletePost(int id)
        {
            return _dbContext.Sql("USP_DeletePost")
                .CommandType(DbCommandTypes.StoredProcedure)
                .Parameter("PostId", id)
                .QuerySingle<bool>();
        }

        public bool NewLike(LikeDto likeDto)
        {
            return _dbContext.Sql("USP_NewLike")
                .CommandType(DbCommandTypes.StoredProcedure)
                .Parameter("PostId", likeDto.PostId)
                .Parameter("UserId", likeDto.UserId)
                .QuerySingle<bool>();
        }

        public bool NewComment(CommentDto commentDto)
        {
            return _dbContext.Sql("USP_NewComment")
                .CommandType(DbCommandTypes.StoredProcedure)
                .Parameter("PostId", commentDto.PostId)
                .Parameter("Text", commentDto.Text)
                .Parameter("Username", commentDto.Username)
                .QuerySingle<bool>();
        }
    }
}