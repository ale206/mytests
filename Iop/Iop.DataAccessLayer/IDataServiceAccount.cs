﻿using Iop.DataAccessLayer.Dto;

namespace Iop.DataAccessLayer
{
    public interface IDataServiceAccount
    {
        UserDto Login(string username, string password);
    }
}