﻿using System.Collections.Generic;
using Iop.DataAccessLayer.Dto;

namespace Iop.DataAccessLayer
{
    public interface IDataServiceNews
    {
        IEnumerable<PostDto> GetAllPosts();
        IEnumerable<CommentDto> GetAllComments(int postId);
        PostDto GetById(int id);
        bool NewPost(PostDto postDto);
        bool EditPost(PostDto postDto);
        bool DeletePost(int id);
        bool NewLike(LikeDto likeDto);
        bool NewComment(CommentDto commentDto);
    }
}