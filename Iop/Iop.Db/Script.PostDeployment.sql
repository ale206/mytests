﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

MERGE INTO [User] AS Target 
USING (VALUES 
  ('admin', 'admin', 5, 'admin'), 
  ('user1', 'user', 3, 'user1'), 
  ('staff1', 'staff1', 8, 'staff1'), 
  ('staff2', 'staff2', 9, 'staff2')
) 
AS Source (Username, [Role], Likes, [Password]) 
ON Target.Username = Source.Username 
-- update matched rows 
--WHEN MATCHED THEN 
--UPDATE SET Name = Source.Name 
--insert new rows 
WHEN NOT MATCHED BY TARGET THEN 
INSERT (Username, [Role], Likes, [Password]) 
VALUES (Username, [Role], Likes, [Password]);