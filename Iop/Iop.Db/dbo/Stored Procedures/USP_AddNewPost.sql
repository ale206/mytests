﻿-- =============================================
-- Author:		<Alessio Di Salvo>
-- Create date: <06/03/2016>
-- Description:	<Add new post>
-- =============================================
CREATE PROCEDURE USP_AddNewPost
	@Title nvarchar(150),
	@Author nvarchar(150),
	@Body nvarchar(MAX),
	@PublishDate datetime

AS
BEGIN
	
	BEGIN TRY

		SET NOCOUNT ON;

		INSERT  INTO Post (Title, Author, Body, PublishDate) 
				VALUES (@Title, @Author, @Body, @PublishDate)

		SELECT @@IDENTITY as PostId

	END TRY

	BEGIN CATCH

		SELECT 0 as PostId

	END CATCH
END