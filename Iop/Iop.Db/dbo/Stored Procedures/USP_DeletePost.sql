﻿-- =============================================
-- Author:		<Alessio Di Salvo>
-- Create date: <06/03/2016>
-- Description:	<Delete post>
-- =============================================
CREATE PROCEDURE USP_DeletePost
	@PostId int
	
AS
BEGIN
		
	BEGIN TRY

		SET NOCOUNT ON;

		DELETE FROM  Post 
		WHERE Id = @PostId

		SELECT 1 AS PostDeleted

	END TRY
	
	BEGIN CATCH
		SELECT 0 AS PostDeleted
	END CATCH

END