﻿-- =============================================
-- Author:		<Alessio Di Salvo>
-- Create date: <06/03/2016>
-- Description:	<Edit post>
-- =============================================
CREATE PROCEDURE USP_EditPost
	@PostId int,
	@Author nvarchar(150),
	@Title nvarchar(150),
	@Body nvarchar(MAX)
	
AS
BEGIN
		
	BEGIN TRY

		SET NOCOUNT ON;

		UPDATE  Post 
		SET Title = @Title, Author = @Author, Body = @Body 
		WHERE Id = @PostId

		SELECT 1 AS PostUpdated

	END TRY
	
	BEGIN CATCH
		SELECT 0 AS PostUpdated
	END CATCH

END