﻿-- =============================================
-- Author:		<Alessio Di Salvo>
-- Create date: <06/03/2016>
-- Description:	<Get All Comments>
-- =============================================
CREATE PROCEDURE USP_GetAllComments
	@PostId int
	
AS
BEGIN
		
	BEGIN TRY

		SET NOCOUNT ON;

		SELECT Id, [Text], Username
		FROM [Comment]
		WHERE PostId = @PostId 

	END TRY
	
	BEGIN CATCH
		SELECT 0
	END CATCH

END