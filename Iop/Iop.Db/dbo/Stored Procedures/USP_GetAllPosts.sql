﻿-- =============================================
-- Author:		<Alessio Di Salvo>
-- Create date: <06/03/2016>
-- Description:	<Get All Posts>
-- =============================================
CREATE PROCEDURE USP_GetAllPosts
	
	
AS
BEGIN
		
	BEGIN TRY

		SET NOCOUNT ON;

		SELECT Id, Title, Body, PublishDate, Author, Likes
		FROM [Post] 

	END TRY
	
	BEGIN CATCH
		SELECT 0
	END CATCH

END