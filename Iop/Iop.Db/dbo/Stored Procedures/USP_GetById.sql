﻿-- =============================================
-- Author:		<Alessio Di Salvo>
-- Create date: <06/03/2016>
-- Description:	<Get Post by Id>
-- =============================================
CREATE PROCEDURE USP_GetById
	@Id int
	
AS
BEGIN
		
	BEGIN TRY

		SET NOCOUNT ON;

		SELECT Id, Title, Body, PublishDate, Author, Likes
		FROM [Post] 
		WHERE Id = @Id

	END TRY
	
	BEGIN CATCH
		SELECT 0
	END CATCH

END