﻿-- =============================================
-- Author:		<Alessio Di Salvo>
-- Create date: <06/03/2016>
-- Description:	<Edit post>
-- =============================================
CREATE PROCEDURE USP_Login
	@Username nvarchar(150),
	@Password nvarchar(150)
	
AS
BEGIN
		
	BEGIN TRY

		SET NOCOUNT ON;

		SELECT Id, Username, [Role], Likes, [Password]
		FROM [User] 
		WHERE Username = @Username AND Password = @Password 

	END TRY
	
	BEGIN CATCH
		SELECT 0
	END CATCH

END