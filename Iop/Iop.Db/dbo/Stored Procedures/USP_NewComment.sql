﻿-- =============================================
-- Author:		<Alessio Di Salvo>
-- Create date: <06/03/2016>
-- Description:	<Add new comment>
-- =============================================
CREATE PROCEDURE USP_NewComment
	@PostId int,
	@Text nvarchar(MAX),
	@Username nvarchar(150)

AS
BEGIN
	
	BEGIN TRY

		SET NOCOUNT ON;

	INSERT INTO Comment 
           (PostId, [Text], Username)
     VALUES
           (@PostId, @Text, @Username)
		
		SELECT 1 AS CommentInserted

	END TRY

	BEGIN CATCH

		SELECT 0 as CommentInserted

	END CATCH
END