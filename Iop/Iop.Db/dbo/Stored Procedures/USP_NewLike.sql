﻿-- =============================================
-- Author:		<Alessio Di Salvo>
-- Create date: <06/03/2016>
-- Description:	<Add new like>
-- =============================================
CREATE PROCEDURE USP_NewLike
	@PostId int,
	@UserId int
AS
BEGIN
	
	BEGIN TRY

		SET NOCOUNT ON;

		DECLARE @Likes int = 0
		DECLARE @UserLikes int = 0

		SELECT @UserLikes = Likes FROM [User] WHERE Id = @UserId

		IF(@UserLikes > 0)
			BEGIN

				UPDATE  [User] SET Likes = (@UserLikes - 1) WHERE  Id = @UserId

				SELECT @Likes = Likes FROM Post WHERE Id = @PostId
				UPDATE Post SET Likes = (@Likes + 1) WHERE Id = @PostId

				SELECT 1 AS LikeInserted
			END
		ELSE
			BEGIN
				SELECT 0 AS LikeInserted
			END

	END TRY

	BEGIN CATCH

		SELECT 0 as LikeInserted

	END CATCH
END