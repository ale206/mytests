﻿-- =============================================
-- Author:		<Alessio Di Salvo>
-- Create date: <06/03/2016>
-- Description:	<Add new post>
-- =============================================
CREATE PROCEDURE USP_NewPost
	@Author nvarchar(150),
	@Body nvarchar(MAX),
	@Title nvarchar(150)

AS
BEGIN
	
	BEGIN TRY

		SET NOCOUNT ON;

	INSERT INTO [Post] 
           ([Title], [Body], [PublishDate], [Author], [Likes])
     VALUES
           (@Title, @Body, GETDATE(), @Author, 0)
		
		SELECT 1 AS PostInserted

	END TRY

	BEGIN CATCH

		SELECT 0 as PostInserted

	END CATCH
END