﻿CREATE TABLE [dbo].[Post] (
    [Id] INT IDENTITY (1, 1) NOT NULL,
    [Title] NVARCHAR(150) NOT NULL, 
    [Body] NVARCHAR(MAX) NOT NULL, 
    [PublishDate] DATETIME NOT NULL, 
    [Author] NVARCHAR(150) NOT NULL, 
    [Likes] INT NULL, 
    CONSTRAINT [PK_Post] PRIMARY KEY CLUSTERED ([Id] ASC)
);

