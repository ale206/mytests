﻿using Iop.Api.Helpers.AutoMapper;
using Iop.DataAccessLayer;
using Iop.DataAccessLayer.Dto;
using Iop.ObjectManager.Dto;
using Moq;
using NUnit.Framework;

namespace Iop.ObjectManager.Test
{
    [TestFixture]
    public class AccountManagerTests
    {
        private Mock<IDataServiceAccount> _mockIDataServiceAccount;

        [SetUp]
        public void Init()
        {
            //TODO: Find a better way to test automapper
            MapForAccount.Setup();

            _mockIDataServiceAccount = new Mock<IDataServiceAccount>();
        }

        [TearDown]
        public void Cleanup()
        {
            _mockIDataServiceAccount = null;
        }

        [Test]
        public void Login_WrongUsernameOrPassword_Test()
        {
            var userManager = new AccountManager(_mockIDataServiceAccount.Object);

            Assert.AreEqual(null, userManager.Login("", ""));
        }

        [Test]
        public void Login_Success_Test()
        {
            var userManager = new AccountManager(_mockIDataServiceAccount.Object);

            _mockIDataServiceAccount.Setup(x => x.Login(It.IsAny<string>(), It.IsAny<string>())).Returns(new UserDto());

            Assert.IsInstanceOf<UserObj>(userManager.Login("admin", "admin"));
        }
    }
}