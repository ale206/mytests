﻿using System.Collections.Generic;
using Iop.Api.Helpers.AutoMapper;
using Iop.DataAccessLayer;
using Iop.DataAccessLayer.Dto;
using Iop.ObjectManager.Dto;
using Moq;
using NUnit.Framework;

namespace Iop.ObjectManager.Test
{
    public class NewsManagerTests
    {
        [TestFixture]
        public class AccountManagerTests
        {
            private Mock<IDataServiceNews> _mockIDataServiceNews;

            [SetUp]
            public void Init()
            {
                //TODO: Find a better way to test automapper
                MapForNews.Setup();

                _mockIDataServiceNews = new Mock<IDataServiceNews>();
            }

            [TearDown]
            public void Cleanup()
            {
                _mockIDataServiceNews = null;
            }

            [Test]
            public void GetAllPosts_Test()
            {
                var newsManager = new NewsManager(_mockIDataServiceNews.Object);
                _mockIDataServiceNews.Setup(x => x.GetAllPosts()).Returns(new List<PostDto>());

                Assert.IsInstanceOf<IEnumerable<PostObj>>(newsManager.GetAllPosts());
            }

            [Test]
            public void GetById_Test()
            {
                var newsManager = new NewsManager(_mockIDataServiceNews.Object);
                _mockIDataServiceNews.Setup(x => x.GetById(It.IsAny<int>())).Returns(new PostDto());

                Assert.IsInstanceOf<PostObj>(newsManager.GetById(1));
            }

            [Test]
            public void NewPost_Test()
            {
                var newsManager = new NewsManager(_mockIDataServiceNews.Object);
                _mockIDataServiceNews.Setup(x => x.NewPost(It.IsAny<PostDto>())).Returns(true);

                Assert.IsInstanceOf<bool>(newsManager.NewPost(new PostObj()));
            }

            [Test]
            public void EditPost_Test()
            {
                var newsManager = new NewsManager(_mockIDataServiceNews.Object);
                _mockIDataServiceNews.Setup(x => x.EditPost(It.IsAny<PostDto>())).Returns(true);

                Assert.IsInstanceOf<bool>(newsManager.EditPost(new PostObj()));
            }

            [Test]
            public void DeletePost_Test()
            {
                var newsManager = new NewsManager(_mockIDataServiceNews.Object);
                _mockIDataServiceNews.Setup(x => x.DeletePost(It.IsAny<int>())).Returns(true);

                Assert.IsInstanceOf<bool>(newsManager.DeletePost(1));
            }

            [Test]
            public void NewLike_Test()
            {
                var newsManager = new NewsManager(_mockIDataServiceNews.Object);
                _mockIDataServiceNews.Setup(x => x.NewLike(It.IsAny<LikeDto>())).Returns(true);

                Assert.IsInstanceOf<bool>(newsManager.NewLike(new LikeObj()));
            }

            [Test]
            public void NewComment_Test()
            {
                var newsManager = new NewsManager(_mockIDataServiceNews.Object);
                _mockIDataServiceNews.Setup(x => x.NewComment(It.IsAny<CommentDto>())).Returns(true);

                Assert.IsInstanceOf<bool>(newsManager.NewComment(new CommentObj()));
            }

            [Test]
            public void GetAllComments_Test()
            {
                var newsManager = new NewsManager(_mockIDataServiceNews.Object);
                _mockIDataServiceNews.Setup(x => x.GetAllComments(It.IsAny<int>())).Returns(new List<CommentDto>());

                Assert.IsInstanceOf<IEnumerable<CommentObj>>(newsManager.GetAllComments(1));
            }
        }
    }
}