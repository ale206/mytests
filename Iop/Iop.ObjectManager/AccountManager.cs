﻿using AutoMapper;
using Iop.DataAccessLayer;
using Iop.ObjectManager.Dto;

namespace Iop.ObjectManager
{
    public class AccountManager : IAccountManager
    {
        private readonly IDataServiceAccount _dataServiceAccount;

        public AccountManager(IDataServiceAccount dataServiceAccount)
        {
            _dataServiceAccount = dataServiceAccount;
        }

        public UserObj Login(string username, string password)
        {
            return Mapper.Map<UserObj>(_dataServiceAccount.Login(username, password));
        }
    }
}