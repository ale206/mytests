﻿using System;
using System.Collections.Generic;

namespace Iop.ObjectManager.Dto
{
    public class PostObj
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string Body { get; set; }
        public DateTime PublishDate { get; set; }
        public string Url { get; set; }
        public int Likes { get; set; }
        public IEnumerable<CommentObj> Comments { get; set; }
    }
}