﻿using Iop.ObjectManager.Dto;

namespace Iop.ObjectManager
{
    public interface IAccountManager
    {
        UserObj Login(string username, string password);
    }
}