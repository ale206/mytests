﻿using System.Collections.Generic;
using Iop.ObjectManager.Dto;

namespace Iop.ObjectManager
{
    public interface INewsManager
    {
        IEnumerable<PostObj> GetAllPosts();
        PostObj GetById(int id);
        bool NewPost(PostObj postObj);
        bool EditPost(PostObj postObj);
        bool DeletePost(int id);
        bool NewLike(LikeObj likeObj);
        bool NewComment(CommentObj commentObj);
        IEnumerable<CommentObj> GetAllComments(int postId);
    }
}