﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using AutoMapper;
using Iop.DataAccessLayer;
using Iop.DataAccessLayer.Dto;
using Iop.ObjectManager.Dto;

namespace Iop.ObjectManager
{
    public class NewsManager : INewsManager
    {
        private readonly IDataServiceNews _dataServiceNews;

        public NewsManager(IDataServiceNews dataServiceNews)
        {
            _dataServiceNews = dataServiceNews;
        }

        public IEnumerable<PostObj> GetAllPosts()
        {
            var allPosts = Mapper.Map<IEnumerable<PostObj>>(_dataServiceNews.GetAllPosts());

            var postObjs = allPosts as IList<PostObj> ?? allPosts.ToList();
            postObjs.ToList().ForEach(c => c.Comments = GetAllComments(c.Id));
            postObjs.ToList().ForEach(c => c.Url = ConfigurationManager.AppSettings["url"]);

            return postObjs;
        }

        public PostObj GetById(int id)
        {
            var post =  Mapper.Map<PostObj>(_dataServiceNews.GetById(id));
            post.Url = ConfigurationManager.AppSettings["url"];

            return post;
        }

        public bool NewPost(PostObj postObj)
        {
            return _dataServiceNews.NewPost(Mapper.Map<PostDto>(postObj));
        }

        public bool EditPost(PostObj postObj)
        {
            return _dataServiceNews.EditPost(Mapper.Map<PostDto>(postObj));
        }

        public bool DeletePost(int id)
        {
            return _dataServiceNews.DeletePost(id);
        }

        public bool NewLike(LikeObj likeObj)
        {
            return _dataServiceNews.NewLike(Mapper.Map<LikeDto>(likeObj));
        }

        public bool NewComment(CommentObj commentobj)
        {
            return _dataServiceNews.NewComment(Mapper.Map<CommentDto>(commentobj));
        }

        public IEnumerable<CommentObj> GetAllComments(int postId)
        {
            return Mapper.Map<IEnumerable<CommentObj>>(_dataServiceNews.GetAllComments(postId));
        }
    }
}