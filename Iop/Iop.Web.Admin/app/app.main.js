﻿
var iop_main = angular.module('iop_main', ['ngRoute', 'ngCookies', 'adminController']);

iop_main.constant('appConfig', {
	apiBaseUrl: "http://localhost:12360/"
});

iop_main.config([
    '$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    	$routeProvider.
             when('/admin', {
             	templateUrl: 'app/components/admin/admin.html',
             	controller: 'adminController'
             }).
            otherwise({
                redirectTo: '/admin'
            });

    	//To remove # from URL
    	$locationProvider.html5Mode(true);
    }]);

iop_main.controller('mainController', ['$scope', '$cookies', function ($scope, $cookies) {

    $scope.username = $cookies.Username;
}]);


