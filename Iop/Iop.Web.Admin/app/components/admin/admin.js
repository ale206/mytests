﻿var adminController = angular.module('adminController', ["ngCookies"]);

adminController.controller('adminController', ['$scope', '$rootScope', '$http', '$location', '$window', '$cookies', 'appConfig',
    function ($scope, $rootScope, $http, $location, $window, $cookies, appConfig) {

        $scope.showListPnl = true;

        $scope.userEnabledToPublish = $cookies.Role !== "staff2" ? true : false

        $scope.addNew = function() {
            $scope.addNewPnl = true;
            $scope.showListPnl = false;
            $scope.isCreation = true;

            $('#title').val();
            $('#author').val();
            $('#body').val();
            $('#hiddenPostId').val();
        }

        $scope.showList = function () {
            $scope.addNewPnl = false;
            $scope.showListPnl = true;
        }
        


        $scope.posts = {};

        $scope.getAllPosts = function() {

            // GET ALL POSTS
            $http({
                method: 'GET',
                url: appConfig.apiBaseUrl + 'news/getAll'
            }).then(function(response) {

                $scope.posts = response.data;
            }, function errorCallback(response) {
                alert(response.data['Message']);
            });
        }

        $scope.getAllPosts();

        $scope.editPost = function(postId) {

            $scope.addNew();
            $scope.isCreation = false;
            $scope.postId = postId;

            // GET POST
            $http({
                method: 'GET',
                url: appConfig.apiBaseUrl + 'news/' + postId
            }).then(function (response) {

                $('#title').val(response.data["Title"]);
                $('#author').val(response.data["Author"]);
                $('#body').val(response.data["Body"]);                
                $('#hiddenPostId').val(postId);

            }, function errorCallback(response) {
                alert(response.data['Message']);
            });
        }

        // ADD POST
        $scope.newPostSubmit = function () {
            $http({
                method: 'POST',
                url: appConfig.apiBaseUrl + 'news/add',
                headers: { 'Content-Type': 'application/json' },
                data: JSON.stringify({
                    Title: $scope.new.title,
                    Author: $scope.new.author,
                    Body: $scope.new.body
                })
            }).then(function (response) {

                if (response.data) {
                    $scope.showList();
                    $scope.getAllPosts();
                }
            }, function errorCallback(response) {
                alert(response.data['Message']);
            });
        }

        // EDIT POST
        $scope.editPostSubmit = function () {

            $http({
                method: 'POST', //TODO: USE PUT
                url: appConfig.apiBaseUrl + 'news/edit',
                headers: { 'Content-Type': 'application/json' },
                data: JSON.stringify({
                    Title: $('#title').val(),
                    Author: $('#author').val(),
                    Body: $('#body').val(),
                    Id: $('#hiddenPostId').val()
                })
            }).then(function (response) {

                if (response.data) {
                    $scope.getAllPosts();
                    $scope.showList();
                }
            }, function errorCallback(response) {
                alert(response.data['Message']);
            });
        }

        // DELETE POST
        $scope.deletePost = function (postId) {
            $http({
                method: 'POST', //TODO: USE DELETE
                url: appConfig.apiBaseUrl + 'news/' + postId + '/delete',
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {

                if (response.data)
                    $scope.showList();

            }, function errorCallback(response) {
                alert(response.data['Message']);
            });
        }

    }]);

