﻿
var iop_main = angular.module('iop_main', ['ngRoute', 'homeController', 'loginController']);

iop_main.constant('appConfig', {
	apiBaseUrl: "http://localhost:12360/",
	adminBaseUrl: "http://localhost:4041/"
});

iop_main.config([
    '$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    	$routeProvider.
            when('/home', {
            	templateUrl: 'app/components/home/home.html',
            	controller: 'homeController'
            }).
             when('/login', {
                 templateUrl: 'app/components/login/login.html',
                 controller: 'loginController'
             }).
            otherwise({
            	redirectTo: '/home'
            });

    	//To remove # from URL
    	$locationProvider.html5Mode(true);
    }]);

iop_main.controller('mainController', ['$scope', '$rootScope', '$cookies', function ($scope, $rootScope, $cookies) {

    //console.log($cookies.Role);
    //console.log($cookies.UserId);

    var userId = $cookies.UserId;

    $rootScope.userLogged = (userId > 0) ? true : false;

    $scope.logout = function() {
        $cookies.Role = "";
        $cookies.UserId = "";
        $scope.userLogged = false;
        console.log('logged out');
    }

}]);


