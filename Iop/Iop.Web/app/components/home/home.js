﻿var homeController = angular.module('homeController', []);

homeController.controller('homeController', ['$scope', '$rootScope', '$http', '$location', '$window', '$cookies', 'appConfig',
    function ($scope, $rootScope, $http, $location, $window, $cookies, appConfig) {

        // GET ALL POSTS

        $scope.getAllMessages = function() {
            $http({
                method: 'GET',
                url: appConfig.apiBaseUrl + 'news/getAll'
            }).then(function(response) {

                $scope.posts = response.data;

            }, function errorCallback(response) {
                alert(response.data['Message']);
            });
        };

        $scope.getAllMessages();      

        $scope.newCommentSubmit = function (postId) {
            $http({
                method: 'POST', 
                url: appConfig.apiBaseUrl + 'news/comment',
                headers: { 'Content-Type': 'application/json' },
                data: JSON.stringify({
                    PostId: postId,
                    Text: $('#commentText' + postId).val(),
                    Username: $cookies.Username
                })
            }).then(function (response) {

                $('#commentText' + postId).val("");
                $scope.getAllMessages();

            }, function errorCallback(response) {
                alert(response.data['Message']);
            });
        }


        $scope.newLikeSubmit = function (postId) {
            $http({
                method: 'POST',
                url: appConfig.apiBaseUrl + 'news/like',
                headers: { 'Content-Type': 'application/json' },
                data: JSON.stringify({
                    PostId: postId,
                    UserId: $cookies.UserId
                })
            }).then(function (response) {

                $scope.getAllMessages();

                if (!response["data"])
                    alert("No more likes available");
               
            }, function errorCallback(response) {
                alert(response.data['Message']);
            });
        }
       

    }]);

