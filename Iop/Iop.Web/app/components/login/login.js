﻿var loginController = angular.module('loginController', ["ngCookies"]);

loginController.controller('loginController', ['$scope', '$rootScope', '$http', '$location', '$window', '$cookies', 'appConfig',
    function ($scope, $rootScope, $http, $location, $window, $cookies, appConfig) {

        $scope.submitLogin = function() {
            $http({
                method: 'POST',
                url: appConfig.apiBaseUrl + 'account/login?username=' + $scope.login.username + '&password=' + $scope.login.password,
                headers: { 'Content-Type': 'application/json' }
                
            }).then(function successCallback(response) {

                    

                if (response) {
                    
                    console.log(response.data);

                    if (response.data === undefined || response.data === null || response.data === "null") {
                        alert("Wrong U/N or P/W. \n Use one of the following: \n admin admin \n user1 user1 \n staff1 staff1 \n staff2 staff2");
                    } else {

                        // Setting a cookie
                        $cookies.Role = response.data["Role"];
                        $cookies.UserId = response.data["Id"].toString();
                        $cookies.Username = response.data["Username"].toString();

                        console.log(response.data["Id"]);
                        $scope.userLogged = true;

                        if (response.data["Role"] === "user1") {
                            $window.location.href = "/";
                        } else {
                            $window.location.href = appConfig.adminBaseUrl;
                        }
                    }
                }
                },
                function errorCallback(response) {

                });
        };

    }]);

