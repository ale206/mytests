USE [IopDb]
GO
/****** Object:  User [NT AUTHORITY\NETWORK SERVICE]    Script Date: 06/03/2016 19:42:05 ******/
CREATE USER [NT AUTHORITY\NETWORK SERVICE] FOR LOGIN [NT AUTHORITY\NETWORK SERVICE] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [NT AUTHORITY\NETWORK SERVICE]
GO
ALTER ROLE [db_datareader] ADD MEMBER [NT AUTHORITY\NETWORK SERVICE]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [NT AUTHORITY\NETWORK SERVICE]
GO
/****** Object:  Table [dbo].[Comment]    Script Date: 06/03/2016 19:42:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PostId] [int] NOT NULL,
	[Text] [nvarchar](500) NOT NULL,
	[Username] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Post]    Script Date: 06/03/2016 19:42:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Post](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](150) NOT NULL,
	[Body] [nvarchar](max) NOT NULL,
	[PublishDate] [datetime] NOT NULL,
	[Author] [nvarchar](150) NOT NULL,
	[Likes] [int] NULL,
 CONSTRAINT [PK_Post] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 06/03/2016 19:42:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](50) NOT NULL,
	[Role] [nvarchar](50) NOT NULL,
	[Likes] [int] NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([Id], [Username], [Role], [Likes], [Password]) VALUES (1, N'admin', N'admin', 8, N'admin')
INSERT [dbo].[User] ([Id], [Username], [Role], [Likes], [Password]) VALUES (2, N'user1', N'user1', 7, N'user1')
INSERT [dbo].[User] ([Id], [Username], [Role], [Likes], [Password]) VALUES (3, N'staff1', N'staff1', 8, N'staff1')
INSERT [dbo].[User] ([Id], [Username], [Role], [Likes], [Password]) VALUES (4, N'staff2', N'staff2', 5, N'staff2')
SET IDENTITY_INSERT [dbo].[User] OFF
/****** Object:  StoredProcedure [dbo].[USP_AddNewPost]    Script Date: 06/03/2016 19:42:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Alessio Di Salvo>
-- Create date: <06/03/2016>
-- Description:	<Add new post>
-- =============================================
CREATE PROCEDURE [dbo].[USP_AddNewPost]
	@Title nvarchar(150),
	@Author nvarchar(150),
	@Body nvarchar(MAX),
	@PublishDate datetime

AS
BEGIN
	
	BEGIN TRY

		SET NOCOUNT ON;

		INSERT  INTO Post (Title, Author, Body, PublishDate) 
				VALUES (@Title, @Author, @Body, @PublishDate)

		SELECT @@IDENTITY as PostId

	END TRY

	BEGIN CATCH

		SELECT 0 as PostId

	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[USP_DeletePost]    Script Date: 06/03/2016 19:42:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Alessio Di Salvo>
-- Create date: <06/03/2016>
-- Description:	<Delete post>
-- =============================================
CREATE PROCEDURE [dbo].[USP_DeletePost]
	@PostId int
	
AS
BEGIN
		
	BEGIN TRY

		SET NOCOUNT ON;

		DELETE FROM  Post 
		WHERE Id = @PostId

		SELECT 1 AS PostDeleted

	END TRY
	
	BEGIN CATCH
		SELECT 0 AS PostDeleted
	END CATCH

END
GO
/****** Object:  StoredProcedure [dbo].[USP_EditPost]    Script Date: 06/03/2016 19:42:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Alessio Di Salvo>
-- Create date: <06/03/2016>
-- Description:	<Edit post>
-- =============================================
CREATE PROCEDURE [dbo].[USP_EditPost]
	@PostId int,
	@Author nvarchar(150),
	@Title nvarchar(150),
	@Body nvarchar(MAX)
	
AS
BEGIN
		
	BEGIN TRY

		SET NOCOUNT ON;

		UPDATE  Post 
		SET Title = @Title, Author = @Author, Body = @Body 
		WHERE Id = @PostId

		SELECT 1 AS PostUpdated

	END TRY
	
	BEGIN CATCH
		SELECT 0 AS PostUpdated
	END CATCH

END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllComments]    Script Date: 06/03/2016 19:42:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Alessio Di Salvo>
-- Create date: <06/03/2016>
-- Description:	<Get All Comments>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetAllComments]
	@PostId int
	
AS
BEGIN
		
	BEGIN TRY

		SET NOCOUNT ON;

		SELECT Id, [Text], Username
		FROM [Comment]
		WHERE PostId = @PostId 

	END TRY
	
	BEGIN CATCH
		SELECT 0
	END CATCH

END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllPosts]    Script Date: 06/03/2016 19:42:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Alessio Di Salvo>
-- Create date: <06/03/2016>
-- Description:	<Get All Posts>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetAllPosts]
	
	
AS
BEGIN
		
	BEGIN TRY

		SET NOCOUNT ON;

		SELECT Id, Title, Body, PublishDate, Author, Likes
		FROM [Post] 

	END TRY
	
	BEGIN CATCH
		SELECT 0
	END CATCH

END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetById]    Script Date: 06/03/2016 19:42:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Alessio Di Salvo>
-- Create date: <06/03/2016>
-- Description:	<Get Post by Id>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetById]
	@Id int
	
AS
BEGIN
		
	BEGIN TRY

		SET NOCOUNT ON;

		SELECT Id, Title, Body, PublishDate, Author, Likes
		FROM [Post] 
		WHERE Id = @Id

	END TRY
	
	BEGIN CATCH
		SELECT 0
	END CATCH

END
GO
/****** Object:  StoredProcedure [dbo].[USP_Login]    Script Date: 06/03/2016 19:42:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Alessio Di Salvo>
-- Create date: <06/03/2016>
-- Description:	<Edit post>
-- =============================================
CREATE PROCEDURE [dbo].[USP_Login]
	@Username nvarchar(150),
	@Password nvarchar(150)
	
AS
BEGIN
		
	BEGIN TRY

		SET NOCOUNT ON;

		SELECT Id, Username, [Role], Likes, [Password]
		FROM [User] 
		WHERE Username = @Username AND Password = @Password 

	END TRY
	
	BEGIN CATCH
		SELECT 0
	END CATCH

END
GO
/****** Object:  StoredProcedure [dbo].[USP_NewComment]    Script Date: 06/03/2016 19:42:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Alessio Di Salvo>
-- Create date: <06/03/2016>
-- Description:	<Add new comment>
-- =============================================
CREATE PROCEDURE [dbo].[USP_NewComment]
	@PostId int,
	@Text nvarchar(MAX),
	@Username nvarchar(150)

AS
BEGIN
	
	BEGIN TRY

		SET NOCOUNT ON;

	INSERT INTO Comment 
           (PostId, [Text], Username)
     VALUES
           (@PostId, @Text, @Username)
		
		SELECT 1 AS CommentInserted

	END TRY

	BEGIN CATCH

		SELECT 0 as CommentInserted

	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[USP_NewLike]    Script Date: 06/03/2016 19:42:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Alessio Di Salvo>
-- Create date: <06/03/2016>
-- Description:	<Add new like>
-- =============================================
CREATE PROCEDURE [dbo].[USP_NewLike]
	@PostId int,
	@UserId int
AS
BEGIN
	
	BEGIN TRY

		SET NOCOUNT ON;

		DECLARE @Likes int = 0
		DECLARE @UserLikes int = 0

		SELECT @UserLikes = Likes FROM [User] WHERE Id = @UserId

		IF(@UserLikes > 0)
			BEGIN

				UPDATE  [User] SET Likes = (@UserLikes - 1) WHERE  Id = @UserId

				SELECT @Likes = Likes FROM Post WHERE Id = @PostId
				UPDATE Post SET Likes = (@Likes + 1) WHERE Id = @PostId

				SELECT 1 AS LikeInserted
			END
		ELSE
			BEGIN
				SELECT 0 AS LikeInserted
			END

	END TRY

	BEGIN CATCH

		SELECT 0 as LikeInserted

	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[USP_NewPost]    Script Date: 06/03/2016 19:42:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Alessio Di Salvo>
-- Create date: <06/03/2016>
-- Description:	<Add new post>
-- =============================================
CREATE PROCEDURE [dbo].[USP_NewPost]
	@Author nvarchar(150),
	@Body nvarchar(MAX),
	@Title nvarchar(150)

AS
BEGIN
	
	BEGIN TRY

		SET NOCOUNT ON;

	INSERT INTO [Post] 
           ([Title], [Body], [PublishDate], [Author], [Likes])
     VALUES
           (@Title, @Body, GETDATE(), @Author, 0)
		
		SELECT 1 AS PostInserted

	END TRY

	BEGIN CATCH

		SELECT 0 as PostInserted

	END CATCH
END
GO
