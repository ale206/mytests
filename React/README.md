# xxxxUI

[![Build status](https://ci.appveyor.com/api/projects/status/9rlemjx9touscd2b/branch/master?svg=true)](https://ci.appveyor.com/project/xxxx/xxxxui/branch/master)
[![Coverage Status](https://coveralls.io/repos/xxxx/xxxxUI/badge.svg?branch=master&service=github&t=CycukY)](https://coveralls.io/github/xxxx/xxxxUI?branch=master)


# Quick start
1. Clone the repository and run `npm install` to downlaod the dependencies for the project.
2. Run `gulp run` to start a local server, and launch the xxxx UI playground whereby you can view a list of example components based on the source files

# Running tests
Tests can be run in three ways
 - `gulp test` runs tests in default mode against phantom js browser. This is the fastest way to run your tests. 
 - `gulp test --debug true` runs tests in debug mode. Launches chrome so that you can debug using chrome tools
 - `gulp test --release true` runs tests in a range of browsers.
