'use strict';
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var config = require('./config');

// Static files
gulp.task('assets', function () {

  return gulp.src(config.SRC.assets)
    .pipe($.changed(config.DEST))
    .pipe(gulp.dest(config.DEST + '/assets'))
    .pipe($.size({title: 'assets'}));
});
