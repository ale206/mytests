'use strict';
var gulp = require('gulp');
var runSequence = require('run-sequence');
var argv = require('minimist')(process.argv.slice(2));
var RELEASE = argv.release;
var tasks = ['lint', 'checkspecs', 'fonts', 'assets', 'pages', 'styles', 'external-less', 'bundle', 'components'];

// Build the app from source code
gulp.task('build', ['clean'], function(cb) {
  runSequence(tasks, function(){
    gulp.start('vendor');
    cb();
  });
});
