'use strict';
let gulp = require('gulp');
let webpack = require('webpack');
let gutil     = require('gulp-util');
let config = require('./config');

let defaultErrorHandler = function(err, ctx) {
  throw new gutil.PluginError(ctx, err);
};

// Bundle
gulp.task('bundle', function(cb, handlers) {
  handlers = handlers || {};
  handlers = {
    onFatalError: handlers.onFatalError || defaultErrorHandler,
    onError: handlers.onError || defaultErrorHandler,
    onWarning: handlers.onWarning || defaultErrorHandler
  };
  let started = false;
  let webpackConfig = require('../../config/webpack.js')(config.RELEASE);
  let bundler = webpack(webpackConfig);

  function bundle(err, stats) {
    if (err) return handlers.onFatalError(err, 'Fatal error');
    let jsonStats = stats.toJson();
    if (!jsonStats) {

    }
    if (jsonStats.errors.length > 0) {
      return handlers.onError(jsonStats.errors.toString(), 'Stats error');
    }
    if (jsonStats.warnings.length > 0) {
      return handlers.onWarning(jsonStats.warnings.toString(), 'Stats warning');
    }

    if (!started) {
      started = true;
      return cb();
    }
  }
  return config.WATCH ?  bundler.watch(200, bundle) : bundler.run(bundle);
});
