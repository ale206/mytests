'use strict';
var gulp = require('gulp');
var del = require('del');
var DEST = require('./config').DEST;
var EXAMPLES = require('./config').EXAMPLES;
var argv = require('minimist')(process.argv.slice(2));
var RELEASE = argv.release;
var toDelete = [DEST, EXAMPLES];
if (RELEASE) {
  toDelete = [DEST];
}

// Clean up
gulp.task('clean', del.bind(null, toDelete));
