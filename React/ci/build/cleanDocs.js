'use strict';
var gulp = require('gulp');
var del = require('del');
var docs = require('./config').DOCS;

// Clean up
gulp.task('cleanDocs', del.bind(null, docs));
