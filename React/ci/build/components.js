'use strict';
var gulp         = require('gulp');
var gutil        = require('gulp-util');
var config       = require('./config');
var webpack      = require('webpack');
var commonConfig = require('../../config/webpack.js')(config.RELEASE);
var glob         = require("glob");
var path         = require('path');
Object.assign    = require('object-assign');

gulp.task("components", function(callback) {

  // options is optional
  glob("src/components/**/index.js", null , function (er, configFiles) {
    // files is an array of filenames.
    configFiles.forEach(function(f, i){
      var filePath = path.join('../../', f);

      var outputPath = filePath.substring(0, filePath.lastIndexOf("\\")).replace('..\\..\\src', 'build');

      var webpackConfig = Object.assign(commonConfig, require(filePath), {externals: {
          "react": "React",
          "react-dom": "ReactDOM",
          "react-data-grid": "ReactDataGrid",
          "moment" : "moment",
          "underscore": "_",
          "jquery" : 'jQuery',
          "jQuery" : 'jQuery'
        }});
      webpackConfig.plugins = [new webpack.DefinePlugin({'translate': 'window.xxx._', 'pluralTranslate': 'window.xxx.__'})];
      webpackConfig.output = {
        path : outputPath,
        filename : '[name].js',
        publicPatch : './build/',
        libraryTarget: "umd"
      }

      webpack(webpackConfig, function(err, stats) {
      //  if(f=='src/components/User/Contact/index.js') log(f+ ' err?' + err + ' stats:' + stats)
        if(err) throw new gutil.PluginError("webpack", err);
        var jsonStats = stats.toJson();
        if(jsonStats) {
          if(jsonStats.errors.length > 0) {
            throw new gutil.PluginError("webpack",f +jsonStats.errors.toString());
          }
          if(jsonStats.warnings.length > 0) {
            throw new gutil.PluginError("webpack",f + jsonStats.warnings.toString());
          }
        }
        if(i === configFiles.length - 1){
          callback();
        }

      });
    });
  });



});
