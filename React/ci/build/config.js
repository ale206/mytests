var argv = require('minimist')(process.argv.slice(2));
module.exports = {

  SRC : {
    assets :'src/assets/**',
    styles : 'src/styles/**/*.{css,less}',
    pages : 'src/pages/**/*.html'
  },
  DEST : './build',
  VENDOR: './build/vendor',
  DOCS : './docs',
  EXAMPLES: './examples/build',
  WATCH : false,
  RELEASE : !!argv.release
};
