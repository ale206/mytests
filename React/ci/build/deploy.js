'use strict';
var gulp = require('gulp');
var karma = require('karma').server;
var argv = require('minimist')(process.argv.slice(2));
var del = require('del');
var $ = require('gulp-load-plugins')();
var DEST = require('./config').DEST;

// Deploy to GitHub Pages
gulp.task('deploy', function () {

  // Remove temp folder
  if (argv.clean) {
    var os = require('os');
    var path = require('path');
    var repoPath = path.join(os.tmpdir(), 'tmpRepo');
    $.util.log('Delete ' + $.util.colors.magenta(repoPath));
    del.sync(repoPath, {force: true});
  }

  return gulp.src(DEST + '/**/*')
    .pipe($.if('**/robots.txt', !argv.production ? $.replace('Disallow:', 'Disallow: /') : $.util.noop()))
    .pipe($.ghPages({
      remoteUrl: 'https://github.com/{name}/{name}.github.io.git',
      branch: 'master'
    }));
});
