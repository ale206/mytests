'use strict';
var gulp = require('gulp');
var webpack = require('webpack');
var gutil     = require('gulp-util');
var config = require('./config');
var eslint = require('gulp-eslint');

var defaultErrorHandler = function(err, ctx) {
  throw new gutil.PluginError(ctx, err);
};

gulp.task('examples-lint', function() {
    return gulp.src(['examples/js'])
        // eslint() attaches the lint output to the eslint property
        // of the file object so it can be used by other modules.
        .pipe(eslint())
        // eslint.format() outputs the lint results to the console.
        // Alternatively use eslint.formatEach() (see Docs).
        .pipe(eslint.format())
        // To have the process exit with an error code (1) on
        // lint error, return the stream and pipe to failOnError last.
        .pipe(eslint.failOnError());
});

// Bundle
gulp.task('examples', ['styles'], function (cb, handlers) {
  handlers = handlers || {};
  handlers = {
    onFatalError: handlers.onFatalError || defaultErrorHandler,
    onError: handlers.onError || defaultErrorHandler,
    onWarning: handlers.onWarning || defaultErrorHandler
  };
  var started = false;
  var webpackConfig = require('../../examples/webpack.js')(config.RELEASE);
  var bundler = webpack(webpackConfig);

  function bundle(err, stats) {
    if(err) return handlers.onFatalError(err, 'Fatal error');
    var jsonStats = stats.toJson();
    if(!jsonStats) {

    }
    if(jsonStats.errors.length > 0) {
      return handlers.onError(jsonStats.errors.toString(), 'Stats error');
    }
    if(jsonStats.warnings.length > 0) {
      return handlers.onWarning(jsonStats.warnings.toString(), 'Stats warning');
    }

    if (!started) {
      started = true;
      return cb();
    }
  }
  return config.WATCH ?  bundler.watch(200, bundle) : bundler.run(bundle);
});
