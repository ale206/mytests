'use strict';
var gulp = require('gulp');
var DEST = require('./config').DEST;

// fonts
gulp.task('fonts', function () {
  return gulp.src(['./node_modules/bootstrap/dist/fonts/**', './node_modules/font-awesome/fonts/**' ])
      .pipe(gulp.dest(DEST + '/fonts'));
});
