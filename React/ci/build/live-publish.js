var run = require('gulp-run');
var gulp = require('gulp');

gulp.task('master-publish', function (cb) {
  var cmd = new run.Command('./ci/publish/master-publish.ps1', {verbosity: 3, usePowerShell: true});
  cmd.exec(null, function(){
    cb();
  });
})
