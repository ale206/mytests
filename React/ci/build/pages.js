'use strict';
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var config = require('./config');

// HTML pages
gulp.task('pages', function () {
  return gulp.src(config.SRC.pages)
    .pipe($.changed(config.DEST))
    .pipe($.if(config.RELEASE, $.htmlmin({
      removeComments: true,
      collapseWhitespace: true,
      minifyJS: true
    })))
    .pipe(gulp.dest(config.DEST))
    .pipe($.size({title: 'pages'}));
});
