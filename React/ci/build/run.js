var gulp = require('gulp');
var browserSync = require('browser-sync');

var PORT = process.env.PORT || 3000;

gulp.task('serve', ['examples'], function() {
  var dir = './examples';
  browserSync({
    server: {
      baseDir: dir
    }
  });
  gulp.watch("./src/**/*.js", ['js-watch']);
  gulp.watch("./src/**/*.jsx", ['js-watch']);

  //Watch for 'examples' folder, excluding 'build' folder inside it
  gulp.watch(['./examples/**/*.js', '!./examples/build/**/*.*'], ['js-watch']);
});

gulp.task('js-watch', ['examples'], browserSync.reload);

gulp.task('run', ['build'], function(dir) {
  gulp.start('serve');
});
