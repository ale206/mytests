'use strict';
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();

var config = require('./config');
var AUTOPREFIXER_BROWSERS = [                 // https://github.com/ai/autoprefixer
  'ie >= 10',
  'ie_mob >= 10',
  'ff >= 30',
  'chrome >= 34',
  'safari >= 7',
  'opera >= 23',
  'ios >= 7',
  'android >= 4.4',
  'bb >= 10'
];


// CSS style sheets
gulp.task('styles', function () {
  return gulp.src([
    'src/styles/mediaplan.less',
    'src/styles/reconciliation.less',
    'node_modules/font-awesome/css/font-awesome.css', 'src/styles/data-grid.less' ])
    .pipe($.plumber())
    .pipe($.less({
      sourceMap: !config.RELEASE,
      sourceMapBasepath: __dirname
    }))
    .on('error', console.error.bind(console))
    .pipe($.autoprefixer({browsers: AUTOPREFIXER_BROWSERS}))
    .pipe($.csscomb())
    .pipe($.if(config.RELEASE, $.minifyCss()))
    .pipe(gulp.dest(config.DEST + '/css'))
    .pipe(gulp.dest(config.EXAMPLES + '/css'))
    .pipe($.size({title: 'styles'}));
});

gulp.task('external-less', function() {
    gulp.src([
    'src/styles/data-grid.less'])
    .pipe(gulp.dest(config.DEST + '/less'));
    
  return gulp.src([
    'node_modules/react-select/less/*.less'], {base: 'node_modules'})
    .pipe($.plumber())
    .pipe(gulp.dest(config.DEST + '/less'))
    .pipe($.size({title: 'external-less'}));
});
