'use strict';
var gulp = require('gulp');
var VENDOR = require('./config').VENDOR;
var rename = require("gulp-rename");
var del = require('del');

// fonts
gulp.task('vendor', function () {
  gulp.src('./node_modules/classnames/index.js')
    .pipe(rename("classnames.js"))
      .pipe(gulp.dest(VENDOR));

      // move vendor base
      gulp.src('./build/vendor.bundle.js')
          .pipe(rename("vendor.js"))
          .pipe(gulp.dest(VENDOR));
        del(['./build/vendor.bundle.js']);

  // Separate react-data-grid UI plugins.
  gulp.src('./node_modules/react-data-grid/dist/react-data-grid.ui-plugins.js')
    .pipe(gulp.dest(VENDOR));

  return gulp.src(['./node_modules/react-bootstrap/dist/react-bootstrap.js', './node_modules/bootstrap-daterangepicker/daterangepicker.js' ])
      .pipe(gulp.dest(VENDOR));


});
