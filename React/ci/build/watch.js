'use strict';
var gulp = require('gulp');
var runSequence = require('run-sequence');
var path = require('path');
var config = require('./config');



// Launch a lightweight HTTP Server
gulp.task('watch', ['build'], function (cb) {
  //TODO we need to pass this through for bundle
  //should move to using dependencies, and deps with args (once gulp4 is out)
  config.WATCH = true;

    gulp.watch(config.SRC.assets, ['assets']);
    gulp.watch(config.SRC.images, ['images']);
    gulp.watch(config.SRC.pages, ['pages']);
    gulp.watch(config.SRC.styles, ['styles']);
    gulp.watch(config.DEST + '/**/*.*', ['bundle']);
    return cb();
});
