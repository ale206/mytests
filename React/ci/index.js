"use strict";
var fs = require('fs');
var onlyScripts = require('./util/scriptFilter');
var buildTasks = fs.readdirSync('./ci/build/').filter(onlyScripts);
var testTasks = fs.readdirSync('./ci/test/');

buildTasks.forEach(function(task) {
	require('./build/' + task);
});

testTasks.forEach(function(task) {
	require('./test/' + task);
});
