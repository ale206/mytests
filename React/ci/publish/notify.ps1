Function PostToSlack
{
	Param ([string]$logmessage)
	$notificationPayload = @{ 
	text=$logmessage; 
	username="rex"; 
	icon_url="https://s3-us-west-2.amazonaws.com/slack-files2/avatars/2015-02-03/3601434084_93dffc1e2ced06b484f7_72.jpg"
	} 
	Invoke-RestMethod -Uri "https://hooks.slack.com/services/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" -Method Post -Body (ConvertTo-Json $notificationPayload) 
}

PostToSlack "<!channel> ###UI - master build failed. Time to fix it!"
