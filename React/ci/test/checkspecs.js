var fsTools = require('fs-tools');
var gulp = require('gulp');
var gutil = require('gulp-util');
var fs = require('fs');
var pathUtils = require('path');

// Static files
gulp.task('checkspecs', function (done) {
  var srcPath = './src'; // Where are your scripts?
  var testFolder= '__tests__\\'; //Where are your specs?
  var excluded = ['spec.js', '.spec.jsx']
  var missingSpecs = [];
  fsTools.walk(srcPath, /\.js$|.jsx$/, function(path,stats,callback){

    if (path.indexOf('__tests__') === -1 &&
        path.indexOf('testUtils') === -1 &&
        path.indexOf('testRunners') === -1 &&
        path.indexOf('MockServices') === -1 &&
        path.indexOf('app.jsx') === -1 &&
        path.indexOf('index.js') === -1) {
      var fileName = pathUtils.basename(path);
      var dirname = path.replace(fileName, '');
      var fileExtension = pathUtils.extname(fileName);
      var fileBaseName = pathUtils.basename(path, fileExtension);
      var specPath = dirname + testFolder + fileBaseName + '.spec' + fileExtension;
      if(!fs.existsSync(specPath)) {
        missingSpecs.push({path: path, expected: specPath});
      }
    }
    callback();
  }, function (err){
    if(err){
      gutil.log(gutil.colors.red(err));
      done(false);
    }
    if(missingSpecs.length > 0){
      //gutil.log(gutil.colors.red('Missing test file'));
      missingSpecs.forEach(function(spec){
        gutil.log(gutil.colors.red('Missing SPEC file for ') + gutil.colors.red(spec.path));
        gutil.log('Was expecting ' + spec.expected);
      });
      process.exit(1)
    } else {
     gutil.log('Spec files present for all source files');
    }
    done();
  });
});
