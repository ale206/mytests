var re = /^\/\/#\ssourceMappingURL(.+)$/gm;

module.exports = function(src) {
    this.cacheable && this.cacheable();
    return src.replace(re, '');
}
