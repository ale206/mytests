/*!
* Facebook React Starter Kit | https://github.com/kriasoft/react-starter-kit
* Copyright (c) KriaSoft, LLC. All rights reserved. See LICENSE.txt
*/

/*
* Webpack configuration. For more information visit
* http://webpack.github.io/docs/configuration
*/

'use strict';

var webpack = require('webpack');
var path    = require('path');

module.exports = function (release) {


  function getPlugins() {
    var nodeEnv = release ? '"production"' : '"development"';
    var pluginsBase =  [
    new webpack.DefinePlugin({'process.env.NODE_ENV': nodeEnv, 'translate': 'window.xxx._', 'pluralTranslate': 'window.xxx.__'})
  ];
  pluginsBase.push(new webpack.optimize.CommonsChunkPlugin(/* chunkName= */"vendor", /* filename= */"vendor.bundle.js"));
  if (release) {
    pluginsBase.push(new webpack.optimize.DedupePlugin());
    pluginsBase.push(new webpack.optimize.OccurenceOrderPlugin());
    pluginsBase.push(new webpack.optimize.AggressiveMergingPlugin());
  }
  return pluginsBase;
  };

  return {
    cache: !release,
    debug: !release,
    devtool: false,
    // entry: {
    //   app: './src/modules/mediaplan/index.jsx',
    //   utils: ['./src/utils/index.js'],
    //   vendor: ['es5-shim/es5-shim.min.js', 'es5-shim/es5-sham.min.js', 'es6-shim/es6-shim.min.js', './vendor.js']
    // },

    output: {
      path: './build/',
      filename: '[name].js',
      publicPath: './node_modules/@xxx/xxx-ui/build/',
      libraryTarget: "umd"
    },
    externals: [{
      "jquery" : 'jQuery',
      "jQuery" : 'jQuery',
      "_": "underscore"
    },
    /translate$/],
    stats: {
      colors: true,
      reasons: !release
    },

    plugins: getPlugins(),

    resolve: {
      extensions: ['', '.webpack.js', '.web.js', '.js', '.jsx', '.json']
    },
    resolveLoader: {
      alias: {
        'strip-sourecemap': path.join(__dirname, '../ci/util/strip-sourcemap.js')
      }
    },
    amd: { globalize : false },
    module: {
      loaders: [
      {
        test: /\.js|.jsx/,
        exclude: /node_modules/,
        loader:  'babel-loader?optional[]=runtime'
      },
      {
        test: /\.css$/,
        loader: 'style!css'
      },
      {
        test: /\.less$/,
        loader: 'style!css!less'
      },
      {
        test: /\.gif/,
        loader: 'url-loader?limit=10000&mimetype=image/gif'
      },
      {
        test: /\.jpg/,
        loader: 'url-loader?limit=10000&mimetype=image/jpg'
      },
      {
        test: /\.png/,
        loader: 'url-loader?limit=10000&mimetype=image/png'
      },
      {
        test: /\.json/,
        loader: 'json-loader'
      },
      {
        test: /[\/\\]node_modules[\/\\]globalize[\/\\]dist[\/\\]*.js|[\/\\]node_modules[\/\\]globalize[\/\\]dist[\/\\]globalize[\/\\]*.js/,
        loader: "imports?define=>false"
      },
      {
        test: /\.js$/,
        loader: 'strip-sourecemap'
      }
      ]
    }
  };
};
