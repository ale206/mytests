`BudgetBar` (component)
=======================
[view code](../src/modules/mediaPlan/js/components/Header/BudgetBar.jsx)


Props
-----

### `overBudget` (required)

type: `bool`


### `percentage` (required)

type: `number`

