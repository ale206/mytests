`MediaPlanGrid` (component)
===========================
[view code](../src/modules/mediaPlan/js/components/MediaPlanGrid.jsx)


Props
-----

### `minHeight` (required)

type: `number`
defaultValue: `500`

