window._ = require('underscore');
window.moment = require('moment');

import xxxxFlux from './js/app.js';

jQuery(function() {
  let app = new xxxxFlux({
    container: document.getElementById('appContainer')
  });

  app.start();
});
