import React from 'react';
import MyFacebookLogin from '../../src/components/Trafficking/Login/ExternalSystems/MyFacebookLogin';

const responseFacebook = (response) => {
  console.log(response);

  // this is the token we want to use in our credentials
  console.log(response.longLivedToken);
};

const example = React.createClass({

  render() {
    return (
  <MyFacebookLogin
    appId="299672110242158"
    callback= {responseFacebook}
    appSecretId="e2c63b82667cb3daafe2ffc5b05f93b2"
    scope="ads_read,ads_management,read_insights"
    fields="name"
    language="en_GB"
  />);
  }
});

export default example;
