import React, { PropTypes } from 'react';

//grab whatever component you wnat here
import LoadingGraphic from '../../src/components/General/ContainerLoader.js';

  //ACTIONS
  const COMP_POT_ACTIONS = {
    GET_POT:null
  }
  let dispatch = function(action, payload) {
    switch(action) {
      case COMP_POT_ACTIONS.GET_POT:
        compPotStore.gotPot(payload.id, payload.pot)
        break;

    }
  }

  let api = {
    compPots: {
      get: function(id, success, err) {
        let apiLoadPot = function() {
          let res = compPotStore.getPot(id) || {errors:[]};
          if(res.errors.length>3) {
            res.pot={id};
            success(res);
          }
          else {
            res.errors.push({msg:'No network connection'})
            err(res);
            setTimeout(apiLoadPot,1000);
          }
        }
        setTimeout(apiLoadPot,1000);
      }
    }
  }
  //ACTION CREATORS
  let compPotActionCreator = {
    getPot: function(id) {
      let pot = compPotStore.getPot(id);
      if(pot && pot.id) {
        //good to go
        dispatch(COMP_POT_ACTIONS.GOT_POT, {id,pot})
      }
      else {
        //load it
        api.compPots.get(id,
          (res) => {dispatch(COMP_POT_ACTIONS.GET_POT, {id,pot:res});},
          (err) => {dispatch(COMP_POT_ACTIONS.GET_POT, {id,pot:err}); } )

      }
    }
  }
  //STORES

  // class ServiceResult {
  //   //debug info? ie url / params
  //   data: Object;//raw response
  //   errors: List<ServiceError>
  //   get isLoaded() { return this.data !== null}
  // }
  // class PotResult extends ServiceResult {
  //   pot: CompensationPot;
  // }
  let compPotStore = {

    potResults:[], //List<PotResult>
    getPot: function(id) {
      return this.potResults[id];
    },
    gotPot: function(id, result) {
      this.potResults[id]=result;
      if(this.changeHandler) this.changeHandler();//aka emit
    },
    onChange: function(fn) {
      this.changeHandler = fn;
    }

  }



  //COMPONENTS
  let CompPot = React.createClass({
    getInitialState: function() {
      //need to load up the data
      return this.loadDataFromStores();
    },
    componentDidMount: function() {
      compPotStore.onChange(this.onCompPotStoreChange);
    },
    onCompPotStoreChange: function() {
      this.setState(this.loadDataFromStores());
    },
    loadDataFromStores: function() {
      return {pot:compPotStore.getPot(this.props.id).pot}
    },
    render: function() {
      return <div>Pot {this.state.pot.id}</div>
    }
  });

  let LoadingContainer = React.createClass({
    getDefaultProps: function() {
      return {isLoadedCheck:(res) => res.data!=null }
    },
    getInitialState: function() {
      return {isLoaded:false, errors:[]}
    },
    componentDidMount: function() {
      if(this.props.initialLoad) { this.props.initialLoad(); }
      if(this.props.changeListener) { this.props.changeListener(this.onChange); }
    },
    onChange: function() {
      let res = this.props.loadData();
      this.setState({errors: res.errors, isLoaded: this.props.isLoadedCheck(res)});
    },
    render: function() {
      if(!this.state.isLoaded) {
        let errors = this.state.errors && (<div>{this.state.errors.length} errors</div>)
        return (<div>{errors}<LoadingGraphic /> </div>)
      }
      return <div>{this.props.children}</div>
    }
  });

  let Fiddle = React.createClass({
    getDefaultProps: function() {
      return {potId:23}
    },
    render: function() {
      let initialLoad = () => compPotActionCreator.getPot(this.props.potId);
      let loadData = () => compPotStore.getPot(this.props.potId);
      let changeListener = (fn) => compPotStore.onChange(fn);
      return (<div><LoadingContainer initialLoad={initialLoad}
        changeListener={changeListener}
        loadData={loadData}
        isLoadedCheck={(res) => res.pot != null}><CompPot id={this.props.potId} /></LoadingContainer></div>)
    }
  });

  export default Fiddle;
