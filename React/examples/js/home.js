import React from 'react';

export default React.createClass({
    render() {
      return (
        <div>
          <p>This is designed to give a quick and easy place for you to try stuff out</p>
          <p>To get started, edit examples.js, then run it using <pre>gulp run</pre> and then <a href="localhost:3000/#/">hit the page</a></p>
          <p>You can add different examples using routes. If you do, add a link in getAppContainer</p>
          <p>We DONT watch for file changes under so you need to re-run gulp run if you make changes, or run gulp examples in another cmd and hit reload</p>
        </div>);
      }
  });
