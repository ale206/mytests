import React from 'react';
import ReactDOM from 'react-dom';
// grab whatever component you wnat here
import PlaygroundComponent from './FluxExample.js';

function getComponent() {
  return React.createClass({
    getDefaultProps: function() {
      return {};
    },

    openModal: function() {
      ReactDOM.render(
        <MediaPlanReview />, document
      );
    },

    render: function() {
      // add any fake props in here
      return (
        <div>
          <h2>Here is a loading image. Exciting, isn't it!</h2><PlaygroundComponent {...this.props} />
          <button onClick={ this.openModal }>'Click me!'</button>
        </div>
      );
    }
  });
}
export default getComponent;
