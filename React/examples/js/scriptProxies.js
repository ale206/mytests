
import BookingAttributesServices from './testServices/BookingAttributes';
import BookingCategoriesServices from './testServices/BookingCategories';
import BeCampaignServices from './testServices/BeCampaigns';
import CampaignAvailableCommissionsServices from './testServices/CampaignAvailableCommissions';
import TargetingServices from './testServices/TargetingServices';
import SupplierOptionsServices from './testServices/SupplierOptionsServices';

window.xxx = window.xxx || {};
window.xxx._ = (input) => input;
xxx.api = TargetingServices;
xxx.Services = xxx.Services || {};
xxx.Services.BookingAttributes = BookingAttributesServices;
xxx.Services.BookingCategories = BookingCategoriesServices;
xxx.Services.BeCampaign = BeCampaignServices;
xxx.Services.ClientCommission = CampaignAvailableCommissionsServices;
xxx.Services.Model = xxx.Services.Model || {};
xxx.Services.CampaignAdservingTypes= function(){};
xxx.Services.Model.CampaignAdservingTypes=function(){};
xxx.Services.CampaignAdservingTypes.GetAdservers=function(successCallBack,ajaxSettings){

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/GetAdservers', {}, ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.GetCampaignAdserverBookingsWithProviders=function(campaignId, includeAssignedToAdservers,successCallBack,ajaxSettings){
var dataObject = new Object();
dataObject.campaignId = campaignId;
dataObject.includeAssignedToAdservers = includeAssignedToAdservers;

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/GetCampaignAdserverBookingsWithProviders', JSON.stringify(dataObject), ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.GetCampaignAdservingTypes=function(campaignId,successCallBack,ajaxSettings){
var dataObject = new Object();
dataObject.campaignId = campaignId;

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/GetCampaignAdservingTypes', JSON.stringify(dataObject), ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.AddNewAdservingType=function(type, token,successCallBack,ajaxSettings){
var dataObject = new Object();
dataObject.type = type;
dataObject.token = token;

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/AddNewAdservingType', JSON.stringify(dataObject), ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.AddCommonlyUsedTypes=function(token,successCallBack,ajaxSettings){
var dataObject = new Object();
dataObject.token = token;

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/AddCommonlyUsedTypes', JSON.stringify(dataObject), ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.AddFromStoredRates=function(storedRates, selectedAdserverParentSiteId, defaultItemId, token,successCallBack,ajaxSettings){
var dataObject = new Object();
dataObject.storedRates = storedRates;
dataObject.selectedAdserverParentSiteId = selectedAdserverParentSiteId;
dataObject.defaultItemId = defaultItemId;
dataObject.token = token;

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/AddFromStoredRates', JSON.stringify(dataObject), ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.UpdateAdservingType=function(type, token,successCallBack,ajaxSettings){
var dataObject = new Object();
dataObject.type = type;
dataObject.token = token;

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/UpdateAdservingType', JSON.stringify(dataObject), ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.DeleteAdservingType=function(typeId, token, isCombo,successCallBack,ajaxSettings){
var dataObject = new Object();
dataObject.typeId = typeId;
dataObject.token = token;
dataObject.isCombo = isCombo;

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/DeleteAdservingType', JSON.stringify(dataObject), ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.GetAvailableStoredRates=function(serverId, unitType, token,successCallBack,ajaxSettings){
var dataObject = new Object();
dataObject.serverId = serverId;
dataObject.unitType = unitType;
dataObject.token = token;

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/GetAvailableStoredRates', JSON.stringify(dataObject), ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.GetTechCostModes=function(successCallBack,ajaxSettings){

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/GetTechCostModes', {}, ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.SyncWithCentralRates=function(token,successCallBack,ajaxSettings){
var dataObject = new Object();
dataObject.token = token;

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/SyncWithCentralRates', JSON.stringify(dataObject), ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.GetCentralRatesSyncStatus=function(token,successCallBack,ajaxSettings){
var dataObject = new Object();
dataObject.token = token;

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/GetCentralRatesSyncStatus', JSON.stringify(dataObject), ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.IgnoreSyncStatus=function(token,successCallBack,ajaxSettings){
var dataObject = new Object();
dataObject.token = token;

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/IgnoreSyncStatus', JSON.stringify(dataObject), ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.GetApplicableTechProviderContracts=function(token,successCallBack,ajaxSettings){
var dataObject = new Object();
dataObject.token = token;

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/GetApplicableTechProviderContracts', JSON.stringify(dataObject), ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.AddCampaignRatesFromStoredContracts=function(storedRates, token,successCallBack,ajaxSettings){
var dataObject = new Object();
dataObject.storedRates = storedRates;
dataObject.token = token;

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/AddCampaignRatesFromStoredContracts', JSON.stringify(dataObject), ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.AddCampaignRatesFromStoredContractsAsCombo=function(selectedRates, token, comboName,successCallBack,ajaxSettings){
var dataObject = new Object();
dataObject.selectedRates = selectedRates;
dataObject.token = token;
dataObject.comboName = comboName;

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/AddCampaignRatesFromStoredContractsAsCombo', JSON.stringify(dataObject), ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.AddCampaignRatesFromStoredCombos=function(storedCombos, token,successCallBack,ajaxSettings){
var dataObject = new Object();
dataObject.storedCombos = storedCombos;
dataObject.token = token;

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/AddCampaignRatesFromStoredCombos', JSON.stringify(dataObject), ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.AddCommonlyUsedRatesAndCombos=function(token,successCallBack,ajaxSettings){
var dataObject = new Object();
dataObject.token = token;

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/AddCommonlyUsedRatesAndCombos', JSON.stringify(dataObject), ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.AddFavouriteRatesAndCombos=function(token,successCallBack,ajaxSettings){
var dataObject = new Object();
dataObject.token = token;

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/AddFavouriteRatesAndCombos', JSON.stringify(dataObject), ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.GetListCostModels=function(successCallBack,ajaxSettings){

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/GetListCostModels', {}, ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.GetListRateType=function(successCallBack,ajaxSettings){

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/GetListRateType', {}, ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.GetListSpendType=function(successCallBack,ajaxSettings){

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/GetListSpendType', {}, ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.GetListCurrencyCodes=function(successCallBack,ajaxSettings){

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/GetListCurrencyCodes', {}, ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.GetListUnitTypes=function(successCallBack,ajaxSettings){

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/GetListUnitTypes', {}, ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.GetListBillableCostModes=function(successCallBack,ajaxSettings){

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/GetListBillableCostModes', {}, ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.GetListProducts=function(adServer,successCallBack,ajaxSettings){
var dataObject = new Object();
dataObject.adServer = adServer;

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/GetListProducts', JSON.stringify(dataObject), ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.GetSupplierData=function(adserverId, currencyId, token,successCallBack,ajaxSettings){
var dataObject = new Object();
dataObject.adserverId = adserverId;
dataObject.currencyId = currencyId;
dataObject.token = token;

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/GetSupplierData', JSON.stringify(dataObject), ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.GetSingleAdservingTypes=function(id, token,successCallBack,ajaxSettings){
var dataObject = new Object();
dataObject.id = id;
dataObject.token = token;

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/GetSingleAdservingTypes', JSON.stringify(dataObject), ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.SaveCustomRate=function(type, token,successCallBack,ajaxSettings){
var dataObject = new Object();
dataObject.type = type;
dataObject.token = token;

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/SaveCustomRate', JSON.stringify(dataObject), ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.ClearAllRates=function(token,successCallBack,ajaxSettings){
var dataObject = new Object();
dataObject.token = token;

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/ClearAllRates', JSON.stringify(dataObject), ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.SaveDefaultStatus=function(id, isCombo, defaultStatus, token,successCallBack,ajaxSettings){
var dataObject = new Object();
dataObject.id = id;
dataObject.isCombo = isCombo;
dataObject.defaultStatus = defaultStatus;
dataObject.token = token;

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/SaveDefaultStatus', JSON.stringify(dataObject), ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.SaveCampaignLevelRate=function(id, isCombo, isCampaignLevel, token,successCallBack,ajaxSettings){
var dataObject = new Object();
dataObject.id = id;
dataObject.isCombo = isCombo;
dataObject.isCampaignLevel = isCampaignLevel;
dataObject.token = token;

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/SaveCampaignLevelRate', JSON.stringify(dataObject), ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.CampaignAdservingTypes.GetAssignmentSyncInfo=function(campaignId,successCallBack,ajaxSettings){
var dataObject = new Object();
dataObject.campaignId = campaignId;

return xxx.ajax.send(
'/xxxxMediaAppWeb/pages/services/campaign/CampaignAdservingTypes.asmx/GetAssignmentSyncInfo', JSON.stringify(dataObject), ajaxSettings, successCallBack, 'application/json; charset=utf-8');
};
xxx.Services.Model.CampaignAdservingTypes.CurrencyCodes= {GBP:0,AED:1,AFN:2,ALL:3,AMD:4,ANG:5,AOA:6,ARS:7,AUD:8,AWG:9,AZN:10,BAM:11,BBD:12,BDT:13,BGN:14,BHD:15,BIF:16,BMD:17,BND:18,BOB:19,BRL:20,BSD:21,BTN:22,BWP:23,BYR:24,BZD:25,CAD:26,CDF:27,CHF:28,CLP:29,CNY:30,COP:31,CRC:32,CUP:33,CVE:34,CYP:35,CZK:36,DJF:37,DKK:38,DOP:39,DZD:40,EEK:41,EGP:42,ERN:43,ETB:44,EUR:45,FJD:46,FKP:47,GEL:48,GGP:49,GHS:50,GIP:51,GMD:52,GNF:53,GTQ:54,GYD:55,HKD:56,HNL:57,HRK:58,HTG:59,HUF:60,IDR:61,ILS:62,IMP:63,INR:64,IQD:65,IRR:66,ISK:67,JEP:68,JMD:69,JOD:70,JPY:71,KES:72,KGS:73,KHR:74,KMF:75,KPW:76,KRW:77,KWD:78,KYD:79,KZT:80,LAK:81,LBP:82,LKR:83,LRD:84,LSL:85,LTL:86,LVL:87,LYD:88,MAD:89,MDL:90,MGA:91,MKD:92,MMK:93,MNT:94,MOP:95,MRO:96,MTL:97,MUR:98,MVR:99,MWK:100,MXN:101,MYR:102,MZN:103,NAD:104,NGN:105,NIO:106,NOK:107,NPR:108,NZD:109,OMR:110,PAB:111,PEN:112,PGK:113,PHP:114,PKR:115,PLN:116,PYG:117,QAR:118,RON:119,RSD:120,RUB:121,RWF:122,SAR:123,SBD:124,SCR:125,SDG:126,SEK:127,SGD:128,SHP:129,SLL:130,SOS:131,SPL:132,SRD:133,STD:134,SVC:135,SYP:136,SZL:137,THB:138,TJS:139,TMT:140,TND:141,TOP:142,TRY:143,TTD:144,TVD:145,TWD:146,TZS:147,UAH:148,UGX:149,USD:150,UYU:151,UZS:152,VEB:153,VEF:154,VND:155,VUV:156,WST:157,XAF:158,XAG:159,XAU:160,XCD:161,XDR:162,XOF:163,XPD:164,XPF:165,XPT:166,YER:167,ZAR:168,ZMK:169,ZWD:170};
xxx.Services.Model.CampaignAdservingTypes.UnitType= {None:0,Scc:1,Inserts:2,Acquisitions:3,Clicks:4,Impressions:5,Engagements:6,Units:7,Leads:8,Views:9,Installs:10,Advertorial:11,Registrations:12,Sponsorship:13,Tenancy:14,Fixed:15};
xxx.Services.Model.CampaignAdservingTypes.SpendType= {Net:0,Gross:1,Client:2,Billable:3};
xxx.Services.Model.CampaignAdservingTypes.TechType= {AdServing:1,PrimaryTracking:2,ContentVerification:4,AdditionalTracking:8,DataFee:16,AdditionalFee:32,Other:64};
xxx.Services.Model.CampaignAdservingTypes.TechCostMode= {PercentageOfCosts:1,Volume:2,FixedCost:4};
xxx.Services.Model.CampaignAdservingTypes.BillableCostMode= {SameAsPayable:0,Independent:1};
xxx.Services.Model.MediaPlan = {};
xxx.Services.Model.MediaPlan.CampaignMode= {Planning:0,Buying:1,BookingDisabled:2,Draft:-1};
xxx.Services.Model.MediaPlan.VersionStatusTypes= {LockedByCurrentUser:0,Unlocked:1,LockedByOtherUser:2};
xxx.Services.Model.MediaPlan.CompensationPotType= {CreditDebit:0,Tracker:1};
xxx.Services.Model.MediaPlan.AgencyLinkLevel= {none:5,calculated:10,insertion:40,shopper:50,supplier:100,brief:150,campaign:180,BeCampaign:185,product:190,client:200,Advertiser:210,agency:300,company:400,companyParent:450,global:500};
xxx.Services.Model.MediaPlan.CurrencyCodes= {GBP:0,AED:1,AFN:2,ALL:3,AMD:4,ANG:5,AOA:6,ARS:7,AUD:8,AWG:9,AZN:10,BAM:11,BBD:12,BDT:13,BGN:14,BHD:15,BIF:16,BMD:17,BND:18,BOB:19,BRL:20,BSD:21,BTN:22,BWP:23,BYR:24,BZD:25,CAD:26,CDF:27,CHF:28,CLP:29,CNY:30,COP:31,CRC:32,CUP:33,CVE:34,CYP:35,CZK:36,DJF:37,DKK:38,DOP:39,DZD:40,EEK:41,EGP:42,ERN:43,ETB:44,EUR:45,FJD:46,FKP:47,GEL:48,GGP:49,GHS:50,GIP:51,GMD:52,GNF:53,GTQ:54,GYD:55,HKD:56,HNL:57,HRK:58,HTG:59,HUF:60,IDR:61,ILS:62,IMP:63,INR:64,IQD:65,IRR:66,ISK:67,JEP:68,JMD:69,JOD:70,JPY:71,KES:72,KGS:73,KHR:74,KMF:75,KPW:76,KRW:77,KWD:78,KYD:79,KZT:80,LAK:81,LBP:82,LKR:83,LRD:84,LSL:85,LTL:86,LVL:87,LYD:88,MAD:89,MDL:90,MGA:91,MKD:92,MMK:93,MNT:94,MOP:95,MRO:96,MTL:97,MUR:98,MVR:99,MWK:100,MXN:101,MYR:102,MZN:103,NAD:104,NGN:105,NIO:106,NOK:107,NPR:108,NZD:109,OMR:110,PAB:111,PEN:112,PGK:113,PHP:114,PKR:115,PLN:116,PYG:117,QAR:118,RON:119,RSD:120,RUB:121,RWF:122,SAR:123,SBD:124,SCR:125,SDG:126,SEK:127,SGD:128,SHP:129,SLL:130,SOS:131,SPL:132,SRD:133,STD:134,SVC:135,SYP:136,SZL:137,THB:138,TJS:139,TMT:140,TND:141,TOP:142,TRY:143,TTD:144,TVD:145,TWD:146,TZS:147,UAH:148,UGX:149,USD:150,UYU:151,UZS:152,VEB:153,VEF:154,VND:155,VUV:156,WST:157,XAF:158,XAG:159,XAU:160,XCD:161,XDR:162,XOF:163,XPD:164,XPF:165,XPT:166,YER:167,ZAR:168,ZMK:169,ZWD:170};
xxx.Services.Model.MediaPlan.UnitType= {None:0,Scc:1,Inserts:2,Acquisitions:3,Clicks:4,Impressions:5,Engagements:6,Units:7,Leads:8,Views:9,Installs:10,Advertorial:11,Registrations:12,Sponsorship:13,Tenancy:14,Fixed:15};
xxx.Services.Model.MediaPlan.SpendType= {Net:0,Gross:1,Client:2,Billable:3};
xxx.Services.Model.MediaPlan.TechType= {AdServing:1,PrimaryTracking:2,ContentVerification:4,AdditionalTracking:8,DataFee:16,AdditionalFee:32,Other:64};
xxx.Services.Model.MediaPlan.TechCostMode= {PercentageOfCosts:1,Volume:2,FixedCost:4};
xxx.Services.Model.MediaPlan.PlanArea= {DataColumnLeft:0,HeaderLeftTop:1,HeaderLeftBottom:2,HeaderRight:3,FooterLeft:4,FooterRight:5,DataColumnRight:6,DataColumnCentre:7,FooterCentre:8,HeaderCentre:9,FlightingElements:10,SplitFlightingElements:11,GridConstants:12};
xxx.Services.Model.MediaPlan.ColumnMetaDataTypes= {DateBoundaryMin:0,DateBoundaryMax:1,SectionIndex:2,FirstInSection:3,LastInSection:4,DateSplitMode:5,DateEditMode:6,BookingCodeType:7,GroupHeader:8,ColumnDisplay:9,FirstDayOfWeek:10,LastDayOfWeek:11,CostSplitInterval:12};
xxx.Services.Model.MediaPlan.ElementValueType= {String:0,Number:1,Currency:2,URL:3,Bool:4,Date:5,Percentage:6,Image:7,Decimal:8,ColourKey:9,ColourPicker:10};
xxx.Services.Model.MediaPlan.TotalType= {None:0,Sum:1,Average:2};
xxx.Services.Model.MediaPlan.PlanItemType= {Booking:0,Calculation:1,CustomEditable:2,Campaign:3,CustomReadOnly:4,Custom:5,Flighting:6,CustomList:7};
xxx.Services.Model.MediaPlan.ColumnDisplayRule= {ClientReady:0,PlanGrid:1,Both:2};
xxx.Services.Model.MediaPlan.FirmedStates= {planned:0,firmed:1,confirmed:2};
xxx.Services.Model.MediaPlan.State= {Unchanged:0,Added:1,Modified:2,Deleted:3};
xxx.Services.Model.MediaPlan.DateSplitModes= {Days:0,Weeks:1,Months:2,Quarters:3,Years:4,NotApplicable:5};
xxx.Services.Model.MediaPlan.DateEditModes= {None:0,Simple:1,MonthWeek:2,StartEnd:3,MonthDay:4,Quarters:5,Years:6,MonthWeekGroup:7,MonthDayGroup:8};
xxx.Services.Model.MediaPlan.VersionType= {Planned:0,Hidden:1,Archived:2,Locked:3,Booked:4,AutoSave:5,Draft:-1};
xxx.Services.Model.MediaPlan.BalanceSheetDetailLevel= {Brief:0,Booking:1};
xxx.Services.AgencySupplierDocument = {};
xxx.Services.AgencySupplierDocument.GetAgencySupplierDocuments = SupplierOptionsServices.GetAgencySupplierDocuments;
xxx.Services.StoredRates = {};
xxx.Services.StoredRates.GetBySupplierAndCampaign = SupplierOptionsServices.GetBySupplierAndCampaign;
export default xxx;
