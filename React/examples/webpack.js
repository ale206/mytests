/*!
* Facebook React Starter Kit | https://github.com/kriasoft/react-starter-kit
* Copyright (c) KriaSoft, LLC. All rights reserved. See LICENSE.txt
*/

/*
* Webpack configuration. For more information visit
* http://webpack.github.io/docs/configuration
*/

'use strict';

var webpack = require('webpack');
var path    = require('path');

module.exports = function (release) {
 var nodeEnv = release ? '"production"' : '"development"';
 var writeFileStats =  function() {
    this.plugin("done", function(stats) {
      require("fs").writeFileSync(
        path.join(__dirname, "../examples/", "stats.json"),
        JSON.stringify(stats.toJson()));
      });
    }

  return {


    cache: !release,
    debug: !release,
    entry: {
      example: './examples/example.js'
    },

    output: {
      path: './examples/build/',
      filename: '[name].js',
      publicPath: './examples/build/',
      libraryTarget: "umd"
    },
    externals: {
      "jQuery" : 'jQuery'
    },
    stats: {
      colors: true,
      reasons: !release
    },
    devtool: '#cheap-module-source-map',
    resolve: {
      extensions: ['', '.webpack.js', '.web.js', '.js', '.jsx', '.json']
    },
    plugins: [  new webpack.DefinePlugin({'process.env.NODE_ENV': nodeEnv, 'translate': 'window.xxx._'})],
    amd: { globalize : false },
    module: {
      loaders: [
      {
        test: /\.js|.jsx/,
        exclude: /node_modules/,
        loader:  'babel-loader?optional[]=runtime'
      },
      {
        test: /\.css$/,
        loader: 'style!css'
      },
      {
        test: /\.less$/,
        loader: 'style!css!less'
      },
      {
        test: /\.gif/,
        loader: 'url-loader?limit=10000&mimetype=image/gif'
      },
      {
        test: /\.jpg/,
        loader: 'url-loader?limit=10000&mimetype=image/jpg'
      },
      {
        test: /\.png/,
        loader: 'url-loader?limit=10000&mimetype=image/png'
      },
      {
        test: /\.json/,
        loader: 'json-loader'
      },
      {
        test: /[\/\\]node_modules[\/\\]globalize[\/\\]dist[\/\\]*.js|[\/\\]node_modules[\/\\]globalize[\/\\]dist[\/\\]globalize[\/\\]*.js/,
        loader: "imports?define=>false"
      }
      ],
      postLoaders: [
      {
        test: /build\/\.js$/,
        ignore : /testData/,
        loader: 'jshint'
      }
      ]
    },

  };
};
