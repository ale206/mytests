type ServerReceiveLoadPlanDataAction = {
  type: any;
  serverGridData: ServerGridData;
};


type ServerAction =  ServerReceiveLoadPlanDataAction;

/*add other view actions here when necessary
type ViewCreateMessageAction = {
  type: any;
  text: string;
};

type ViewClickThreadAction = {
  type: any;
  threadID: string;
};

type ViewAction = ViewCreateMessageAction | ViewClickThreadAction;

/*/


type PayloadType = {
  source: any;
  action: ServerReceiveLoadPlanDataAction;
};
