/* @flow */
declare class ServerPlanData {

  PlanElements:Array<ServerPlanElementData>;

}

declare class ServerPlanElementData {


  ElementAreaId: number;
  AreaName: string;
  Columns: Array<ServerPlanColumn>;
  Rows: Array<ServerPlanRow>;
}

declare class ServerPlanColumn {
  ItemId: string;
  ItemDataType: string;
}

declare class PlanRowItem {
  Value: string;
  FormatOptions: any;
  ItemDataType: string;
}

declare class ServerPlanRow {
  Id: number;
  Items: Array<PlanRowItem>;
}

declare class ServerGridData {

}

type PlanWebServices = {
  LoadPlanDataForCampaign : Function
}
