import keyMirror from 'fbjs/lib/keyMirror';

const contants = {

  PayloadSources: keyMirror({
    SERVER_ACTION: null,
    VIEW_ACTION: null
  })

};

export default contants;
