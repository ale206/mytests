import AppConstants from './appConstants';
let Dispatcher =require('flux').Dispatcher;

let _dispatcherInstance: Dispatcher<PayloadType> = new Dispatcher();

let AppDispatcher = {

  // WORKAROUND: flow prevents extension of class instances on run-time. Thus,
  // we cannot use Object.assign to extend a Dispatcher instance. Instead, we
  // work around this by simply wrapping all Dispatcher methods on our own.
  register: _dispatcherInstance.register.bind(_dispatcherInstance),
  unregister: _dispatcherInstance.unregister.bind(_dispatcherInstance),
  waitFor: _dispatcherInstance.waitFor.bind(_dispatcherInstance),
  dispatch: _dispatcherInstance.dispatch.bind(_dispatcherInstance),
  isDispatching: _dispatcherInstance.isDispatching.bind(_dispatcherInstance),
  /**
   * @param {object} action The details of the action, including the action's
   * type and additional data coming from the server.
   */
  handleServerAction: function(action: ServerAction) {
    let payload = {
      source: AppConstants.PayloadSources.SERVER_ACTION,
      action: action
    };
    this.dispatch(payload);
  },

  /**
   * @param {object} action The details of the action, including the action's
   * type and additional data coming from the view.
   */
  handleViewAction: function(action: any) {
    let payload = {
      source: AppConstants.PayloadSources.VIEW_ACTION,
      action: action
    };
    this.dispatch(payload);
  }

};

module.exports = AppDispatcher;
