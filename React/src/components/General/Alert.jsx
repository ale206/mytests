import React, {PropTypes} from 'react';
import classNames from 'classnames';

class Alert extends React.Component {
  renderDismissButton() {
    return (<button type="button" onKeyDown={this.props.onDismissBtnKeyDown} onClick={this.props.onDismiss} className="close" aria-label="Close"><span aria-hidden="true" onClick={this.props.onDismiss}>&times;</span></button>);
  }

  render() {
    let dismissButton = this.props.dismissable ? this.renderDismissButton() : null;
    let typeClass = `alert-${this.props.alertType}`;
    let alertClass = classNames('alert', typeClass, {'alert-dismissable': this.props.dismissable});
    return (<div className={alertClass}>{dismissButton}<span className="js-error-message">{this.props.message}</span></div>);
  }
}

Alert.propTypes = {
  dismissable: PropTypes.bool.isRequired,
  message: PropTypes.object.isRequired,
  alertType: PropTypes.oneOf(['success', 'error', 'info', 'warning']),
  onDismiss: PropTypes.func,
  onDismissBtnKeyDown: PropTypes.func
};

export default Alert;
