let React = require('react');

function supportsTransitions() {
  let b = document.body || document.documentElement;
  let s = b.style;
  let p = 'transition';

  if (typeof s[p] == 'string') { return true; }

  // Tests for vendor specific prop
  let v = ['Moz', 'webkit', 'Webkit', 'Khtml', 'O', 'ms'];
  p = p.charAt(0).toUpperCase() + p.substr(1);

  for (let i = 0; i < v.length; i++) {
    if (typeof s[v[i] + p] == 'string') { return true; }
  }

  return false;
}

let Loader;

if (supportsTransitions() === true ) {
  let assign = require('react-kit/appendVendorPrefix');
  let insertKeyframesRule = require('react-kit/insertKeyframesRule');

  let keyframes = {
    '0%': {
      transform: 'scaley(1.0)'
    },
    '50%': {
      transform: 'scaley(0.4)'
    },
    '100%': {
      transform: 'scaley(1.0)'
    }
  };

  let animationName = insertKeyframesRule(keyframes);

  Loader = React.createClass({
    propTypes: {
      color: React.PropTypes.string,
      height: React.PropTypes.string,
      width: React.PropTypes.string,
      margin: React.PropTypes.string,
      radius: React.PropTypes.string
    },
    getDefaultProps: function() {
      return {
        color: '#ffffff',
        height: '35px',
        width: '4px',
        margin: '2px',
        radius: '2px'
      };
    },
    getLineStyle: function() {
      return {
        backgroundColor: this.props.color,
        height: this.props.height,
        width: this.props.width,
        margin: this.props.margin,
        borderRadius: this.props.radius
      };
    },
    getAnimationStyle: function(i) {
      let animation = [animationName, '1s', (i * 0.1) + 's', 'infinite', 'cubic-bezier(.2,.68,.18,1.08)'].join(' ');
      let animationFillMode = 'both';
      return {
        animation: animation,
        animationFillMode: animationFillMode
      };
    },
    getStyle: function(i) {
      return assign(
        this.getLineStyle(i),
        this.getAnimationStyle(i),
        {
          display: 'inline-block'
        }
      );
    },
    render: function() {
      return (<div>
        <div style={this.getStyle(1)}></div>
        <div style={this.getStyle(2)}></div>
        <div style={this.getStyle(3)}></div>
        <div style={this.getStyle(4)}></div>
        <div style={this.getStyle(5)}></div>
        </div>);
    }
  });
} else {
  Loader =  React.createClass({
    render() {
      return (<div>Loading Mediaplan</div>);
    }
  });
}

module.exports = class extends React.Component {
  render() {
    return (
      <div className="loader-container">
        <Loader color="#1ab394" size="16px" margin="4px"/>
      </div>

    );
  }
};
