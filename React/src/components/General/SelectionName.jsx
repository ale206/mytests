import React, {PropTypes} from 'react';

class SelectionName extends React.Component {
  constructor(props, context) {
    super(props, context);

    let value = this.props.value;
    this.state = { value };
  }

  onChange(e) {
    let value = e.target.value;
    this.setState({ value });
  }

  getInputValue() {
    let value = '';

    if (this.props.isDisabled === false) {
      value = this.state.value;
    }

    return value;
  }

  render() {
    return (
      <input
        className="editor-main"
        ref="inputNode"
        type="text"
        placeholder={this.props.placeholder}
        value={this.getInputValue()}
        onKeyDown={this.props.onKeyDown}
        onChange={this.onChange.bind(this)}
        disabled={this.props.isDisabled} />);
  }
}

SelectionName.propTypes = {
  value: PropTypes.string,
  placeholder: PropTypes.string.isRequired,
  isDisabled: PropTypes.bool.isRequired,
  onKeyDown: PropTypes.func
};

SelectionName.defaultProps = {
  placeholder: 'name your selection',
  isDisabled: false
};

export default SelectionName;
