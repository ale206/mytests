import React from 'react';
import TestUtils from 'react-addons-test-utils';
import SelectionName from '../SelectionName';
import ComponentTestRunner from '../../../utils/testRunners/ComponentTestRunner';

const selectionNameDefaultProps = {
  value: ''
};

const getComponentInstance = (props = selectionNameDefaultProps) => {
  let component = <SelectionName {...props} />;
  let componentTestRunner = new ComponentTestRunner({component});

  return componentTestRunner.componentInstance;
};

describe('SelectionName test', () => {
  let selectionNameInstance;

  beforeEach(() => {
    selectionNameInstance = getComponentInstance();
  });

  it('should import SelectionName', () => {
    expect(SelectionName).toBeDefined();
  });

  it('should render SelectionName', () => {
    expect(selectionNameInstance).toBeDefined();
  });

  describe('input', () => {
    it('should be enabled when isDisabled is false', () => {
      expect(selectionNameInstance.refs.inputNode.disabled).toBe(false);
    });

    it('should be disabled when isDisabled is true', () => {
      let selectionNameProps = Object.assign({isDisabled: true}, selectionNameDefaultProps);

      selectionNameInstance = getComponentInstance(selectionNameProps);
      expect(selectionNameInstance.refs.inputNode.disabled).toBe(true);
    });

    it('should clear input value when input is disabled', () => {
      let selectionNameProps = Object.assign(selectionNameDefaultProps, {isDisabled: true, value: 'value'});

      selectionNameInstance = getComponentInstance(selectionNameProps);
      expect(selectionNameInstance.refs.inputNode.value).toBe('');
    });
  });
});
