import React from 'react';
import cookie from 'react-cookie';
import LoginButton from './LoginButton';
import LogoutButton from './LogoutButton';

class ExternalSystemLogin extends React.Component {

  constructor(props, context) {
    super(props, context);

    // State
    // Get Cookie to check if logged
    this.state = { authCookie: cookie.load('authCookie') };

    // Bindings
    this.checkCookie = this.checkCookie.bind(this);
  }

  checkCookie() {
    this.setState({ authCookie: cookie.load('authCookie')});
  }

  render() {
    return (
      this.state.authCookie === undefined ?
        <LoginButton  checkCookie={this.checkCookie} logoImageUrl={this.props.logoImageUrl} api={this.props.api} /> :
        <LogoutButton checkCookie={this.checkCookie} />
    );
  }
}

ExternalSystemLogin.propTypes = {
  logoImageUrl: React.PropTypes.string.isRequired,
  // api: React.PropTypes.object.isRequired,
  api: React.PropTypes.shape({
    invoke: React.PropTypes.func.isRequired
  }).isRequired
};

ExternalSystemLogin.defaultProps = {
  logoImageUrl: 'https://www.xxxx.com/xxxxMediaAppWeb/static/images/public/xxxxLogoTrans.png',
  api: { 'invoke': function() {} } // TODO: REMOVE
};

export default ExternalSystemLogin;
