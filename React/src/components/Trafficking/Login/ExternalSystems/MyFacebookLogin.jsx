import React, { PropTypes, Component } from 'react';
import FacebookLogin from 'react-facebook-login';
import QueryString from 'querystring';

class MyFacebookLogin extends Component {

  constructor(props) {
    super(props);

    this.exchangeTokenForLongLived = this.exchangeTokenForLongLived.bind(this);
  }

  exchangeTokenForLongLived(authResponse) {
    const { appId, appSecretId } = this.props;

    const url = `https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=${appId}&client_secret=${appSecretId}&fb_exchange_token=${authResponse.accessToken}`;

    fetch(url)
    .then((response) => response.text())
    .then((responseText) => {
      const parsed = QueryString.parse(responseText);
      // Add longLivedToken to the callback
      let finalResponse = Object.assign({ longLivedToken: parsed.access_token}, authResponse);
      this.props.callback(finalResponse);
    });
  }

  render() {
    let callback = this.props.useLongLivedToken ? this.exchangeTokenForLongLived : this.props.callback;

    return <FacebookLogin {...this.props} callback={callback} />;
  }
}

MyFacebookLogin.propTypes =  Object.assign({
  useLongLivedToken: PropTypes.bool
}, FacebookLogin.propTypes);

MyFacebookLogin.defaultProps  = {
  textButton: 'Login with Facebook',
  typeButton: 'button',
  xfbml: false,
  cookie: false,
  size: 'small',
  cssClass: 'kep-login-facebook',
  version: '2.3',
  scope: 'email,ads_read,ads_management,read_insights',
  fields: 'name',
  language: 'en_GB',
  useLongLivedToken: true
};

window.adazzleUI = window.adazzleUI || {};
let adazzleUI = window.adazzleUI;
adazzleUI.beSystem = adazzleUI.beSystem || {};
module.exports = adazzleUI.beSystem.MyFacebookLogin = MyFacebookLogin;
