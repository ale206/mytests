import React from 'react';
import {Modal, Button} from 'react-bootstrap';
import LoginForm from './LoginForm';

class LoginButton extends React.Component {

  constructor(props, context) {
    super(props, context);

    // State
    this.state = { showModal: false };

    // Bindings
    this.closeModal = this.closeModal.bind(this);
    this.openModal = this.openModal.bind(this);
  }

  closeModal() {
    this.setState({ showModal: false });
  }

  openModal() {
    this.setState({ showModal: true });
  }

  render() {
    return (
      <div>
        <Button
          bsStyle="primary"
          bsSize="large"
          onClick={this.openModal}
          >
          Log In
        </Button>

        <Modal show={this.state.showModal} onHide={this.closeModal}>
          <Modal.Header closeButton>
            <Modal.Title>Login</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <img src={this.props.logoImageUrl} />
            <LoginForm closeModal={this.closeModal} checkCookie={this.props.checkCookie} api={this.props.api} />
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.closeModal}>Close</Button>
          </Modal.Footer>
          </Modal>
      </div>
    );
  }
}

LoginButton.propTypes = {
  logoImageUrl: React.PropTypes.string.isRequired,
  checkCookie: React.PropTypes.func
};

export default LoginButton;
