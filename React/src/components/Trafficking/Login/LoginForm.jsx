import React from 'react';
import {Button, FormGroup, FormControl, ControlLabel} from 'react-bootstrap';
import cookie from 'react-cookie';
import fetchMock from 'fetch-mock'; // TODO: REMOVE WHEN BACKEND READY
import 'isomorphic-fetch';  // TODO: REMOVE WHEN BACKEND READY

class LoginForm extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state =  { username: '', password: '' };

    this.doLogin = this.doLogin.bind(this);

    this.AuthCookieMock = {
      'Name': 'Sizmek_ImpCkAlessioBuyer',
      'Path': '/',
      'Secure': false,
      'Shareable': false,
      'HttpOnly': false,
      'Domain': null,
      'Expires': '2016-08-24T16:17:03.047443+01:00',
      'Value':
        'system_type_key=XrYYjTerP9FyRuLj9VWaIvbfOHta5dX0&Username=i0Cdj2S1wNrofBaGz8wi9M12dNijKqvA&Password=gWfas1V8xk1Hc8RX4tU7NqgGomDhGH84&APIUsername=geCigRMl5vwY3P961J3mKqJreqQ1ugcL&APIPassword=HEY0Td0s45C4hiSzCtnwXyADyJ8HIVYn&ApplicationKey=VnagS0s1DHSD8oD2nFZxnxw4mksl6RC9MYz0sP2UMyKk3owAnMVQvL/c/uMFyWqY&UseSandbox=IPE5MUagVufeJR2miWCveq8KFXQgL/kK',
      'HasKeys': true, 'Values': ['system_type_key',
                                   'Username',
                                   'Password',
                                   'APIUsername',
                                   'APIPassword',
                                   'ApplicationKey',
                                   'UseSandbox']
    };


    fetchMock.mock('Trafficking/Login',
      {status: 200, body: {authCookie: this.AuthCookieMock}});
  }

  getValidationState(username, password) {
    const minLength = 6;  // TODO: MOVE TO CONFIG FILE

    if (username.length >= minLength && password.length >= minLength) {
      return 'success';
    }
    else if (username.length > 0 || password.length > 0) {
      return 'error';
    }
  }

  doLogin() {
    // Check if form is valid
    if (this.getValidationState(this.state.username, this.state.password) === 'success') {
      // let data = JSON.stringify({
      //   username: this.state.username,
      //   password: this.state.password
      // });

      fetch('Trafficking/Login')
        .then(response =>
          response.json().then(json => ({ json, response }))
        ).then(({ json, response }) => {
          if (!response.ok) {
            console.log('login error');
          }
          else {
            // Save Cookie
            cookie.save('authCookie', this.AuthCookieMock, { path: '/' });

            console.log(json.authCookie);

            // Calling the checkCookie method in ExternalSystemLogin, it will show the LogoutButton
            this.props.checkCookie();

            // Close Modal
            this.props.closeModal();
          }
        });

      // PRODUCTION CALL (TBC)
      // this.props.api.invoke('Trafficking/Login', 'POST', data, {
      //     success: (result) => {
      //       console.log("login success");

      //       //ON SUCCESS:
      //       //Save Cookie
      //       cookie.save('userId', 1, { path: '/' });

      //     },
      //     error: () => {
      //       console.log("login error");
      //       //this.setState({error: true});
      //     },
      //     complete: () => {}
      // }, true);// cache the service

      // //Calling the checkCookie method in ExternalSystemLogin, it will show the LogoutButton
      // this.props.checkCookie();

      // //Close Modal
      // this.props.closeModal();
    }
  }

  handleChange(e) {
    e.target.id === 'username' ? this.setState({ username: e.target.value }) : this.setState({ password: e.target.value });
  }

  render() {
    return (
      <form>
       <FormGroup
          controlId="formBasicText"
          validationState={this.getValidationState(this.state.username, this.state.password)}
        >
          <ControlLabel>Username</ControlLabel>
          <FormControl
            type="text"
            value={this.state.username}
            placeholder="Enter text"
            onChange={(e) => { this.handleChange(e);}}
            id="username"
          />

          <ControlLabel>Password</ControlLabel>
          <FormControl
            type="Password"
            value={this.state.password}
            placeholder="Enter password"
            onChange={(e) => { this.handleChange(e);}}
            id="password"
          />

          <Button onClick={this.doLogin} bsStyle="primary">Login</Button>

        </FormGroup>
      </form>
    );
  }
}

LoginForm.propTypes = {
  checkCookie: React.PropTypes.func,
  closeModal: React.PropTypes.func
};

export default LoginForm;
