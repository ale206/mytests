import React from 'react';
import {Button} from 'react-bootstrap';
import cookie from 'react-cookie';

class LogoutButton extends React.Component {

  constructor(props, context) {
    super(props, context);

    // Bindings
    this.doLogOut = this.doLogOut.bind(this);
  }

  doLogOut() {
    // TODO: Call to Logout API...

    // Remove Cookie
    cookie.remove('authCookie', { path: '/' });

    // Calling the checkCookie method in ExternalSystemLogin, it will show the LoginButton again
    this.props.checkCookie();
  }

  render() {
    return (
      <div>
        <Button
          bsStyle="primary"
          bsSize="large"
          onClick={this.doLogOut}
        >
        Log Out
        </Button>
      </div>
    );
  }
}

LogoutButton.propTypes = {
  checkCookie: React.PropTypes.func
};

export default LogoutButton;
