export const MediaIds = {
  Online: 1,
  Press: 2,
  Outdoor: 16,
  Radio: 32,
  TV: 64,
  Cinema: 128,
  Ambient: 256,
  Sponsorship: 512,
  OnlineTechnology: 1024,
  Regional: 2048,
  Magazines: 4096,
  Search: 8192,
  OnlineAffiliates: 16384,
  InternationalPress: 32768
};

export const PrintMediaTypes = [
  MediaIds.Press,
  MediaIds.Magazines,
  MediaIds.Regional,
  MediaIds.InternationalPress
];

export const OnlineMediaTypes = [
  MediaIds.Online,
  MediaIds.OnlineAffiliates,
  MediaIds.Search,
  MediaIds.OnlineTechnology
];

export const SupplierFrequency = {
  Monthly: 'Monthly',
  Daily: 'Daily'
};

export default class MediaUtils {
  static getOnlineComponent(mediaId) {
    let onlineMedias = OnlineMediaTypes.filter((m) => {
      return ((m & mediaId) > 0);
    });
    return onlineMedias.reduce((prev, curr) => prev + curr);
  }

  static getPrintComponent(mediaId) {
    let printMedias = PrintMediaTypes.filter((m) => {
      return ((m & mediaId) > 0);
    });
    return printMedias.reduce((prev, curr) => prev + curr);
  }

  static hasPrint(mediaId) {
    let foundMatch = false;
    PrintMediaTypes.forEach((type) => {
      if ((type & mediaId) > 0 && !foundMatch) {
        foundMatch = true;
      }
    });
    return foundMatch;
  }

  static hasOnline(mediaId) {
    let foundMatch = false;
    OnlineMediaTypes.forEach((type) => {
      if ((type & mediaId) > 0 && !foundMatch) {
        foundMatch = true;
      }
    });
    return foundMatch;
  }
}
