import MediaUtils, {MediaIds, PrintMediaTypes, OnlineMediaTypes} from '../MediaUtils.js';

describe('MediaUtils', () => {
  it('imports should be defined', () => {
    expect(MediaUtils).toBeDefined();
    expect(MediaIds).toBeDefined();
    expect(PrintMediaTypes).toBeDefined();
    expect(OnlineMediaTypes).toBeDefined();
  });

  it('hasPrint works', () => {
    expect(MediaUtils.hasPrint(MediaIds.Magazines)).toBe(true);
  });
});
