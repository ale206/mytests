import globalize from '../globalize';
import moment from 'moment';

describe('globalize test', () => {
  it('should import globalize', () => {
    expect(globalize).toBeDefined();
  });

  describe('formatDate', () => {
    describe('en-GB locale', () => {
      it('should recognise a date in the "MS ticks" format', () => {
        let dateToCheck = '/Date(1428246000000)/';
        let outputDate = globalize.formatDate(dateToCheck);
        expect(outputDate).toEqual('05 Apr 15');
      });

      it('should recognise a date in the "UNIX ms timestamp" format', () => {
        let dateToCheck = '1428246000000';
        let outputDate = globalize.formatDate(dateToCheck);
        expect(outputDate).toEqual('05 Apr 15');
      });

      it('should recognise a date in YYYY-MM-DD format', () => {
        let dateToCheck = '2015-04-05';
        let outputDate = globalize.formatDate(dateToCheck);
        expect(outputDate).toEqual('05 Apr 15');
      });

      it('should recognise a date in YYYYMMDD format', () => {
        let dateToCheck = '20150405';
        let outputDate = globalize.formatDate(dateToCheck);
        expect(outputDate).toEqual('05 Apr 15');
      });

      it('should recognise a date in DD MMM YYYY format', () => {
        let dateToCheck = '05 Apr 2015';
        let outputDate = globalize.formatDate(dateToCheck);
        expect(outputDate).toEqual('05 Apr 15');
      });

      it('should recognise a date in DD MMM YY format', () => {
        let dateToCheck = '05 Apr 15';
        let outputDate = globalize.formatDate(dateToCheck);
        expect(outputDate).toEqual('05 Apr 15');
      });

      it('should recognise a date in DD MM YYYY format', () => {
        let dateToCheck = '05 04 2015';
        let outputDate = globalize.formatDate(dateToCheck);
        expect(outputDate).toEqual('05 Apr 15');
      });

      it('should recognise a date in DD MM YY format', () => {
        let dateToCheck = '05 04 15';
        let outputDate = globalize.formatDate(dateToCheck);
        expect(outputDate).toEqual('05 Apr 15');
      });

      it('should recognise a date in DD-MM-YY format', () => {
        let dateToCheck = '05-04-15';
        let outputDate = globalize.formatDate(dateToCheck);
        expect(outputDate).toEqual('05 Apr 15');
      });

      it('should recognise a date in DD-MM-YYYY format', () => {
        let dateToCheck = '05-04-2015';
        let outputDate = globalize.formatDate(dateToCheck);
        expect(outputDate).toEqual('05 Apr 15');
      });

      it('should recognise a date in DD/MM/YY format', () => {
        let dateToCheck = '05/04/15';
        let outputDate = globalize.formatDate(dateToCheck);
        expect(outputDate).toEqual('05 Apr 15');
      });

      it('should recognise a date in DD/MM/YYYY format', () => {
        let dateToCheck = '05/04/15';
        let outputDate = globalize.formatDate(dateToCheck);
        expect(outputDate).toEqual('05 Apr 15');
      });

      describe('should format moment objects into the selected format', () => {
        it('should parse D MMMM YYYY', () => {
          expect(globalize.formatDate(new moment('10 September 2015', 'D MMMM YYYY'))).toBe('10 Sep 15', 'parsing 10 September 2015');
        });
        it('should parse D MMM YY', () => {
          expect(globalize.formatDate(new moment('10 Sep 15', 'D MMM YY'))).toBe('10 Sep 15', 'parsing 10 Sep 15');
        });
        it('should parse ISO', () => {
          expect(globalize.formatDate(new moment('2015-09-10T00:00:00.000Z', moment.ISO_8601))).toBe('10 Sep 15', 'ISO string 2015-09-10T00:00:00.000Z');
        });
        it('should parse DD/MM/YY', () => {
          expect(globalize.formatDate(new moment('10/09/15', 'DD/MM/YY'))).toBe('10 Sep 15', 'parsing 10/09/15 ');
        });
        it('should parse DD/MM/YYYY', () => {
          expect(globalize.formatDate(new moment('10/09/2015', 'DD/MM/YYYY'))).toBe('10 Sep 15', 'parsing 10/09/2015 ');
        });
        it('should parse DD/MM/YY', () => {
          expect(globalize.formatDate(new moment('10-09-15', 'DD-MM-YY'))).toBe('10 Sep 15', 'parsing 10-09-15 ');
        });
        it('should parse DD-MM-YYYY', () => {
          expect(globalize.formatDate(new moment('10-09-2015', 'DD-MM-YYYY'))).toBe('10 Sep 15', 'parsing 10-09-2015 ');
        });
        it('should parse DD MM YY', () => {
          expect(globalize.formatDate(new moment('10 09 15', 'DD MM YY'))).toBe('10 Sep 15', 'parsing 10 09 15 ');
        });
        it('should parse DD MM YYYY', () => {
          expect(globalize.formatDate(new moment('10 09 2015', 'DD MM YYYY'))).toBe('10 Sep 15', 'parsing 10 09 2015 ');
        });
      });

      describe('should parse strings into moment objects', () => {
        it('should recognise a date from string in ISO_8601 format', () => {
          let dateToCheck = globalize.parseDate('2015-09-10T00:00:00.000Z');
          let momentObject = new moment('2015-09-10T00:00:00.000Z', moment.ISO_8601);
          expect(dateToCheck._d.getTime()).toEqual(momentObject._d.getTime());
        });
        it('should recognise a date from string in D MMMM YYYY format', () => {
          let dateToCheck = globalize.parseDate('10 September 2015');
          let momentObject = new moment('10 September 2015', 'D MMMM YYYY');
          expect(dateToCheck._d.getTime()).toEqual(momentObject._d.getTime());
        });
        it('should recognise a date from string in D MMM YY format', () => {
          let dateToCheck = globalize.parseDate('10 Sep 15');
          let momentObject = new moment('10 Sep 15', 'D MMM YY');
          expect(dateToCheck._d.getTime()).toEqual(momentObject._d.getTime());
        });
        it('should recognise a date from string in DD/MM/YY format', () => {
          let dateToCheck = globalize.parseDate('10/09/15');
          let momentObject = new moment('10/09/15', 'DD/MM/YY');
          expect(dateToCheck._d.getTime()).toEqual(momentObject._d.getTime());
        });
        it('should recognise a date from string in DD/MM/YYYY format', () => {
          let dateToCheck = globalize.parseDate('10/09/2015');
          let momentObject = new moment('10/09/2015', 'DD/MM/YYYY');
          expect(dateToCheck._d.getTime()).toEqual(momentObject._d.getTime());
        });
        it('should recognise a date from string in DD-MM-YY format', () => {
          let dateToCheck = globalize.parseDate('10-09-15');
          let momentObject = new moment('10-09-15', 'DD-MM-YY');
          expect(dateToCheck._d.getTime()).toEqual(momentObject._d.getTime());
        });
        it('should recognise a date from string in DD-MM-YYYY format', () => {
          let dateToCheck = globalize.parseDate('10-09-2015');
          let momentObject = new moment('10-09-2015', 'DD-MM-YYYY');
          expect(dateToCheck._d.getTime()).toEqual(momentObject._d.getTime());
        });
        it('should recognise a date from string in DD MM YY format', () => {
          let dateToCheck = globalize.parseDate('10 09 15');
          let momentObject = new moment('10 09 15', 'DD MM YY');
          expect(dateToCheck._d.getTime()).toEqual(momentObject._d.getTime());
        });
        it('should recognise a date from string in DD MM YYYY format', () => {
          let dateToCheck = globalize.parseDate('10 09 2015');
          let momentObject = new moment('10 09 2015', 'DD MM YYYY');
          expect(dateToCheck._d.getTime()).toEqual(momentObject._d.getTime());
        });
      });
    });
  });
});
