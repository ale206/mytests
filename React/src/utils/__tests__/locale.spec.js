import  {getCurrentLocale} from '../locale';

describe('locale test', () => {
  it('should import locale', () => {
    expect(getCurrentLocale).toBeDefined();
  });

  it('Should deafult to en-GB', () => {
    expect(getCurrentLocale()).toBe('en-GB');
  });
});
