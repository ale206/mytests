
// temporarily adding this until GetBookingsSubPositions and GetBookingsPositions return Promises
// async autocomplete editor does not accept promises so need to change that first
let xxx = window.xxx || {};
xxx.api = xxx.api || {
  invoke: () => {}
};
xxx.Services = xxx.Services || {
  BookingAttributes: () => {},
  MediaPlan: () => {},
  BookingCategories: () => {},
  BeCampaign: () => {},
  ClientCommission: () => {}
};

let webAPIUtils = {

  MediaPlan: {
    loadPlanData({campaignId}) {
      return new Promise((resolve, reject) => {
        let settings = {
          onError: (e) => {reject(e); }
        };

        xxx.Services.MediaPlan.LoadPlanDataForCampaign(campaignId, function(testPlanData) {
          resolve(testPlanData);
        }, settings);
      });
    },

    savePlanData({dataRowsIn, planId, colCustomFormulas, versionId}) {
      return new Promise((resolve, reject) => {
        let ajaxSettings = {
          onError: (e) => { reject(e); }
        };
        xxx.Services.MediaPlan.SavePlanData(dataRowsIn, planId, colCustomFormulas, versionId, function(savePlanResult) {
          resolve(savePlanResult);
        }, ajaxSettings);
      });
    },

    GetCompensationPots(suppIds, advertiserId = 0) {
      return new Promise((resolve, reject) => {
        let ajaxSettings = {
          onError: (e) => { reject(e); }
        };
        xxx.Services.MediaPlan.GetCompensationPots(suppIds, advertiserId, (compenstationPots) => {
          resolve(compenstationPots);
        }, ajaxSettings);
      });
    },

    GetStoredRatesBySupplierAndCampaign(supplierId, campaignId) {
      return new Promise((resolve, reject) => {
        let ajaxSettings = {
          onError: (e) => { reject(e); }
        };
        xxx.Services.StoredRates.GetBySupplierAndCampaign(supplierId, campaignId, (result) => {
          resolve(result);
        }, ajaxSettings);
      });
    },

    GetSupplierDocuments(supplierId, docTypes, viewDocumentsLinkId, includeLinkDescription) {
      return new Promise((resolve, reject) => {
        let ajaxSettings = {
          onError: (e) => { reject(e); }
        };
        xxx.Services.AgencySupplierDocument.GetAgencySupplierDocuments(supplierId, docTypes, viewDocumentsLinkId, includeLinkDescription, (result) => {
          resolve(result);
        }, ajaxSettings);
      });
    }
  },

  BookingAttributes: {
    GetBookingsFormats({mediaId}) {
      return new Promise((resolve, reject) => {
        let settings = {
          onError: (e) => {reject(e); }
        };
        xxx.Services.BookingAttributes.GetBookingsFormats(mediaId, (result) => {
          resolve(result);
        }, settings);
      });
    },
    GetBookingsSubPositions: xxx.Services.BookingAttributes.GetBookingsSubPositions,
    GetBookingsPositions: xxx.Services.BookingAttributes.GetBookingPositions,
    GetBookingCostTypes: xxx.Services.BookingAttributes.GetBookingCostTypes,
    GetBookingsColourCodes: xxx.Services.BookingAttributes.GetBookingsColourCodes,
    GetBookingRateTypes: xxx.Services.BookingAttributes.GetBookingRateTypes,
    GetBookingSubRateTypes: xxx.Services.BookingAttributes.GetBookingSubRateTypes,
    GetBookingMarkets: xxx.Services.BookingAttributes.GetBookingMarkets
  },

  BookingCategories: {
    GetUsersBookingCategories(onSuccess) {
      return new Promise((resolve, reject) => {
        let settings = {
          onError: (e) => {reject(e); }
        };
        xxx.Services.BookingCategories.GetUsersBookingCategories(onSuccess, (result) => {
          resolve(onSuccess(result));
        }, settings);
      });
    },
    GetBookingCategories(supplierId, mediaId, includeDisabled, validFrom, validTo, onSuccess) {
      return new Promise((resolve, reject) => {
        let settings = {
          onError: (e) => {reject(e); }
        };
        xxx.Services.BookingCategories.GetBookingCategories(supplierId, mediaId, includeDisabled, validFrom, validTo, onSuccess, (result) => {
          resolve(onSuccess(result));
        }, settings);
      });
    },
    GetBookingCategoriesByBePublication(bePubId, mediaId, includeDisabled, validFrom, validTo, onSuccess) {
      return new Promise((resolve, reject) => {
        let settings = {
          onError: (e) => { reject(e); }
        };
        xxx.Services.BookingCategories.GetBookingCategoriesByBePublication(bePubId, mediaId, includeDisabled, validFrom, validTo, onSuccess, (result) => {
          resolve(onSuccess(result));
        }, settings);
      });
    }
  },

  BeCampaigns: {
    GetBeCampaigns: xxx.Services.BeCampaign.GetMyBeCampaigns
  },

  CampaignAvailableCommissions: {
    GetForCampaign: xxx.Services.ClientCommission.GetForCampaign
  },

  api: {
    invoke: xxx.api.invoke
  },

  PlacementEditor: {
    getTraffickingTemplates() {
      return window["PlacementEditorPageData"].columns;
    },

    getPlacementDataRows() {
      return window["PlacementEditorPageData"].rows;
    }
  }

};

export default webAPIUtils;
