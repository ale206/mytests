import CostCalculationsUtils from '../costs';

describe('Calculations for costs', () => {
  // Calculate Margin
  it('should calculate Margin from valid Payable and Billable figures', () => {
    let margin = CostCalculationsUtils.calculateMargin(800, 1000);
    expect(margin).toBe(0.2);
  });
  it('should calculate Margin rounded to 5 d.p.', () => {
    let margin = CostCalculationsUtils.calculateMargin(800, 1100);
    expect(margin).toBe(0.27273);
  });
  it('should return 0 Margin for equal Payable and Billable figures', () => {
    let margin = CostCalculationsUtils.calculateMargin(800, 800);
    expect(margin).toBe(0);
  });
  it('should return 0 Margin for equal Payable and Billable figures if they\'re 0', () => {
    let margin = CostCalculationsUtils.calculateMargin(0, 0);
    expect(margin).toBe(0);
  });
  it('should return NaN Margin for invalid Payable', () => {
    let margin = CostCalculationsUtils.calculateMargin(-1, 1000);
    expect(margin).toBeNaN();
  });
  it('should return NaN Margin for invalid Billable', () => {
    let margin = CostCalculationsUtils.calculateMargin(800, -1);
    expect(margin).toBeNaN();
  });

  // Calculate Payable
  it('should calculate Payable from valid Billable and Margin figures', () => {
    let pay = CostCalculationsUtils.calculatePayable(1000, 0.2);
    expect(pay).toBe(800.0);
  });
  it('should calculate Payable from a 0 Billable figure', () => {
    let pay = CostCalculationsUtils.calculatePayable(0, 0.2);
    expect(pay).toBe(0);
  });
  it('should calculate Payable rounded to 2 d.p.', () => {
    let pay = CostCalculationsUtils.calculatePayable(1333, 0.27273);
    expect(pay).toBe(969.45);
  });
  it('should return NaN Payable for invalid Margin', () => {
    let pay = CostCalculationsUtils.calculatePayable(1000, 1);
    expect(pay).toBeNaN();
  });
  it('should return NaN Payable for invalid Billable', () => {
    let pay = CostCalculationsUtils.calculatePayable(-1, 0.3);
    expect(pay).toBeNaN();
  });

  // Calculate Billable
  it('should calculate Billable from valid Payable and Margin figures', () => {
    let bill = CostCalculationsUtils.calculateBillable(800, 0.2);
    expect(bill).toBe(1000.0);
  });
  it('should calculate Billable from a 0 Payable figure', () => {
    let bill = CostCalculationsUtils.calculateBillable(0, 0.2);
    expect(bill).toBe(0);
  });
  it('should calculate Billable rounded to 2 d.p.', () => {
    let bill = CostCalculationsUtils.calculateBillable(833, 0.27273);
    expect(bill).toBe(1145.38);
  });
  it('should return NaN Billable for invalid Margin', () => {
    let bill = CostCalculationsUtils.calculateBillable(800, 1);
    expect(bill).toBeNaN();
  });
  it('should return NaN Billable for invalid Payable', () => {
    let bill = CostCalculationsUtils.calculateBillable(-1, 0.3);
    expect(bill).toBeNaN();
  });
});
