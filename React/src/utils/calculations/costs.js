// To be exported
let CostCalculationsUtils = {
  // Calculates Margin based on a Payable and a Billable figure. NaN is returned if Margin can't be calculated
  calculateMargin(pay, bill) {
    let margin = NaN;
    try {
      if (pay === 0 && bill === 0) {
        margin = 0;
      } else if (pay > 0 && bill > 0) {
        margin = 1 - (pay / bill);
      }
    } catch (ex) {}

    return isNaN(margin) ? margin : Math.round(margin * 100000) / 100000;
  },

  // Calculates Payable from a Billable and a Margin figure. NaN is returned if Payable can't be calculated
  calculatePayable(bill, margin) {
    let pay = NaN;
    try {
      if (margin !== 1 && bill >= 0) {
        pay = bill * (1 - margin);
      }
    } catch (ex) { }

    return isNaN(pay) ? pay : Math.round(pay * 100) / 100;
  },

  // Calculates Billable from a Payable and a Margin figure. NaN is returned if Billable can't be calculated
  calculateBillable(pay, margin) {
    let bill = NaN;
    try {
      if (margin !== 1 && pay >= 0) {
        bill = pay / (1 - margin);
      }
    } catch (ex) { }

    return isNaN(bill) ? bill : Math.round(bill * 100) / 100;
  }
};

export default CostCalculationsUtils;
