import moment from 'moment';

module.exports = {
  orderByDate: function(a, b) {
    let momentA = moment(a.date);
    let momentB = moment(b.date);

    let value = 0;
    if (momentA.isBefore(momentB)) {
      value = 1;
    } else if (momentA.isAfter(momentB)) {
      value = -1;
    }

    return value;
  }
};
