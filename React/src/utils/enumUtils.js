module.exports = {

   getObjectFromValue : function (enumObj, value) {
     for (var key in enumObj) {
       if(enumObj[key] === value){
         return enumObj;
       }
     }
   },
   getObjectFromDescription : function (enumObj, description) {
     for (var key in enumObj) {
       if(key === description){
         return enumObj;
       }
     }
   },
   getValue : function (enumObj, description) {
     for (var key in enumObj) {
       if(key === description){
         return enumObj[key];
       }
     }
   },
   getName : function (enumObj, value) {
      for (var key in enumObj) {
        if(enumObj[key] === value){
          return key;
        }
      }
   },
   transpose: function (obj) {
      var transposed = {};
      for (var key in obj) {
          transposed[obj[key]] = key;
      }
      return transposed;
   }
 }
