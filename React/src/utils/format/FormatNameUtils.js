
let formatNameUtils = {

  isPackage(text) {
    return text.indexOf('|') > -1;
  },

  parse(text) {
    let formats = [];
    let packageName = this.isPackage(text) ? text.split('|')[0].trim() : '';
    let formatText = text.trim();
    if (formatText !== '') {
      if (this.isPackage(formatText)) {
        let concatenatedFormats = formatText.split('|')[1];
        if (concatenatedFormats !== '' && concatenatedFormats !== null && concatenatedFormats !== undefined) {
          formats = concatenatedFormats.trim().split(',').map(f => f.trim());
        }
      } else {
        formats = formatText.split(',').map(f => f.trim());
      }
    }
    return {packageName, formats, numberOfFormats: formats.length};
  },

  parseFormatsFromText(text) {
    return this.parse(text).formats;
  },

  parsePackageNameFromText(text) {
    return this.parse(text).packageName;
  },

  parseNumberOfFormats(text) {
    return this.parse(text).numberOfFormats;
  },

  createFormatPackageText(packageName, savedFormats) {
    let formatText = '';
    if (savedFormats.length === 1) {
      formatText = savedFormats[0];
    } else {
      formatText = savedFormats.join(',');
    }
    if (packageName !== null && packageName !== undefined && packageName !== '') {
      formatText = `${packageName} | ${formatText}`;
    }
    return formatText;
  }
};

export default formatNameUtils;
