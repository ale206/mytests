import moment from 'moment';
import {getCurrentLocale} from './locale';
import createFormatCache from 'intl-format-cache';
import _ from 'underscore';
let locale = getCurrentLocale();
let getNumberFormat = createFormatCache(Intl.NumberFormat);
moment.locale(locale);

let defaultNumberFormatOptions = {
  Integer: { maximumFractionDigits: 0 },
  Number: { maximumFractionDigits: 2, minimumFractionDigits: 2 },
  Currency: { maximumFractionDigits: 2, minimumFractionDigits: 2 },
  Percentage: { style: 'percent', minimumFractionDigits: 2 },
  UnitRate: { minimumFractionDigits: 2, maximumFractionDigits: 5 }
};

function parseNumber(str) {
  let value;
  if (str == null || typeof str === 'number') {
    return str;
  }
  if (_.isFunction(Number.parseLocale)) {
    value =  Number.parseLocale(str);
  } else {
    value = parseFloat(str);
  }
  return value;
}


function getNumberFormatter(value, itemType, overrideOptions, maxDp) {
  let options = {};
  if (_.isObject(overrideOptions)) {
    options = overrideOptions;
  }
  if (_.isNumber(overrideOptions)) {
    options.minimumFractionDigits = overrideOptions;
  }
  if (_.isNumber(maxDp)) {
    options.maximumFractionDigits = maxDp;
  }
  let numberFormatOptions = Object.assign(defaultNumberFormatOptions[itemType] || {}, options);
  if (numberFormatOptions.minimumFractionDigits > numberFormatOptions.maximumFractionDigits) {
    numberFormatOptions.maximumFractionDigits = numberFormatOptions.minimumFractionDigits;
  }
  return getNumberFormat(getCurrentLocale(), numberFormatOptions);
}

function _isValueEmptyOrNull(value) {
  return !value || value === null || value === '';
}

function formatNumberAuto(value, itemType, options, maxDp) {
  let numToFormat = value;

  if (_isValueEmptyOrNull(value)) {
    numToFormat = 0;
  }

  let formatter = getNumberFormatter(numToFormat, itemType, options, maxDp);
  if (_.isString(numToFormat)) {
    numToFormat = parseNumber(numToFormat);
  }
  return formatter.format(_.isNumber(numToFormat) ? +numToFormat : numToFormat);
}

function formatDate(date, format = 'DD MMM YY', defaultValue = '') {
  let formattedDate;
  locale = getCurrentLocale();
  moment.locale(locale);
  if (typeof date === 'string') {
    // Try to parse from ISO8601 format, Unix ms timestamp and finally 'DD MMM YY' before
    // falling backing to moment default.
    let asMoment = moment(date, [moment.ISO_8601, 'YYYYMMDD', 'DD MMM YYYY', 'DD MMM YY', 'DD MM YY', 'DD MM YYYY', 'DD-MM-YY', 'DD-MM-YYYYY', 'DD/MM/YY', 'DD/MM/YYYY', 'x']);
    formattedDate = asMoment.isValid() ? asMoment.format(format) : defaultValue;
  } else {
    let asMoment = moment(date);
    formattedDate = asMoment.isValid() ? asMoment.format(format) : defaultValue;
  }
  return formattedDate || defaultValue;
}

function parseDate(date, forceStrict) {
  return moment(date, [moment.ISO_8601, 'YYYYMMDD', 'DD MMM YYYY', 'DD MMM YY', 'DD MM YY', 'DD MM YYYY', 'DD-MM-YY', 'DD-MM-YYYYY', 'DD/MM/YY', 'DD/MM/YYYY'], forceStrict || false);
}

function tryParseNumber(str) {
  let canParse = false;
  try {
    Number.parseLocale(str);
    canParse = true;
  } catch(r) {
    canParse = false;
  }
  return canParse;
}

export default {
  formatNumberAuto: formatNumberAuto,
  tryParseNumber: tryParseNumber,
  parseNumber: parseNumber,
  formatDate: formatDate,
  parseDate: parseDate
};
export {formatNumberAuto};
export {tryParseNumber};
export {parseNumber};
export {formatDate};
export {parseDate};
