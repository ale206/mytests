import React, {PropTypes} from 'react';
import ReactDOM from 'react-dom';
import jQuery from 'jquery';

class IframeContainer extends React.Component {
  componentDidMount() {
    if (this.props.autoResize) {
      let iframe = ReactDOM.findDOMNode(this.refs.iframe);
      if (iframe.attachEvent) {
        iframe.attachEvent('onload', () => {
          this.autoResize();
        });
      } else {
        iframe.onload = () => {
          this.autoResize();
        };
      }
    }
  }

  render() {
    let style = {
      overflow: this.props.allowOverflow ? 'auto' : 'hidden',
      border: 'none',
      width: this.props.width,
      height: this.props.height
    };
    let scrollable = this.props.allowOverflow ? 'yes' : 'no';
    return (
      <iframe
        ref="iframe"
        scrolling={scrollable}
        onLoad={this.removePadding}
        style={style}
        src={this.props.url}
      />
    );
  }

  autoResize() {
    setTimeout(() => {
      let iframe = ReactDOM.findDOMNode(this.refs.iframe);
      let newHeight = iframe.contentWindow.document .body.scrollHeight;
      let newWidth = iframe.contentWindow.document .body.scrollWidth;
      iframe.height = (newHeight) + 'px';
      iframe.width = (newWidth) + 'px';
      iframe.contentWindow.document.body.style.paddingTop = '0px';
    }, 2000);
  }
}

IframeContainer.defaultProps = {
  width: '100%',
  height: jQuery(window).height(),
  allowOverflow: false
};

IframeContainer.propTypes = {
  url: PropTypes.string.isRequired,
  autoResize: PropTypes.bool,
  width: PropTypes.string,
  height: PropTypes.string,
  allowOverflow: PropTypes.bool
};

export default IframeContainer;
