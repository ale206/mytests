Object.assign = require('object-assign');
import globalize from './globalize';
import locale from './locale';
window.xxxxUI = window.xxxxUI || {};
window.xxxxUI.utils = {};
Object.assign(window.xxxxUI.utils, {
  globalize: globalize,
  locale: locale
});
