let cookieName = 'language';
function getCookie(cname) {
  let name = cname + '=';
  let ca = document.cookie.split(';');
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i].trim();
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }
  return '';
}

function getLanguageFromQueryString() {
  let qs = (function(a) {
      if (a === '') return {};
      let b = {};
      for (let i = 0; i < a.length; ++i) {
        let p = a[i].split('=', 2);
        if (p.length === 1) {
          b[p[0]] = '';
        } else {
          b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, ' '));
        }
      }
      return b;
    })(window.location.search.substr(1).split('&'));
  return qs.lang;
}

function createLanguageCookie(value) {
  document.cookie = cookieName + '=' + value + '; path=/';
}

let currentLocale = null;

function getCultureName() {
  if (window.xxxGlobalPageData !== null && window.xxxGlobalPageData !== '' && window.xxxGlobalPageData !== undefined) {
    currentLocale = window.xxxGlobalPageData.CultureName;
  }
  return currentLocale;
}

function getCurrentLocale() {
  if (currentLocale === null) {
    currentLocale = getLanguageFromQueryString() || getCookie(cookieName) || getCultureName() || 'en-GB';
  }
  return currentLocale;
}

function setLocale(args) {
  if (!args || !args.code) return;
  currentLocale = args.code;
  if (args.storeCookie) { createLanguageCookie(currentLocale); }
  if (args.reload) { window.location.reload(); }
}

export default {
  getCurrentLocale: getCurrentLocale,
  setLocale: setLocale
};
export {getCurrentLocale};
export {setLocale};
