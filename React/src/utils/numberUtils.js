module.exports = {
  // decimalPlaces
  // -------------
  // Returns how many relevant decimal places a number has, between a max and min value
  // Examples:
  //  numberUtils.decimalPlaces(10.5, 2, 5) would return 2
  //  numberUtils.decimalPlaces(10.525, 2, 5) would return 3
  //  numberUtils.decimalPlaces(10.52539856, 2, 5) would return 5
  decimalPlaces : function (number, minDp, maxDp) {
    // Validate args
    if (typeof number !== 'number' || typeof minDp !== 'number' || typeof maxDp !== 'number') {
      throw new TypeError("cannot operate decimalPlaces on a non number parameter!");
    }
    if (number == null) {
      number = 0;
    }
    if (minDp == null) {
      minDp = 2;
    }
    if (maxDp == null) {
      return minDp;
    }

    // Determines how many we need
    let decimalPlaces = minDp;
    let decimalPart = Math.round((number % 1) * Math.pow(10, maxDp)) / Math.pow(10, maxDp);

    let limit = 1 / Math.pow(10, maxDp + 1);

    for (let div = Math.pow(10, decimalPlaces); decimalPlaces < maxDp; decimalPlaces++, div *= 10) {
      if ((decimalPart * div % 1) <= limit) {
        break;
      }
    }

    return decimalPlaces;
  },
  // flagSplit
  // -------------
  // Transforms a number into an array containing it's individual binary flag numbers.
  // Examples:
  //  numberUtils.flagSplit(11) would return [8, 2, 1]
  //  numberUtils.flagSplit(4) would return [4]
  //  numberUtils.flagSplit(0) would return []
  flagSplit : function () {
    let currentValue = arguments[0];
    if (typeof currentValue !== 'number') {
      throw new TypeError("cannot operate flagSplit on a non number parameter!");
    }

    // go through all the flags and pick individual values
    let arrValue = [];
    let count = 0;
    while (currentValue > 0) {
      let currentPower = Math.pow(2, count);

      if ((currentValue & currentPower) > 0) {
        arrValue.push(currentPower.toString());
      }
      currentValue = currentValue ^ currentPower;
      count++;
    }

    return arrValue;
  }
}
