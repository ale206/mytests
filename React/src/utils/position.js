var jq = require("jquery");

module.exports = {
  bringToFront : function (element) {
      var maxZ = Math.max.apply(null, jq.map(jQuery('body *'), function (e, n) {
        var foundInt = parseInt(jq(e).css('z-index')) || 1;
        //ignore int max set by some random chrome extension
        if (foundInt === 2147483647) {
            foundInt = 1;
        }
        return foundInt;
      }));
      var currentZ = parseInt(jq(element).css("zIndex"));
      if (isNaN(currentZ) || currentZ < maxZ) {
        jq(element).css({ zIndex: maxZ + 1 });
      }
   }
}
