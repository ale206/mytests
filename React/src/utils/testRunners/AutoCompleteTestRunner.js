import TestUtils from 'react-addons-test-utils';
import ComponentTestRunner from './ComponentTestRunner';
const componentSelector = '.react-autocomplete-Result';

class AutoCompleteEditorTestRunner extends ComponentTestRunner {
  constructor({component = null, renderIntoBody = false}) {
    super({renderIntoBody: renderIntoBody, component: component});
  }

  clickInput() {
    TestUtils.Simulate.click(this.componentInstance.getInputNode());
    return this;
  }

  clickResult(index) {
    let results = this.getComponentsWithSelector(this.componentInstance, componentSelector);
    TestUtils.SimulateNative.mouseOver(results[index]);
    TestUtils.Simulate.click(results[index]);
    return this;
  }

  getRendereredResults() {
    return this.getComponentsWithSelector(this.componentInstance, componentSelector);
  }

  getRendereredResultCount() {
    let results = this.getComponentsWithSelector(this.componentInstance, componentSelector);
    return results.length;
  }

  getValue() {
    return this.componentInstance.getValue();
  }
}

export default AutoCompleteEditorTestRunner;
