import TestUtils from 'react-addons-test-utils';
import ComponentTestRunner from './ComponentTestRunner';
const componentSelector = '.react-autocomplete-Result';

class CascadeAutoCompleteTestRunnerHelpers extends ComponentTestRunner {
  constructor({component = null, renderIntoBody = false}) {
    super({renderIntoBody: renderIntoBody, component: component});
  }

  commitResult(resultToClick) {
    this.clickInput().clickResult(resultToClick);
    return this;
  }

  commitResultWithClick(autoCompleteIndex, resultToClick) {
    for (let i = 0; i <= autoCompleteIndex; i++) {
      this.commitResult(resultToClick);
    }
    return this;
  }

  commitResultWithTab(autoCompleteIndex, resultToClick, shiftKey = false) {
    for (let i = 0; i <= autoCompleteIndex; i++) {
      this.clickInput().tabOnResult(resultToClick, shiftKey);
    }
    return this;
  }

  clickInput() {
    let inputNode = this.componentInstance.getInputNode();
    TestUtils.Simulate.click(inputNode);
    return this;
  }

  clickResult(index) {
    let results = this.getComponentsWithSelector(this.componentInstance, componentSelector);
    TestUtils.SimulateNative.mouseOver(results[index]);
    TestUtils.Simulate.click(results[index]);
    return this;
  }

  tabOnResult(index, shiftKey) {
    let results = this.getComponentsWithSelector(this.componentInstance, componentSelector);
    TestUtils.SimulateNative.mouseOver(results[index]);
    TestUtils.Simulate.keyDown(results[index], {key: 'Tab', shiftKey});
    return this;
  }

  getRendereredResults() {
    return this.getComponentsWithSelector(this.componentInstance, componentSelector);
  }

  getRendereredResultCount() {
    let results = this.getComponentsWithSelector(this.componentInstance, componentSelector);
    return results.length;
  }

  getValue() {
    return this.componentInstance.getValue();
  }

  getCurrentNumberOfAutoCompletes() {
    return this.componentInstance.getCurrentNumberOfAutoCompletes();
  }
}

export default CascadeAutoCompleteTestRunnerHelpers;
