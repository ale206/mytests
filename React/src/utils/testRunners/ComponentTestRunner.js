import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';
import getComponentsWithSelector from '../../../test/utils/getComponentsWithSelector';

class ComponentTestRunner {

  constructor({component = null, renderIntoBody = false}) {
    this.renderIntoBody = renderIntoBody;
    this.containerElement = document.createElement('div');
    document.body.appendChild(this.containerElement);
    this.component = component;
    this.componentInstance = this._renderComponent(component, renderIntoBody);
    this.getComponentsWithSelector = getComponentsWithSelector;
  }

  _renderComponent(component, intoBody) {
    return intoBody ?
     ReactDOM.render(component, this.containerElement)
     : TestUtils.renderIntoDocument(component);
  }

  getComponentDOMNode(nodeSelector) {
    return this.getComponentsWithSelector(this.componentInstance, nodeSelector);
  }
  getComponentDOMNodes(selector) {
    return [].slice.call(this.getComponentDOMNode(selector)); // return a real JS array!
  }
  changeNodeValue(value, nodeSelector, index = 0) {
    let node = this.getComponentDOMNode(nodeSelector)[index];
    TestUtils.Simulate.change(node, { target: { value } });
  }

  simulateKeyDownAndSpy(key, domNode, shiftKey) {
    domNode.focus();
    let fakeEvent = {
      key,
      stopPropagation: jasmine.createSpy(),
      preventDefault: jasmine.createSpy(),
      shiftKey: shiftKey
    };
    TestUtils.Simulate.keyDown(domNode, fakeEvent);

    return fakeEvent;
  }

  pressKeyDown(key, selector, shiftKey = false) {
    let result = this.getComponentDOMNode(selector);

    if (result && result.length > 0) {
      return this.simulateKeyDownAndSpy(key, result[0], shiftKey);
    }

    return null;
  }

  dispose() {
    if (this.renderIntoBody) {
      ReactDOM.unmountComponentAtNode(this.containerElement);
    }
    this.componentInstance = null;
  }
}

export default ComponentTestRunner;
