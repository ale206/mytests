import TestUtils from 'react/lib/ReactTestUtils';
import ReactDOM from 'react-dom';
import FormatNameEditor, {SavedFormatNameList} from '../../modules/mediaPlan/js/components/editors/FormatNameEditor.jsx';
import ComponentTestRunner from './ComponentTestRunner';

class FormatNameEditorTestRunner extends ComponentTestRunner {
  constructor({renderIntoBody = false, component = FormatNameEditor}) {
    super({renderIntoBody: renderIntoBody, component: component});
  }

  getFormatItemNode(index) {
    let savedFormatList = TestUtils.findRenderedComponentWithType(this.componentInstance, SavedFormatNameList);
    return TestUtils.scryRenderedDOMComponentsWithClass(savedFormatList, 'list-group-item')[index];
  }

  getPackageNameNode() {
    return ReactDOM.findDOMNode(this.componentInstance.refs.multiSelect.refs.selectionName.refs.inputNode);
  }

  getPackageName() {
    return this.getPackageNameNode().value;
  }

  getRenderedFormats() {
    let savedFormatList = TestUtils.findRenderedComponentWithType(this.componentInstance.refs.multiSelect, SavedFormatNameList);
    let formatItems = TestUtils.scryRenderedDOMComponentsWithClass(savedFormatList, 'list-group-item');
    return formatItems.map(f => {
      return ReactDOM.findDOMNode(f).innerText;
    });
  }
}

export default FormatNameEditorTestRunner;
