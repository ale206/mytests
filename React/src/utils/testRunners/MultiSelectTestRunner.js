import TestUtils from 'react-addons-test-utils';
import ComponentTestRunner from './ComponentTestRunner';
import {SelectedOptionList} from '../../components/Grid/Editors/MultiSelectEditor';
import SelectionName from '../../components/General/SelectionName';
import Alert from '../../components/General/Alert';
import ReactSelectTestUtils from '../testUtils/reactSelectUtils';
import ReactDOM from 'react-dom';

class MultiSelectTestRunner extends ComponentTestRunner {

  constructor({component = null, renderIntoBody = false}) {
    super({renderIntoBody: renderIntoBody, component: component});
    this.reactSelectTestUtils = new ReactSelectTestUtils(this.componentInstance);
  }

  findAndFocusSelect() {
    return this.reactSelectTestUtils.findAndFocusSelect();
  }

  clickSelect() {
    this.reactSelectTestUtils.clickSelect();
    return this;
  }

  typeText(text) {
    this.reactSelectTestUtils.typeText(text);
    return this;
  }

  selectItemWithMouse(index) {
    this.reactSelectTestUtils.selectItemWithMouse(index);
    return this;
  }

  clickDeleteSelectedOption(index) {
    let deleteBtn = this.getDeleteButton(index);
    TestUtils.Simulate.click(deleteBtn);
    return this;
  }

  pressKeyDeleteSelectedOption(index, key) {
    let deleteBtn = this.getDeleteButton(index);
    TestUtils.Simulate.keyDown(deleteBtn, {key: key});
    return this;
  }

  getSelect() {
    return this.reactSelectTestUtils.getSelect();
  }

  getSelectedItems() {
    return this.componentInstance.state.selected;
  }

  getSelectOptionNodes() {
    return this.reactSelectTestUtils.getSelectOptionNodes();
  }

  getAlertBox() {
    return TestUtils.findRenderedComponentWithType(this.componentInstance, Alert);
  }

  getStatusMessage() {
    let alertInstance = this.getAlertBox();
    return ReactDOM.findDOMNode(alertInstance).querySelector('.js-error-message').innerText;
  }

  getSavedItemNode(index) {
    let selectedOptionList = TestUtils.findRenderedComponentWithType(this.componentInstance, SelectedOptionList);
    return TestUtils.scryRenderedDOMComponentsWithClass(selectedOptionList, 'list-group-item')[index];
  }

  getDeleteButton(index) {
    let selectedOption = this.getSavedItemNode(index);
    return this.getComponentsWithSelector(selectedOption, '.js-delete-option')[0];
  }

  getRenderedSelectedOptions() {
    let selectedOptionList = TestUtils.findRenderedComponentWithType(this.componentInstance, SelectedOptionList);
    let selectedItems = TestUtils.scryRenderedDOMComponentsWithClass(selectedOptionList, 'list-group-item');
    return selectedItems.map(i => {
      return ReactDOM.findDOMNode(i).innerText;
    });
  }

  getSelectionName() {
    return TestUtils.findRenderedComponentWithType(this.componentInstance, SelectionName);
  }

  getRenderedSelectionName() {
    let selectionName = this.getSelectionName();
    return selectionName.refs.inputNode;
  }

  simulateTabAndSpy(domeNode, shiftKey) {
    domeNode.focus();
    let fakeEvent = {
      key: 'Tab',
      stopPropagation: jasmine.createSpy(),
      shiftKey: shiftKey
    };
    TestUtils.Simulate.keyDown(domeNode, fakeEvent);
    return fakeEvent;
  }

  tabLastSelectedOption({shiftKey = false}) {
    let lastOptionIndex = this.getRenderedSelectedOptions().length - 1;
    let deleteButton = this.getDeleteButton(lastOptionIndex);
    return this.simulateTabAndSpy(deleteButton, shiftKey);
  }

  tabDismissStatusMessageButton({shiftKey = false}) {
    let alertInstance = this.getAlertBox();
    let dismissButton = TestUtils.findRenderedDOMComponentWithTag(alertInstance, 'button');
    return this.simulateTabAndSpy(dismissButton, shiftKey);
  }


}

export default MultiSelectTestRunner;
