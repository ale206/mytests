import React, {Component} from 'react';
import ReactPerf from 'react-addons-perf';

export default class PerfContainer extends Component {

  constructor() {
    super();
    this.state = {perfStarted: null, summary: [], visible: false};
    this.onPerfButtonClick = this.onPerfButtonClick.bind(this);
    this.toggleShow = this.toggleShow.bind(this);
  }

  onPerfButtonClick() {
    if (this.state.perfStarted === true) {
      ReactPerf.stop();
      let measurements = ReactPerf.getLastMeasurements();
      let summary = ReactPerf.getMeasurementsSummaryMap(measurements);
      this.setState({summary});
    } else {
      ReactPerf.start();
    }
    let perfStarted = this.state.perfStarted === true ? false : true;
    this.setState({perfStarted: perfStarted});
  }

  getPerfPanel() {
    if (this.state.perfStarted === true) {
      return (<div>I'm recording, click above to stop and show the results..</div>);
    }
    let summary = this.state.summary.map(s => {
      return (
        <div>
          <div className="title react-grid-cell-warning">
            {s['Owner > component']}
          </div>
          <div className="settings-item" style={{paddingLeft: '10px', paddingTop: '10px'}}>
            <span><ul className="list-unstyled">
              <li><strong>Instances:</strong> {s.Instances}</li>
              <li><strong>Wasted Time:</strong> {s['Wasted time (ms)'].toFixed(2)} ms</li>
            </ul></span>
          </div>
        </div>
      );
    });
    return summary;
  }

  toggleShow() {
    this.setState({visible: !this.state.visible});
  }

  render() {
    let cogClass = this.state.visible ? 'theme-config-box show' : 'theme-config-box';
    return (<div>
        {this.props.children};
      <div className="theme-config">
       <div className={cogClass} onClick={this.toggleShow}>
       <div className="spin-icon">
            <i className="fa fa-cogs fa-spin"></i>
        </div>
        <div className="skin-setttings">
            <div className="title">Performance Tools</div>
            <div className="setings-item">
                <span>
                    Perf Tracing
                </span>
                <div className="switch">
                    <div className="onoffswitch">
                        <input type="checkbox" name="collapsemenu" className="onoffswitch-checkbox" id="collapsemenu" onClick={this.onPerfButtonClick}></input>
                        <label className="onoffswitch-label" htmlFor="collapsemenu">
                            <span className="onoffswitch-inner"></span>
                            <span className="onoffswitch-switch"></span>
                        </label>
                    </div>
                </div>
            </div>
            {this.getPerfPanel()}
        </div>
       </div>
      </div>
    </div>);
  }
}

PerfContainer.propTypes = {
  children: React.PropTypes.object
};


