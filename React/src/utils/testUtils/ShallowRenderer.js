import ReactTestUtils from 'react-addons-test-utils';
import React from 'react';

class ShallowRenderer {
  constructor() {
    this.shallowRenderer = ReactTestUtils.createRenderer();
  }

  /**
  * Shallow renders a element to use in test classes.
  * @param {React.Component} component a react component.
  * @param {Object} props a set of props that you want to render with the component.
  */
  render(component, props) {
    let propsClone = Object.assign({}, props);
    let element = React.createElement(component, propsClone);
    this.shallowRenderer.render(element);

    return this.shallowRenderer.getRenderOutput();
  }

  /**
  * Finds all the matches for a component in the DOM tree.
  * @param {Object} tree a dom tree.
  * @param {Object} match a component to match.
  */
  findAllMatching(tree, match) {
    // match the props too?
    return this._findAll(tree, el => {
      if (!el) return false;
      return (match.type ? el.type === match.type : match === el.type);
    });
  }

  /**
  * Finds the first match for a component in the DOM tree.
  * @param {Object} tree a dom tree.
  * @param {Object} match a component to match.
  */
  findFirstMatching(tree, match) {
    let found = this.findAllMatching(tree, match);
    if (found.length !== 1) throw new Error('Did not find exactly one match');
    return found[0];
  }

  /**
   * Traverses the tree and returns all elements that satisfy the function `test`.
   *
   * @param  {ReactElement}   tree the tree to traverse
   * @param  {Function} test  the test for each component
   * @return {Array}          the elements that satisfied `test`
   */
  _findAll(tree, test) {
    let found = test(tree) ? [tree] : [];

    if (React.isValidElement(tree)) {
      if (React.Children.count(tree.props.children) > 0) {
        React.Children.forEach(tree.props.children, (child) => {
          found = found.concat(this._findAll(child, test));
        });
      }
    }

    return found;
  }

  _findAllVanilla(tree, test) {
    let found = test(tree) ? [tree] : [];

    if (tree && tree.props && tree.props.children) {
      if (tree.props.children.length > 0) {
        tree.props.children.forEach(child => {
          found = found.concat(this._findAllVanilla(child, test));
        });
      }
    }

    return found;
  }
}

export default ShallowRenderer;
