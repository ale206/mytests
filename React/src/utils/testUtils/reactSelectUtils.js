import Select from 'react-select';
import TestUtils from 'react-addons-test-utils';
import getComponentsWithSelector from '../../../test/utils/getComponentsWithSelector';

class ReactSelectUtils {
  constructor(componentInstance) {
    this.componentInstance = componentInstance;
    this.selectIndex = 0;
  }

  findAndFocusSelect() {
    let searchInstance = getComponentsWithSelector(this.componentInstance, '.Select-input');
    let searchInputNode = null;
    if (searchInstance && searchInstance.length > this.selectIndex && searchInstance[this.selectIndex]) {
      searchInputNode = searchInstance[this.selectIndex].querySelector('input');
      if (searchInputNode) {
        TestUtils.Simulate.focus(searchInputNode);
      }
    }
    return searchInputNode;
  }

  clickSelect() {
    let selectNode = this.findAndFocusSelect();
    TestUtils.Simulate.mouseDown(selectNode);
    TestUtils.Simulate.click(selectNode);
  }

  typeText(text) {
    let selectNode = this.findAndFocusSelect();
    this.clickSelect();
    TestUtils.Simulate.change(selectNode, { target: { value: text } });
  }

  selectItemWithMouse(index) {
    this.clickSelect();
    let option = this.getSelectOptionNodes()[index];
    TestUtils.Simulate.mouseDown(option);
    TestUtils.Simulate.click(option);
  }

  getSelect() {
    return TestUtils.scryRenderedComponentsWithType(this.componentInstance, Select)[this.selectIndex];
  }

  getSelectOptionNodes() {
    let select = this.getSelect();
    let options = TestUtils.scryRenderedDOMComponentsWithClass(select, 'Select-option');
    return options;
  }
}

export default ReactSelectUtils;

