import {cloneElement} from 'react';
import ReactDOM from 'react-dom';

export function render(element, mountPoint) {
  let mount = mountPoint || document.createElement('div');
  let instance = ReactDOM.render(element, mount);

  if (!instance.renderWithProps) {
    instance.renderWithProps = function(newProps) {
      return render(
       cloneElement(element, newProps), mount);
    };
  }
  return instance;
}
