const typeUtils = {
  MediaPlan: {
    Discovery: {
      getSystemType: (id) => {
        let xxx = window.xxx || { };
        xxx.MediaPlan = xxx.MediaPlan || {
          Discovery: {
            SystemTypes: {
              getSystem: () => { return {}; }
            }
          }
        };

        return xxx.MediaPlan.Discovery.SystemTypes.getSystem(id);
      }
    },
    ElementValueType: () => {
      let xxx = window.xxx || { };
      if (xxx && xxx.Services && xxx.Services.Model) {
        return xxx.Services.Model.MediaPlan.ElementValueType;
      }

      return {};
    }
  }
};

export default typeUtils;
