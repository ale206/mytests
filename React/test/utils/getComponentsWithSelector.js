import ReactDOM from 'react-dom';

function getComponentsWithSelector(componentInstance, selector) {
  let node = ReactDOM.findDOMNode(componentInstance);
  return node.querySelectorAll(selector);
}

export default getComponentsWithSelector;
