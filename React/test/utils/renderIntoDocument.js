import ReactDOM from 'react-dom';

let renderIntoDocument = (instance) => {
  let div = document.createElement('div');
  return ReactDOM.render(instance, div);
};

export default renderIntoDocument;
