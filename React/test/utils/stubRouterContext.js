// react-router requires react context to be set when rendering links, but
// context is not set when rendering components in a test environment.
// This helper sets a dummy context, so that these components may be tested.
// Further information about this method & react context can be found here:
// https://github.com/rackt/react-router/blob/v0.12.4/docs/guides/testing.md
// https://www.tildedave.com/2014/11/15/introduction-to-contexts-in-react-js.html

import React from 'react';

let stubRouterContext = (Component, props, stubs) => {
  return React.createClass({
    getChildContext() {
      return Object.assign({
        makePath() {},
        makeHref() {},
        transitionTo() {},
        replaceWith() {},
        goBack() {},
        getCurrentPath() {},
        getCurrentRoutes() {},
        getCurrentPathname() {},
        getCurrentParams() {},
        getCurrentQuery() {},
        isActive() {}
      }, stubs);
    },

    render() {
      return (
        <Component {...props} />
      );
    },

    childContextTypes: {
      makePath: React.PropTypes.func,
      makeHref: React.PropTypes.func,
      transitionTo: React.PropTypes.func,
      replaceWith: React.PropTypes.func,
      goBack: React.PropTypes.func,
      getCurrentPath: React.PropTypes.func,
      getCurrentRoutes: React.PropTypes.func,
      getCurrentPathname: React.PropTypes.func,
      getCurrentParams: React.PropTypes.func,
      getCurrentQuery: React.PropTypes.func,
      isActive: React.PropTypes.func
    }
  });
};

module.exports = stubRouterContext;
