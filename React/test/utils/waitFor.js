let waitsInProgress = [];

let waitFor = (test, message, done, timeLeft = 100) => {
  waitsInProgress.push(setTimeout(() => {
    if (timeLeft <= 0) {
      fail(message);
      done();
    } else if (test()) {
      done();
    } else {
      waitFor(test, message, done, timeLeft - 10);
    }
  }, 10));
};

waitFor.clear = () => waitsInProgress.map(clearTimeout);

export default waitFor;
