import moment from 'moment';
import React from 'react';
import ReactDOM from 'react-dom';
import ReactDataGrid from 'react-data-grid';
import TestUtils from 'react-addons-test-utils';
import CSSTransitionGroup from 'react-addons-css-transition-group';
import update from 'react-addons-update';
React.addons = {
  update: update,
  TestUtils: TestUtils,
  CSSTransitionGroup: CSSTransitionGroup
};

window.moment = moment;
window.React = React;
window.ReactDOM = ReactDOM;
window.ReactDataGrid = ReactDataGrid;
window.TestUtils = TestUtils;
