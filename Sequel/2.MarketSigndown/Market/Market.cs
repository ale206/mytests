﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Market
{
    public class Market
    {
        public List<Underwriter> MarketSigndown(float TotalOrder, List<Underwriter> underwriters)
        {
            // Sequel - Development Team interview
            // -----------------------------------
            // Please enter your code below this line

            if (underwriters.Count == 0)
                return null;

            var totalWrittenLine = underwriters.Sum(x => x.WrittenLine);

            underwriters = AssignSignedLine(underwriters, totalWrittenLine);
            
            // Please enter your code above this line
            // Please do not change anything else (feel free to add your own private methods to this class if you want to do so)
            return underwriters;
        }

        private List<Underwriter> AssignSignedLine(List<Underwriter> underwriters, float totalWrittenLine)
        {
            for (var i = 0; i < underwriters.Count; i++)
            {
                if (totalWrittenLine <= 100)
                {
                    underwriters[i].SignedLine =  underwriters[i].WrittenLine;
                }
                else
                {
                    var percentageAccordingToWriteLine = underwriters[i].WrittenLine / totalWrittenLine;
                    underwriters[i].SignedLine = percentageAccordingToWriteLine * 100;
                }
            }

            return underwriters;
        }
    }
}