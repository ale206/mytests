﻿using System;
using System.Collections.Generic;

public class Worker : IDisposable
{
    private List<string> workItems = new List<string>();

    public int Id { get; private set; }

    public IEnumerable<string> WorkItems { get { return this.workItems; } }

    private Object thisLock = new Object();

    public Worker(int id)
    {
        this.Id = id;
    }

    public void DoWork(string work)
    {
        lock (thisLock)
        {
            if (this.workItems == null)
                throw new ObjectDisposedException(this.GetType().Name);

            this.workItems.Add(work);
        }
    }

    public void Dispose()
    {
        this.workItems = null;
    }
}

public class Dispatcher
{
    private readonly Dictionary<int, Worker> workers = new Dictionary<int, Worker>();

    public IEnumerable<Worker> Workers { get { return this.workers.Values; } }



    public Worker AcquireWorker(int id)
    {

        Worker w = null;
        if (!this.workers.TryGetValue(id, out w))
        {
            w = new Worker(id);
            this.workers.Add(id, w);
        }

        return w;

    }

    public void ReleaseWorker(int id)
    {
        Worker w = null;
        if (!this.workers.TryGetValue(id, out w))
            throw new ArgumentException();

        w.Dispose();
    }

    public static void Main(string[] args)
    {
        //var d = new Dispatcher();

        var worker1 = new Worker(1);
        var worker2 = new Worker(2);

        Print(worker1, "Work11");
        Print(worker1, "Work12");
        Print(worker2, "Work21");

        //d.AcquireWorker(1).DoWork("Work11");        
        //d.AcquireWorker(2).DoWork("Work21");
        //Console.WriteLine(string.Join(", ", d.AcquireWorker(2).WorkItems));
        //d.ReleaseWorker(2);
        //d.AcquireWorker(1).DoWork("Work12");
        //Console.WriteLine(string.Join(", ", d.AcquireWorker(1).WorkItems));

    }

    public static void Print(Worker worker, string work)
    {
        var d = new Dispatcher();

        d.AcquireWorker(worker.Id).DoWork(work);
        Console.WriteLine(string.Join(", ", d.AcquireWorker(worker.Id).WorkItems));
        d.ReleaseWorker(worker.Id);

    }

}