﻿using System.Reflection;
using Autofac;

namespace IoC
{
    public class AppBuilderExtensions
    {
        /// <summary>
        /// Setup for Consoles
        /// </summary>
        public static IContainer IoCSetup()
        {
            var builder = new ContainerBuilder();

            builder.RegisterAssemblyModules(Assembly.GetExecutingAssembly());

            var container = builder.Build();

            return container;
        }
    }
}