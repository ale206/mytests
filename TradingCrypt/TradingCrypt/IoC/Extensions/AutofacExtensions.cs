﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Autofac;
using Autofac.Builder;
using Autofac.Features.Scanning;

namespace IoC.Extensions
{
    /// <summary>
    /// Extension methods for Autofac
    /// </summary>
    public static class AutofacExtensions
    {
        #region Apis

        /// <summary>
        /// Filter out types not implementing an interface
        /// </summary>
        /// <param name="registration">Builder</param>
        /// <returns>The builder</returns>
        public static IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> WithAtLeastOneInterface(
            this IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> registration)
        {
            return registration.Where(t => t.GetInterfaces().Any());
        }

        /// <summary>
        /// Select only types with the given attribute
        /// </summary>
        /// <typeparam name="TAttribute">Type of the attribute used to select types</typeparam>
        /// <param name="registration">Builder</param>
        /// <returns>The builder</returns>
        public static IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> WithAttribute<TAttribute>(
            this IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> registration)
            where TAttribute : Attribute
        {
            return registration.Where(t => t.GetCustomAttribute<TAttribute>(true) != null);
        }

        /// <summary>
        /// Select only types without the given attribute
        /// </summary>
        /// <typeparam name="TAttribute">Type of the attribute used to select types</typeparam>
        /// <param name="registration">Builder</param>
        /// <returns>The builder</returns>
        public static IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> WithoutAttribute<TAttribute>(
            this IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> registration)
            where TAttribute : Attribute
        {
            return registration.Where(t => t.GetCustomAttribute<TAttribute>(true) == null);
        }

        /// <summary>
        /// Apply a list of filters for types
        /// </summary>
        /// <param name="registration">Builder</param>
        /// <param name="typeFilters">List of predicates for types</param>
        /// <returns>The builder</returns>
        public static IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> ApplyTypeFilters(
            this IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> registration, IEnumerable<Func<Type, bool>> typeFilters)
        {
            if (typeFilters != null)
            {
                //registration.Where(t => typeFilters.All(f => f(t)));
                foreach (var filter in typeFilters)
                {
                    registration.Where(filter);
                }
            }

            return registration;
        }

        /// <summary>
        /// Filters types based on namespaces
        /// </summary>
        /// <param name="registration">Builder</param>
        /// <param name="nameSpaces">List of namespaces to pick types from</param>
        /// <returns>The builder</returns>
        public static IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> ApplyNamespaceFilters(
            this IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> registration, IEnumerable<string> nameSpaces)
        {
            if (nameSpaces != null)
            {
                registration.Where(t => nameSpaces.Any(t.IsInNamespace));
            }

            return registration;
        }

        #endregion
    }
}