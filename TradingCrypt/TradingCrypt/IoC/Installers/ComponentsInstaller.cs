﻿using Autofac;
using TradingCrypt.BusinessLogic;
using TradingCrypt.BusinessLogic.Parsers;
using TradingCrypt.BusinessLogic.Subscriptions;

namespace IoC.Installers
{
    public class ComponentsInstaller : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //var rb = builder
            //.RegisterAssemblyTypes(typeof(Candle).Assembly)
            //.WithAtLeastOneInterface()
            //.PropertiesAutowired();


            builder.RegisterType<WebSocketManager>().As<IWebSocketManager>().PropertiesAutowired();
            builder.RegisterType<CandleResponseParser>().As<ICandleResponseParser>().PropertiesAutowired();
            builder.RegisterType<SubscriptionManager>().As<ISubscriptionManager>().PropertiesAutowired();
        }
    }
}