﻿namespace TradingCrypt.BusinessLogic.Enums
{
    public enum WsEvent
    {
        Info,
        Subscribed
    }
}