﻿using System;

namespace TradingCrypt.BusinessLogic.Helpers
{
    public static class DateHelper
    {
        public static DateTime ConvertFromMilliseconds(long milliseconds)
        {
            var time = TimeSpan.FromMilliseconds(milliseconds);
            var dateTime = new DateTime(1970, 1, 1) + time;

            return dateTime;
        }
    }
}