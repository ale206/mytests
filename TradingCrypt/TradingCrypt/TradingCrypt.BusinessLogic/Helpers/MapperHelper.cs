﻿using System;
using Newtonsoft.Json.Linq;
using TradingCrypt.BusinessLogic.Models;

namespace TradingCrypt.BusinessLogic.Helpers
{
    public static class MapperHelper
    {
        public static Candle MapCandle(JToken jToken)
        {
            var milliseconds = Convert.ToInt64((object) jToken[0].ToString());

            return new Candle()
            {
                Mts = milliseconds,
                Open = float.Parse(jToken[1].ToString()),
                Close = float.Parse(jToken[2].ToString()),
                High = float.Parse(jToken[3].ToString()),
                Low = float.Parse(jToken[4].ToString()),
                Volume = float.Parse(jToken[5].ToString()),
                Date = DateHelper.ConvertFromMilliseconds(milliseconds)
            };
        }
    }
}