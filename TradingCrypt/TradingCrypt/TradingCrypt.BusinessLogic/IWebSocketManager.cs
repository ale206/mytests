﻿namespace TradingCrypt.BusinessLogic
{
    public interface IWebSocketManager
    {
        bool OpenWebSocket();
    }
}