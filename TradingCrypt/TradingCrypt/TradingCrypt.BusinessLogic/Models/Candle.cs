﻿using System;

namespace TradingCrypt.BusinessLogic.Models
{
    public class Candle
    {
        /// <summary>
        /// millisecond time stamp
        /// </summary>
        public long Mts { get; set; }

        /// <summary>
        /// First execution during the time frame
        /// </summary>
        public float Open { get; set; }

        /// <summary>
        /// Last execution during the time frame
        /// </summary>
        public float Close { get; set; }

        /// <summary>
        /// Highest execution during the time frame
        /// </summary>
        public float High { get; set; }

        /// <summary>
        /// Lowest execution during the timeframe
        /// </summary>
        public float Low { get; set; }

        /// <summary>
        /// Quantity of symbol traded within the timeframe
        /// </summary>
        public float Volume { get; set; }

        /// <summary>
        /// Converted Date from milliseconds
        /// </summary>
        public DateTime Date { get; set; }
    }
}