﻿using Newtonsoft.Json;

namespace TradingCrypt.BusinessLogic.Models.Requests
{
    public class CandleRequest
    {
        [JsonProperty(PropertyName = "event")]
        public string Event { get; set; }

        [JsonProperty(PropertyName = "channel")]
        public string Channel { get; set; }

        [JsonProperty(PropertyName = "key")]
        public string Key { get; set; }
    }
}