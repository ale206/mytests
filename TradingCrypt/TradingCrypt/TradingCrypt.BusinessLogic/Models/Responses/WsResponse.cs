﻿using Newtonsoft.Json;
using TradingCrypt.BusinessLogic.Enums;

namespace TradingCrypt.BusinessLogic.Models.Responses
{
    public class WsResponse
    {
        /// <summary>
        /// Event Type
        /// </summary>
        [JsonProperty(PropertyName = "event")]
        public WsEvent Event { get; set; }

        /// <summary>
        /// Version Number
        /// </summary>
        [JsonProperty(PropertyName = "version")]
        public double Version { get; set; }

        /// <summary>
        /// Channel Id
        /// </summary>
        [JsonProperty(PropertyName = "chanId")]
        public int ChanId { get; set; }

        /// <summary>
        /// Channel Description
        /// </summary>
        [JsonProperty(PropertyName = "channel")]
        public string Channel { get; set; }

        /// <summary>
        /// Subscription Key
        /// </summary>
        [JsonProperty(PropertyName = "key")]
        public string Key { get; set; }
    }
}