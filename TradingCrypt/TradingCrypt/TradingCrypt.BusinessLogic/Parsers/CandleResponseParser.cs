﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using TradingCrypt.BusinessLogic.Enums;
using TradingCrypt.BusinessLogic.Helpers;
using TradingCrypt.BusinessLogic.Models;
using TradingCrypt.BusinessLogic.Models.Responses;

namespace TradingCrypt.BusinessLogic.Parsers
{
    public class CandleResponseParser : ICandleResponseParser
    {
        public void ParseMultipleCandlesResponse(JArray response)
        {
            var channel = response[0];

            var candlesList = new List<Candle>();

            //6 are the candle information returned by the web socket.
            //if you receive more than 1 candle, it means you have more than 6 info, then cycle
            if (response[1].Count() > 6)
            {
                foreach (var jToken in response[1])
                {
                    candlesList.Add(MapperHelper.MapCandle(jToken));
                }
            }
            else
            {
                if (response[1].ToString().Equals("hb"))
                    return;

                candlesList.Add(MapperHelper.MapCandle(response[1]));
            }

            //TODO: Save Candle info
        }


        public void ParseSingleCandleResponse(JObject response)
        {
            var wsResponse = response.ToObject<WsResponse>();

            switch (wsResponse.Event)
            {
                case WsEvent.Info:
                    break;
                case WsEvent.Subscribed:
                    //TODO: Save channel
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}