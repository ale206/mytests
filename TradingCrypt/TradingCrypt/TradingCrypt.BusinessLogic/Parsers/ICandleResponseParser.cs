﻿using Newtonsoft.Json.Linq;

namespace TradingCrypt.BusinessLogic.Parsers
{
    public interface ICandleResponseParser
    {
        void ParseMultipleCandlesResponse(JArray response);
        void ParseSingleCandleResponse(JObject response);
    }
}