﻿using WebSocket4Net;

namespace TradingCrypt.BusinessLogic.Subscriptions
{
    public interface ISubscriptionManager
    {
        void SubscribeToCandles(WebSocket websocket);
    }
}