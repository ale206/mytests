﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using TradingCrypt.BusinessLogic.Models.Requests;
using WebSocket4Net;

namespace TradingCrypt.BusinessLogic.Subscriptions
{
    public class SubscriptionManager : ISubscriptionManager
    {
        public void SubscribeToCandles(WebSocket websocket)
        {
            var cr = CandlesSubscriptionRequest();

            var jsonSerializerSettings = new JsonSerializerSettings {ContractResolver = new CamelCasePropertyNamesContractResolver()};
            var json = JsonConvert.SerializeObject(cr, Formatting.Indented, jsonSerializerSettings);

            websocket.Send(json);
        }

        private static CandleRequest CandlesSubscriptionRequest()
        {
            return new CandleRequest()
            {
                Channel = "candles",
                Event = "subscribe",
                Key = "trade:15m:tBTCUSD"
            };
        }
    }
}