﻿using System;
using Newtonsoft.Json.Linq;
using SuperSocket.ClientEngine;
using TradingCrypt.BusinessLogic.Parsers;
using TradingCrypt.BusinessLogic.Subscriptions;
using WebSocket4Net;

namespace TradingCrypt.BusinessLogic
{
    public class WebSocketManager : IWebSocketManager
    {
        private WebSocket _websocket;

        private readonly ICandleResponseParser _candleResponseParser;
        private readonly ISubscriptionManager _subscriptionManager;


        public WebSocketManager(ICandleResponseParser candleResponseParser, ISubscriptionManager subscriptionManager)
        {
            _candleResponseParser = candleResponseParser;
            _subscriptionManager = subscriptionManager;
        }

        public bool OpenWebSocket()
        {
            _websocket = new WebSocket("wss://api.bitfinex.com/ws/v2");

            _websocket.Opened += websocket_Opened;
            _websocket.Error += websocket_Error;
            _websocket.Closed += websocket_Closed;
            _websocket.MessageReceived += websocket_MessageReceived;

            _websocket.Open();

            return true;
        }

        private void websocket_Closed(object sender, EventArgs e)
        {
            var close = e.ToString();
        }

        private void websocket_Error(object sender, ErrorEventArgs e)
        {
            var error = e.Exception;
        }

        private void websocket_Opened(object sender, EventArgs e)
        {
            _subscriptionManager.SubscribeToCandles(_websocket);
        }

        private void websocket_MessageReceived(object sender, MessageReceivedEventArgs e)
        {
            var tokenResponse = JToken.Parse(e.Message);

            var message = e.Message;

            dynamic response;
            if (tokenResponse is JArray)
            {
                response = JArray.Parse(message);
                _candleResponseParser.ParseMultipleCandlesResponse(response);
            }
            else
            {
                response = JObject.Parse(message);
                _candleResponseParser.ParseSingleCandleResponse(response);
            }
        }
    }
}