﻿using System;
using System.Threading.Tasks;
using Autofac;
using IoC;
using TradingCrypt.BusinessLogic;
using TradingCrypt.BusinessLogic.Parsers;
using TradingCrypt.BusinessLogic.Subscriptions;

namespace TradingCrypt
{
    internal class Program
    {
        //private static IWebSocketManager _websocketManager;
        private static ICandleResponseParser _candleResponseParser;
        private static ISubscriptionManager _subscriptionManager;

        private static void Main(string[] args)
        {
            LoadKernel();

            new Program().Start().Wait();

            //Console.WriteLine(_websocket.State);
            Console.ReadLine();
        }

        private async Task Start()
        {
            var wsm = new WebSocketManager(_candleResponseParser, _subscriptionManager);
            wsm.OpenWebSocket();
        }

        /// <summary>
        /// Load Ninject Kernel
        /// </summary>
        private static void LoadKernel()
        {
            //AutoFac
            var container = AppBuilderExtensions.IoCSetup();

            //_websocketManager = container.Resolve<IWebSocketManager>();
            _candleResponseParser = container.Resolve<ICandleResponseParser>();
            _subscriptionManager = container.Resolve<ISubscriptionManager>();
        }
    }
}