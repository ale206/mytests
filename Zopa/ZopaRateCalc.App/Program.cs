﻿using System;
using System.Reflection;
using System.Text;
using Ninject;
using ZopaRateCalc.ObjectManager.Managers;
using ZopaRateCalc.ObjectManager.Models;

namespace ZopaRateCalc.App
{
    internal class Program
    {
        private static IMarketManager _marketManager;

        private static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Welcome to Zopa Rate Calculator");

                LoadKernel();

                var marketRatePath = args[0];
                var loanAmount = _marketManager.ValidateAmount(args[1]);

                var marketRateLenders = _marketManager.GetMarketRateLendersFromCsv(marketRatePath);
                var loanDetail = _marketManager.GetLoanDetail(marketRateLenders, loanAmount);

                ShowLoanDetail(loanDetail);

                Console.WriteLine("Thank you. Press any key to exit.");
                Console.ReadKey();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }
        }

        #region LoadKernel

        /// <summary>
        ///     Load Ninject Kernel
        /// </summary>
        private static void LoadKernel()
        {
            IKernel kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            _marketManager = kernel.Get<MarketManager>();
        }

        #endregion

        #region ShowLoanDetail

        /// <summary>
        ///     Show Load Detail
        /// </summary>
        /// <param name="loanDetail"></param>
        private static void ShowLoanDetail(LoanDetail loanDetail)
        {
            var sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine($"Requested amount: £{loanDetail.RequestedAmount}");
            sb.AppendLine($"Rate: {loanDetail.RatePercentageString}");
            sb.AppendLine($"Monthly repayment: £{loanDetail.MonthlyRepayment}");
            sb.AppendLine($"Total repayment: £{loanDetail.TotalRepayment}");
            sb.AppendLine();

            Console.WriteLine(sb.ToString());
        }

        #endregion
    }
}