﻿using Ninject.Modules;
using ZopaRateCalc.ObjectManager.Configuration;
using ZopaRateCalc.ObjectManager.Managers;

namespace ZopaRateCalc.App
{
    public class TopNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IAppConfigManager>().To<AppConfigManager>();
            Bind<IMarketConfiguration>().To<MarketConfiguration>();
            Bind<IMarketManager>().To<MarketManager>();
        }
    }
}