﻿using Ninject.Modules;
using ZopaRateCalc.ObjectManager.Configuration;
using ZopaRateCalc.ObjectManager.Managers;

namespace ZopaRateCalc.IntegrationTests
{
    public class TopModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IAppConfigManager>().To<AppConfigManager>();
            Bind<IMarketConfiguration>().To<MarketConfiguration>();
            Bind<IMarketManager>().To<MarketManager>();
        }
    }
}