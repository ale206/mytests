﻿using System.Reflection;
using Ninject;
using NUnit.Framework;
using ZopaRateCalc.ObjectManager.Managers;
using ZopaRateCalc.ObjectManager.Models;

namespace ZopaRateCalc.IntegrationTests
{
    public class ZopaRateCalcConsoleTest
    {
        private static IMarketManager _marketManager;

        [Test]
        public void ProcessTest()
        {
            LoadKernel();

            const string MarketRatePath = "MarketFile/market.csv";
            var loanAmount = _marketManager.ValidateAmount("1000");

            var marketRateLenders = _marketManager.GetMarketRateLendersFromCsv(MarketRatePath);
            var loanDetail = _marketManager.GetLoanDetail(marketRateLenders, loanAmount);

            var expected = new LoanDetail
            {
                MonthlyRepayment = 30.88m,
                Rate = 0.07m,
                RequestedAmount = 1000,
                TotalRepayment = 1111.64m,
                RatePercentage = 7.0m,
                RatePercentageString = "7.0%"
                
            };

            Assert.AreEqual(expected.Rate, loanDetail.Rate);
            Assert.AreEqual(expected.RatePercentage, loanDetail.RatePercentage);
            Assert.AreEqual(expected.RatePercentageString, loanDetail.RatePercentageString);
            Assert.AreEqual(expected.MonthlyRepayment, loanDetail.MonthlyRepayment);
            Assert.AreEqual(expected.RequestedAmount, loanDetail.RequestedAmount);
            Assert.AreEqual(expected.TotalRepayment, loanDetail.TotalRepayment);
        }

        #region LoadKernel

        /// <summary>
        ///     Load Ninject Kernel
        /// </summary>
        private static void LoadKernel()
        {
            IKernel kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            _marketManager = kernel.Get<MarketManager>();
        }

        #endregion
    }
}