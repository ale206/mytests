﻿using System.Configuration;

namespace ZopaRateCalc.ObjectManager.Configuration
{
    public class AppConfigManager : IAppConfigManager
    {
        public string GetValue(string appKey)
        {
            return ConfigurationManager.AppSettings[appKey];
        }
    }
}