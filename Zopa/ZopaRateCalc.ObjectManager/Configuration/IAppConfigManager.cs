﻿namespace ZopaRateCalc.ObjectManager.Configuration
{
    public interface IAppConfigManager
    {
        string GetValue(string appKey);
    }
}