﻿namespace ZopaRateCalc.ObjectManager.Configuration
{
    public interface IMarketConfiguration
    {
        int MonthlyPayments { get; }
        decimal MinRange { get; }
        decimal MaxRange { get; }
    }
}