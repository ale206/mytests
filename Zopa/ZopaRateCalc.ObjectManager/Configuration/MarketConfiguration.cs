﻿using System;

namespace ZopaRateCalc.ObjectManager.Configuration
{
    public class MarketConfiguration : IMarketConfiguration
    {
        private readonly IAppConfigManager _appConfigManager;

        public MarketConfiguration(IAppConfigManager appConfigManager)
        {
            _appConfigManager = appConfigManager;
        }

        public int MonthlyPayments => Convert.ToInt32(_appConfigManager.GetValue("MonthlyPayments"));
        public decimal MinRange => Convert.ToDecimal(_appConfigManager.GetValue("MinRange"));
        public decimal MaxRange => Convert.ToDecimal(_appConfigManager.GetValue("MaxRange"));
    }
}