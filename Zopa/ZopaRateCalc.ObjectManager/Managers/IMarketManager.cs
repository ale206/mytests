﻿using System.Collections.Generic;
using ZopaRateCalc.ObjectManager.Models;

namespace ZopaRateCalc.ObjectManager.Managers
{
    public interface IMarketManager
    {
        decimal ValidateAmount(string amount);
        IEnumerable<Lender> GetMarketRateLendersFromCsv(string csvFilePath);
        LoanDetail GetLoanDetail(IEnumerable<Lender> lenders, decimal requestedAmount);
    }
}