﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ZopaRateCalc.ObjectManager.Configuration;
using ZopaRateCalc.ObjectManager.Models;

namespace ZopaRateCalc.ObjectManager.Managers
{
    public class MarketManager : IMarketManager
    {
        private readonly IMarketConfiguration _marketConfig;

        public MarketManager(IMarketConfiguration marketConfig)
        {
            _marketConfig = marketConfig;
        }

        #region ValidateAmount

        public decimal ValidateAmount(string amountFromInput)
        {
            decimal amount;
            decimal.TryParse(amountFromInput, out amount);

            if (amount < _marketConfig.MinRange || amount > _marketConfig.MaxRange)
            {
                throw new ArgumentOutOfRangeException(amountFromInput, $"Amount out of range. Min Value:{_marketConfig.MinRange} MaxValue {_marketConfig.MaxRange}");
            }

            if (amount % 100 != 0)
            {
                throw new ArgumentException("Amount must have only increments of 100");
            }

            return amount;
        }

        #endregion

        #region GetMarketRateLendersFromCsv

        public IEnumerable<Lender> GetMarketRateLendersFromCsv(string csvFilePath)
        {
            if (string.IsNullOrEmpty(csvFilePath))
            {
                throw new ArgumentNullException(nameof(csvFilePath));
            }

            string[] fileRows = { };

            if (File.Exists(csvFilePath))
            {
                fileRows = File.ReadAllLines(csvFilePath);
            }

            var lenders = new List<Lender>();

            if (!fileRows.Any()) return lenders;

            var dataRows = RemoveCsvHeader(fileRows);

            try
            {
                foreach (var dataRow in dataRows)
                {
                    var words = dataRow.Split(',');
                    var name = words[0];
                    var rate = decimal.Parse(words[1]);
                    var amount = decimal.Parse(words[2]);

                    lenders.Add(PopulateLender(name, rate, amount));
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Error on GetMarketRateLendersFromCsv: {ex.Message}", ex);
            }

            return lenders;
        }

        #endregion

        #region GetLoanDetail

        public LoanDetail GetLoanDetail(IEnumerable<Lender> lenders, decimal requestedAmount)
        {
            var lendersList = lenders as IList<Lender> ?? lenders.ToList();
            var totalAmountAvailable = lendersList.Sum(x => x.AvailableAmount);
            if (totalAmountAvailable < requestedAmount)
            {
                throw new ArgumentOutOfRangeException($"Loan amount ({requestedAmount}) is more than the available on the system ({totalAmountAvailable})");
            }

            var amountToBeFilled = requestedAmount;
            decimal monthlyRepayment = 0;
            decimal currentRate = 0;

            foreach (var lender in lendersList.OrderBy(x => x.Rate))
            {
                decimal amountBorrowed;
                if (amountToBeFilled - lender.AvailableAmount <= 0)
                {
                    amountBorrowed = amountToBeFilled;
                    amountToBeFilled = 0;
                }
                else
                {
                    amountBorrowed = lender.AvailableAmount;
                    amountToBeFilled -= lender.AvailableAmount;
                }

                monthlyRepayment = UpdateMonthlyRepayment(monthlyRepayment, amountBorrowed, lender.Rate);

                currentRate = currentRate == 0 ? lender.Rate : UpdateRate(currentRate, lender.Rate);

                if (amountToBeFilled == 0)
                {
                    break;
                }
            }

            var totalRepayment = monthlyRepayment * _marketConfig.MonthlyPayments;

            currentRate = Math.Round(currentRate, 2);
            monthlyRepayment = Math.Round(monthlyRepayment, 2);
            totalRepayment = Math.Round(totalRepayment, 2);

            return PopulateLoanDetail(requestedAmount, currentRate, monthlyRepayment, totalRepayment);
        }

        #endregion

        #region UpdateRate

        private static decimal UpdateRate(decimal currentRate, decimal rate)
        {
            return (currentRate + rate) / 2;
        }

        #endregion

        #region UpdateMonthlyRepayment

        private decimal UpdateMonthlyRepayment(decimal currentMonthlyRepayment, decimal amountBorrowed, decimal rate)
        {
            /*
            c = (rP((1+r)^N))/((1+r)^N) -1
            
            r = monthly interest rate
            N = number of monthly payments
            P = amount borrowed
            */

            var monthlyPayments = _marketConfig.MonthlyPayments;

            var r = rate / 12m;

            //(1+r)^N
            var factor = Convert.ToDecimal(Math.Pow((double) (1 + r), monthlyPayments));

            var monthlyPayment = r * amountBorrowed * factor / (factor - 1);

            currentMonthlyRepayment += monthlyPayment;

            return currentMonthlyRepayment;
        }

        #endregion

        #region RemoveCsvHeader

        private static IEnumerable<string> RemoveCsvHeader(IEnumerable<string> fileRows)
        {
            var fileRowsList = fileRows.ToList();
            fileRowsList.RemoveAt(0);

            return fileRowsList;
        }

        #endregion

        #region PopulateLender

        private static Lender PopulateLender(string name, decimal rate, decimal availableAmount)
        {
            return new Lender {Name = name, Rate = rate, AvailableAmount = availableAmount};
        }

        #endregion

        #region PopulateLoanDetail

        private static LoanDetail PopulateLoanDetail(decimal requestedAmount, decimal rate, decimal monthlyRepayment, decimal totalRepayment)
        {
            return new LoanDetail
            {
                Rate = rate,
                MonthlyRepayment = monthlyRepayment,
                RequestedAmount = requestedAmount,
                TotalRepayment = totalRepayment,
                RatePercentage = rate * 100,
                RatePercentageString = $"{rate * 100:.0###}%"
            };
        }

        #endregion
    }
}