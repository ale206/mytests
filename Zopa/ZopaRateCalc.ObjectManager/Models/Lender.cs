﻿namespace ZopaRateCalc.ObjectManager.Models
{
    public class Lender
    {
        public string Name { get; set; }
        public decimal Rate { get; set; }
        public decimal AvailableAmount { get; set; }
    }
}