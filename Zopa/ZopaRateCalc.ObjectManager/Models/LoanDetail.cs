﻿namespace ZopaRateCalc.ObjectManager.Models
{
    public class LoanDetail
    {
        public decimal RequestedAmount { get; set; }
        public decimal Rate { get; set; }
        public decimal MonthlyRepayment { get; set; }
        public decimal TotalRepayment { get; set; }
        public decimal RatePercentage { get; set; }
        public string RatePercentageString { get; set; }
    }
}