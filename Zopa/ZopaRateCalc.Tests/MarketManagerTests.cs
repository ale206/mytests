﻿using System;
using System.Linq;
using Moq;
using NUnit.Framework;
using ZopaRateCalc.ObjectManager.Configuration;
using ZopaRateCalc.ObjectManager.Managers;
using ZopaRateCalc.ObjectManager.Models;

namespace ZopaRateCalc.Tests
{
    [TestFixture]
    public class MarketManagerTests
    {
        private Mock<IMarketConfiguration> _mockIMarketConfiguration;
        private MarketManager _marketManager;

        [SetUp]
        public void Init()
        {
            _mockIMarketConfiguration = new Mock<IMarketConfiguration>();

            _mockIMarketConfiguration.Setup(x => x.MonthlyPayments).Returns(36);
            _mockIMarketConfiguration.Setup(x => x.MinRange).Returns(1000);
            _mockIMarketConfiguration.Setup(x => x.MaxRange).Returns(15000);
            _marketManager = new MarketManager(_mockIMarketConfiguration.Object);
        }

        [TearDown]
        public void Cleanup()
        {
            _mockIMarketConfiguration = null;
        }

        [TestCase("1000")]
        public void ValidateAmountTest(string amountFromInput)
        {
            var amountReturned = _marketManager.ValidateAmount(amountFromInput);

            Assert.IsInstanceOf<decimal>(amountReturned);
        }

        [TestCase("100")]
        [TestCase("20000")]
        public void ValidateAmountTest_AmountNotValid(string amountFromInput)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => _marketManager.ValidateAmount(amountFromInput));
        }

        [TestCase("1010")]
        [TestCase("13242")]
        public void ValidateAmountTest_AmountWithWrongIncrement(string amountFromInput)
        {
            Assert.Throws<ArgumentException>(() => _marketManager.ValidateAmount(amountFromInput));
        }

        [TestCase("MarketFile/market.csv")]
        public void GetMarketRateFile(string csvFilePath)
        {
            var lenders = _marketManager.GetMarketRateLendersFromCsv(csvFilePath);

            Assert.IsNotNull(lenders);
            Assert.IsInstanceOf<Lender>(lenders.First());
        }

        [TestCase("")]
        public void GetMarketRateFile_FileNotFound(string csvFilePath)
        {
            Assert.Throws<ArgumentNullException>(() => _marketManager.GetMarketRateLendersFromCsv(csvFilePath));
        }

        [Test]
        public void GetLoanDetail_Test()
        {
            var loanDetail = _marketManager.GetLoanDetail(TestHelper.GetLenders(), 1000);

            Assert.IsNotNull(loanDetail);
            Assert.IsInstanceOf<LoanDetail>(loanDetail);
        }
    }
}