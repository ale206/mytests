﻿using System.Collections.Generic;
using ZopaRateCalc.ObjectManager.Models;

namespace ZopaRateCalc.Tests
{
    public class TestHelper
    {
        public static IEnumerable<Lender> GetLenders()
        {
            var lendersList = new List<Lender>
            {
                new Lender
                {
                    Rate = 0.2m,
                    AvailableAmount = 30000,
                    Name = "Paul"
                }
            };

            return lendersList;
        }
    }
}